//
// Created by ketanb on 6/21/2017.
//
#include <jni.h>

extern "C" {
    JNIEXPORT jstring JNICALL
    Java_timesofmoney_iandmsuperagent_IMBAgentApp_callStaging(JNIEnv *env, jobject instance) {
        return env->NewStringUTF("https://movit.timesofmoney.in/mps/");
    }
    JNIEXPORT jstring JNICALL
    Java_timesofmoney_iandmsuperagent_IMBAgentApp_callQA(JNIEnv *env, jobject instance) {
        return env->NewStringUTF("https://qamovit.timesofmoney.in/mps/");
    }
    JNIEXPORT jstring JNICALL
    Java_timesofmoney_iandmsuperagent_IMBAgentApp_callProduction(JNIEnv *env, jobject instance) {
        return env->NewStringUTF("https://nivmt.network.com.eg/mps/");
    }
    JNIEXPORT jstring JNICALL
    Java_timesofmoney_iandmsuperagent_IMBAgentApp_callPrepod(JNIEnv *env, jobject instance) {
        return env->NewStringUTF("https://uat.network.com.eg/mps/");
    }
    JNIEXPORT jstring JNICALL
    Java_timesofmoney_iandmsuperagent_IMBAgentApp_callDemo(JNIEnv *env, jobject instance) {
        return env->NewStringUTF("http://demo.timesofmoney.com/mps/");
    }
    JNIEXPORT jstring JNICALL
    Java_timesofmoney_iandmsuperagent_IMBAgentApp_callLocal(JNIEnv *env, jobject instance) {
        return env->NewStringUTF("http://10.158.208.45:8080/MpsPreProd/");
    }
}
