package timesofmoney.iandmsuperagent;

import android.app.Application;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import timesofmoney.iandmsuperagent.models.LoginResponseDetails;
import timesofmoney.iandmsuperagent.models.MerchantProfile;
import timesofmoney.iandmsuperagent.utilities.Constants;

/**
 * Created by pankajp on 18-04-2016.
 */
public class IMBAgentApp extends Application {

    private static IMBAgentApp imbAgentApp;

    private String strKey;
    private String strSalt;
    private String strIV;
    private String strMobileNumber;
    private String strSessionID;
    private String strAgentID;

    private String balance = "", WalletID = "", commission = "";


    private String strPrepaidWalledID;
    private static LoginResponseDetails.Wallet wallet;


    private static final String TAG = IMBAgentApp.class.getName();


    public static MerchantProfile getMerchantProfile() {
        return objMerchantProfile;
    }

    public static void setMerchantProfile(MerchantProfile objMerchantProfile) {
        IMBAgentApp.objMerchantProfile = objMerchantProfile;
    }

    static {
        System.loadLibrary("native-lib");
    }


    private native String callStaging();
    private native String callQA();
    private native String callProduction();
    private native String callPrepod();
    private native String callDemo();
    private native String callLocal();


    public static IMBAgentApp getImbMerchantApp() {
        return imbAgentApp;
    }

    public static void setImbMerchantApp(IMBAgentApp imbMerchantApp) {
        IMBAgentApp.imbAgentApp = imbMerchantApp;
    }


    private static MerchantProfile objMerchantProfile;

    public Context getAppContext() {
        return this.getContext();
    }

    public String getKey() {
        return strKey;
    }

    public void setKey(String strKey) {
        this.strKey = strKey;
    }

    public String getIV() {
        return strIV;
    }

    public void setIV(String strIV) {
        this.strIV = strIV;
    }


    public String getMobileNumber() {
        return strMobileNumber;
    }

    public void setMobileNumber(String strMobileNumber) {
        this.strMobileNumber = strMobileNumber;
    }

    public String getSessionID() {
        return strSessionID;
    }

    public void setSessionID(String strSessionID) {
        this.strSessionID = strSessionID;
    }

    public String getAgentID() {
        return strAgentID;
    }

    public void setAgentID(String strMerchantID) {
        this.strAgentID = strMerchantID;
    }

    public String getPrepaidWalledID() {
        return strPrepaidWalledID;
    }

    public void setPrepaidWalledID(String strPrepaidWalledID) {
        this.strPrepaidWalledID = strPrepaidWalledID;
    }


    public void setWallet(LoginResponseDetails.Wallet wallet) {
        IMBAgentApp.wallet = wallet;
        Log.v("TAG", " Wallet is set ");
        for (LoginResponseDetails.Wallet.WalletId walletId : wallet.getWallets()) {
            if (walletId.getAccountType().equals("General A/C"))
                setBalance(walletId.getBalance() + "");
            else if (walletId.getAccountType().equals("Commission A/C")) {
                setCommission(walletId.getBalance());
            }
        }

    }

    public String getWalletID() {

        for (LoginResponseDetails.Wallet.WalletId walletId : wallet.getWallets()) {
            if (walletId.getIsPrimary().equals("Y"))
                WalletID = walletId.getValue() + "";
        }

        Log.v("TAG", " Wallet ID " + WalletID);
        return WalletID;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }




    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        imbAgentApp = this;

        //Log.d(TAG, "Starting application" + this.toString());

        /*Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        Fabric.with(this, crashlyticsKit);*/

        // Demo
       /* if (TextUtils.isEmpty(AppSettings.getData(this, AppSettings.BASE_URL)))
            AppSettings.putData(this, AppSettings.BASE_URL, Constants.DEMO_LINK);*/

        // QA
      /*  if (TextUtils.isEmpty(AppSettings.getData(this, AppSettings.BASE_URL)))
            AppSettings.putData(this, AppSettings.BASE_URL, Constants.QA_LINK);*/

        // Staging
      /*  if (TextUtils.isEmpty(AppSettings.getData(this, AppSettings.BASE_URL)))
            AppSettings.putData(this, AppSettings.BASE_URL, Constants.STAGING_LINK);*/


        // Production
        /*if (TextUtils.isEmpty(AppSettings.getData(this, AppSettings.BASE_URL)))
            AppSettings.putData(this, AppSettings.BASE_URL,Constants.PRODUCTION_LINK);*/


    }


    public String getIMEINo() {

        if (Constants.DEVICE_ID) {

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            return telephonyManager.getDeviceId();

        } else {
            return "1234";
        }
    }

    public String getSalt() {
        return strSalt;
    }

    public void setSalt(String strSalt) {
        this.strSalt = strSalt;
    }

    public static IMBAgentApp getInstance() {
        return imbAgentApp;
    }

    public Context getContext() {
        return this;
    }

    public String getBaseURL() {
        return callPrepod();//getApplicationContext().getString(R.string.qa_link);
    }


}



