package timesofmoney.iandmsuperagent.activities;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.LogUtils;


public class AboutmVisa extends BaseActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutm_visa);

        if (getIntent().hasExtra("terms"))
            setToolbar(getString(R.string.terms));
        else
            setToolbar(getString(R.string.about_mvisa));

        String url = "about_mvisa.html";

        webView = (WebView) findViewById(R.id.web);
        webView.setWebViewClient(new MyWebViewClient());
        if (AppSettings.getData(getApplicationContext(), AppSettings.APPLANGUAGE).equals("ar")) {
            url = "about_qnb_ar.html";
        }

        LogUtils.Verbose("About URL", IMBAgentApp.getInstance().getBaseURL() + url);
       // webView.loadUrl(IMBAgentApp.getInstance().getBaseURL() + url);

        //showProgress();
    }

    class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);


            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            dismissProgress();

        }
    }
}
