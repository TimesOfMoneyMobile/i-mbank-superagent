package timesofmoney.iandmsuperagent.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;

import timesofmoney.iandmsuperagent.BuildConfig;
import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.dialogs.ProgressDialog;
import timesofmoney.iandmsuperagent.receivers.AlarmReceiver;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.AnimationUtils;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.ErrorUtils;
import timesofmoney.iandmsuperagent.utilities.KeyBoardUtils;
import timesofmoney.iandmsuperagent.utilities.LogUtils;
import timesofmoney.iandmsuperagent.utilities.ReceiverManager;


/**
 * Created by kunalk on 3/22/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    KeyBoardUtils keyBoardUtils;

    public static final long SESSION_TIMEOUT = 8 * 60 * 1000; // 5 min = 5 * 60 * 1000 ms

    ReceiverManager objReceiverManager;

    String strMerchantID = "";


    public MyBroadcastReceiver objMyBroadcastReceiver;

    public OkDialog objOkDialog;

    ImageView ivIconRefresh;
    CustomTextView ctvCardBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // super.onCreate(savedInstanceState, persistentState);
        super.onCreate(savedInstanceState);

        // To Disable Screenshot

        if (!BuildConfig.DEBUG) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE);
        }

        strMerchantID = IMBAgentApp.getInstance().getAgentID();///AppSettings.getData(BaseActivity.this, AppSettings.MERCHANT_ID);

        objReceiverManager = ReceiverManager.init(BaseActivity.this);

        if (this instanceof DashboardActivity) {
            if (!TextUtils.isEmpty(strMerchantID) && strMerchantID.length() > 0) {
                setTimer();
            }
        }

    }


    public IMBAgentApp getApp() {
        return (IMBAgentApp) this.getApplication();
    }

    public void setToolbar(String titleText) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle(null);
        if(!TextUtils.isEmpty(titleText)){
            TextView title = (TextView) toolbar.findViewById(R.id.title);
            title.setText(titleText);
        }
        setSupportActionBar(toolbar);

        ImageView backArrow = (ImageView) findViewById(R.id.toolbar_dashboard_iv_ham_icon);

        if (backArrow != null) {
            backArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

    }


    public void setFooter(boolean isRefreshIconVisible,String cardBalance,String commission) {
        RelativeLayout rlForContent = (RelativeLayout)findViewById(R.id.footer_dashboard_rl_data);
        if(isRefreshIconVisible){
             ivIconRefresh = (ImageView) findViewById(R.id.footer_dashboard_iv_icon_refresh);
            ivIconRefresh.setVisibility(View.VISIBLE);

            ivIconRefresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBalanceInquiryAPI();
                }
            });
        }

         ctvCardBalance = (CustomTextView)findViewById(R.id.footer_dashboard_ctv_card_balance);
        if(TextUtils.isEmpty(cardBalance)){
            ctvCardBalance.setText(Constants.formatAmount(IMBAgentApp.getInstance().getBalance()));
        }else{
            ctvCardBalance.setText(cardBalance);

        }


        CustomTextView ctvCommission = (CustomTextView)findViewById(R.id.footer_dashboard_ctv_commission);
        ctvCommission.setText(commission);



    }


    public void callBalanceInquiryAPI() {

        String xmlParam = "\t\t<MobileNumber>" + IMBAgentApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<WalletId>" + IMBAgentApp.getInstance().getWalletID() + "</WalletId>";


        xmlParam = Constants.getReqXML("BalanceInquiry", xmlParam);
        final String plainXML = xmlParam;
        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Constants.showToast(getApplicationContext(), getResources().getString(R.string.please_try_again));
            return;
        }

        final AnimationUtils rotateanimation = new AnimationUtils();
        rotateanimation.rotateView(ivIconRefresh, 0, 360, ivIconRefresh.getWidth() / 2, ivIconRefresh.getHeight() / 2, null);

        RetrofitTask.getInstance().executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (getApplicationContext() != null)
                    rotateanimation.stopRotateAnimation(ivIconRefresh);

                if (!isSuccess) {
                    Constants.showToast(getApplicationContext(), response);
                    return;
                }
                String decrypted = null;
                try {
                    decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", "Response " + decrypted);

                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();
                    xpp.setInput(new StringReader(decrypted));
                    int eventType = xpp.getEventType();
                    String balance = "";
                    String gotSuccess = "";
                    String errMsg = "";
                    String text = "";
                    while (eventType != XmlPullParser.END_DOCUMENT) {

                        String tagname = xpp.getName();
                        if (eventType == XmlPullParser.TEXT) {
                            text = xpp.getText();
                        } else if (eventType == XmlPullParser.END_TAG) {
                            if (tagname.equalsIgnoreCase("ResponseType"))
                                gotSuccess = text;
                            if (tagname.equalsIgnoreCase("Balance"))
                                balance = text;

                            if (tagname.equalsIgnoreCase("ErrorCode"))
                                errMsg = text;
                        }
                        eventType = xpp.next();
                    }

                    LogUtils.Verbose("TAG", " Got success " + gotSuccess);

                    if (gotSuccess.equals("Success")) {
                        IMBAgentApp.getInstance().setBalance(Constants.formatAmount(balance));
                        //IMBAgentApp.getInstance().updateBalance(walletId);
                        if (getApplicationContext() != null) {
                            ctvCardBalance.setText(Constants.formatAmount(balance));

                        }
                    } else {
                        if (getApplicationContext() != null) {
                            // Constants.showToast(getActivity(), getString(R.string.please_try_again));
                            Constants.showToast(getApplicationContext(), ErrorUtils.getErrorMessage(errMsg, getString(R.string.errorAPI)));
                        }
                    }


                } catch (Exception e) {
                    LogUtils.Exception(e, plainXML, decrypted);
                    Constants.showToast(getApplicationContext(), getResources().getString(R.string.please_try_again));
                }
            }
        });
    }


    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle b = intent.getExtras();

            String message = b.getString("isSessionExpired");

            //Log.e("isSessionExpired", "" + message);

            strMerchantID = IMBAgentApp.getInstance().getAgentID();


            if (!TextUtils.isEmpty(strMerchantID) && strMerchantID.length() > 0) {
                AppSettings.putData(context, AppSettings.ISSESSIONEXPIRED, message);
            }


            if (!TextUtils.isEmpty(message) && message.equalsIgnoreCase("Yes")) {
                if (!TextUtils.isEmpty(strMerchantID) && strMerchantID.length() > 0) {
                    LogUtils.Debug("TAG", "BR Called");
                    showSessionExpireDialog();
                }
            } else {
                Intent i = new Intent(BaseActivity.this, AlarmReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1, i, 0);
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
            }


        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        String strIsSessionExpired = AppSettings.getData(BaseActivity.this, AppSettings.ISSESSIONEXPIRED);
        strMerchantID = IMBAgentApp.getInstance().getAgentID();


        if (!TextUtils.isEmpty(strIsSessionExpired) && strIsSessionExpired.equalsIgnoreCase("YES")) {
            if (!TextUtils.isEmpty(strMerchantID) && strMerchantID.length() > 0) {
                LogUtils.Debug("TAG", "Resume Called");
                showSessionExpireDialog();
            }
        } else {

            try {
                objMyBroadcastReceiver = new MyBroadcastReceiver();

                if (objReceiverManager != null && !objReceiverManager.isReceiverRegistered(objMyBroadcastReceiver)) {

                    objReceiverManager.registerReceiver(objMyBroadcastReceiver, new IntentFilter("SessionTimeout"));

                    //LogUtils.("TAG", "Reciever Registerd");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

           /* registerReceiver(objMyBroadcastReceiver, new IntentFilter("SessionTimeout"));
            Log.e("TAG", "Reciever Registerd");*/

        }
    }


    public void showSessionExpireDialog() {

        try {

            String message = getString(R.string.session_expire_login_again);

            if (objOkDialog != null && objOkDialog.isShowing()) {
                return;
            }

            objOkDialog = new OkDialog(this, message, getString(R.string.app_name), new OkDialog.IOkDialogCallback() {
                @Override
                public void handleResponse() {


                    IMBAgentApp.getInstance().setAgentID(null);
                    AppSettings.putData(BaseActivity.this, AppSettings.ISSESSIONEXPIRED, "No");

                    Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void setTimer() {

        //Log.e("TAG", " Setting timer on " + System.currentTimeMillis());

        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 1, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                    + (SESSION_TIMEOUT), pendingIntent);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                    + (SESSION_TIMEOUT), pendingIntent);
        }


    }


    public void showProgress() {
        progressDialog = new ProgressDialog(this, getString(R.string.msgWait));
        progressDialog.show();
    }

    public void dismissProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public void showCustomKeyboard(RelativeLayout relativeLayout, EditText editText, View view) {
        keyBoardUtils = new KeyBoardUtils();

        if (AppSettings.getData(this, AppSettings.APPLANGUAGE).equals("ar"))
            keyBoardUtils.setCustomKeyboard(this, relativeLayout, editText, view, KeyBoardUtils.ARABIC_KEYBOARD);
    }

    @Override
    public void onBackPressed() {

        if (keyBoardUtils != null && keyBoardUtils.isVisible())
            keyBoardUtils.hideCustomKeyboard();
        else
            super.onBackPressed();

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

        if (!TextUtils.isEmpty(strMerchantID) && strMerchantID.length() > 0) {
            setTimer();
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (objReceiverManager.isReceiverRegistered(objMyBroadcastReceiver)) {
                objReceiverManager.unregisterReceiver(objMyBroadcastReceiver);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
