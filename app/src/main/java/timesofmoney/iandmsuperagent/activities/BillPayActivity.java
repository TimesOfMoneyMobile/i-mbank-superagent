package timesofmoney.iandmsuperagent.activities;

import android.os.Bundle;

import timesofmoney.iandmsuperagent.R;

/**
 * Created by vishwanathp on 3/5/17.
 */
public class BillPayActivity extends BaseActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_pay);

        setToolbar(getString(R.string.text_bill_payment));

    }

}
