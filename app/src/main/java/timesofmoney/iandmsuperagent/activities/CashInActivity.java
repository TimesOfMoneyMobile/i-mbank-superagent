package timesofmoney.iandmsuperagent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.utilities.Constants;

/**
 * Created by vishwanathp on 3/5/17.
 */
public class CashInActivity extends BaseActivity {

    EditText etCustomerCardNumber,etAmount;
    Button bProceed;
    String strCustomerCardNumber,strAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_in);

        setToolbar(getString(R.string.title_cash_in));

        setFooter(false,"","");

        etCustomerCardNumber = (EditText)findViewById(R.id.act_ci_et_customer_card_number);
        etAmount   = (EditText)findViewById(R.id.act_ci_et_amount);
        bProceed = (Button) findViewById(R.id.act_ci_b_proceed);


        bProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Constants.startNewActivity(CashInActivity.this,CashInConfirmActivity.class);


                if(validate()) {
                    Intent intent = new Intent(getApplicationContext(), CashInConfirmActivity.class);
                    intent.putExtra(Constants.SCREEN_TYPE, Constants.CASH_IN);
                    intent.putExtra(Constants.CARD_NUMBER, strCustomerCardNumber);//"XXXX XXXX XXXX 1234");//
                    intent.putExtra(Constants.AMOUNT_VALUE, strAmount);//"1234");//
                    startActivity(intent);
                }
            }
        });

    }




    private boolean validate() {
        strCustomerCardNumber = etCustomerCardNumber.getText().toString().trim();
        strAmount = etAmount.getText().toString().trim();



        String message = null;

        if (TextUtils.isEmpty(strCustomerCardNumber))
            message = getString(R.string.errorCardNumberEmpty);

        else if (strCustomerCardNumber.length()!=getResources().getInteger(R.integer.cardNumberLength))
            message = getString(R.string.error_valid_card_number);

        else if (TextUtils.isEmpty(strAmount))
            message = getString(R.string.errorEmptyAmount);

        if (message != null) {
            new OkDialog(this, message, null, null);
            return false;
        }

        return true;
    }
}