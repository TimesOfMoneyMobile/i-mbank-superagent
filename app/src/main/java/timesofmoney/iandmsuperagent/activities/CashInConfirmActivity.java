package timesofmoney.iandmsuperagent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.LogUtils;

import static timesofmoney.iandmsuperagent.utilities.Constants.getHash;

/**
 * Created by vishwanathp on 4/5/17.
 */
public class CashInConfirmActivity extends BaseActivity{

    Button bConfirm;
    EditText etMPIN;
    CustomTextView ctvCardNumber,ctvAmount,ctvFees,ctvTotalAmount,ctvLabelCardNumber;
    String strMPIN,strCardNumber,strAmountValue,strMvisaId;

    LinearLayout llForData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_in_confirm);

        setToolbar(getString(R.string.title_cash_in));

        setFooter(false,"","");

        llForData = (LinearLayout)findViewById(R.id.frag_maf_ll_for_data);

        ctvLabelCardNumber = (CustomTextView) findViewById(R.id.frag_conform_data_ctv_label_card_number);
        ctvCardNumber = (CustomTextView)findViewById(R.id.frag_conform_data_ctv_card_number);
        ctvAmount = (CustomTextView)findViewById(R.id.frag_conform_data_ctv_amount);
        ctvFees = (CustomTextView)findViewById(R.id.frag_conform_data_ctv_fees);
        ctvTotalAmount = (CustomTextView)findViewById(R.id.frag_conform_data_ctv_total_amount);

        etMPIN = (EditText)findViewById(R.id.act_cic_et_mpin);
        bConfirm = (Button)findViewById(R.id.act_cic_b_confirm);


        if(getIntent().getStringExtra(Constants.SCREEN_TYPE).equals(Constants.REQUEST_CASH)){
            setToolbar(getString(R.string.menu_request_cash));
            llForData.setVisibility(View.GONE);

            ctvLabelCardNumber.setText(getString(R.string.mvisa_id));
            strMvisaId = getIntent().getStringExtra(Constants.MVISA_ID);
            ctvCardNumber.setText(strMvisaId);

        }else{
            setToolbar(getString(R.string.menu_cash_in));
            llForData.setVisibility(View.VISIBLE);
            ctvLabelCardNumber.setText(getString(R.string.card_number));

            strCardNumber = getIntent().getStringExtra(Constants.CARD_NUMBER);
            ctvCardNumber.setText(strCardNumber);
        }


        strAmountValue = getIntent().getStringExtra(Constants.AMOUNT_VALUE);
        ctvAmount.setText(getString(R.string.rwf)+" "+strAmountValue);

        ctvFees.setText("N.A");
        ctvTotalAmount.setText("N.A");


        bConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validate()){

                    if(getIntent().getStringExtra(Constants.SCREEN_TYPE).equals(Constants.REQUEST_CASH)){
                        callRequestCahAPI();
                    }else{
                        callCashInApi();

                    }
                    
                }

            }
        });

    }

    private void callRequestCahAPI() {

        String strMPINHash = "";

        if (IMBAgentApp.getInstance().getSalt() != null && !TextUtils.isEmpty(IMBAgentApp.getInstance().getSalt())) {
            strMPINHash = getHash(strMPIN, IMBAgentApp.getInstance().getSalt());
        } else {
            strMPINHash = Constants.getHash(strMPIN.getBytes());
        }


        String xmlParam = "\t\t<MobileNumber>"+IMBAgentApp.getInstance().getMobileNumber()+"</MobileNumber>\n"+
                "\t\t<SuperAgentId>"+ strMvisaId+"</SuperAgentId>\n"+
                "\t\t<Amount>"+strAmountValue+"</Amount>\n"+
                "\t\t<Tpin>"+strMPINHash+"</Tpin>\n"+
                "\t\t<WalletId></WalletId>";


        xmlParam = Constants.getReqXML("WithFloat", xmlParam);

        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Constants.showToast(this, e.toString());
            return;
        }

        showProgress();
        RetrofitTask.getInstance().executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {
                dismissProgress();

                if (!isSuccess) {
                    new OkDialog(CashInConfirmActivity.this, response, null, null);
                    return;
                }


                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);

                    Map<String, String> responseMap =parseResponse(decrypted);

                    if (responseMap.get("ResponseType").equals("Success")) {

                        Intent intent = new Intent(CashInConfirmActivity.this,CashInSuccessActivity.class);
                        intent.putExtra(Constants.TXN_TYPE,Constants.REQUEST_CASH);
                        intent.putExtra(Constants.TXN_ID,responseMap.get("TxnId"));
                        intent.putExtra(Constants.AMOUNT_VALUE,responseMap.get("Amount"));
                        intent.putExtra(Constants.MVISA_ID,strMvisaId);

                        intent.putExtra(Constants.TIME_STAMP, Constants.getFormattedDate(responseMap.get("Timestamp"), "dd-MM-yy hh:mm:ss", "dd MMM'' yyyy | HH:mm"));

                       // LogUtils.Verbose("TAG CashInSuccessActivity",responseMap.toString()+","+responseMap.get("ERN")+","+responseMap.get("Recipient")+","+responseMap.get("Recipient"));


                        IMBAgentApp.getInstance().setBalance(responseMap.get("Balance"));

                        startActivity(intent);


                    } else {
                        new OkDialog(CashInConfirmActivity.this, responseMap.get("Reason"), null, null);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    new OkDialog(CashInConfirmActivity.this, e.getMessage(), null, null);
                }


            }
        });







    }

    private void callCashInApi() {

        String strMPINHash = "";

        if (IMBAgentApp.getInstance().getSalt() != null && !TextUtils.isEmpty(IMBAgentApp.getInstance().getSalt())) {
            strMPINHash = getHash(strMPIN, IMBAgentApp.getInstance().getSalt());
        } else {
            strMPINHash = Constants.getHash(strMPIN.getBytes());
        }


        String xmlParam = "\t\t<ReceiverPAN>"+strCardNumber+"</ReceiverPAN>\n"+
                "\t\t<SenderMobileNumber>"+ IMBAgentApp.getInstance().getMobileNumber()+"</SenderMobileNumber>\n"+
                "\t\t<Amount>"+strAmountValue+"</Amount>\n"+
                "\t\t<Tpin>"+strMPINHash+"</Tpin>\n"+
                "\t\t<WalletId></WalletId>";


        xmlParam = Constants.getReqXML("CashIn", xmlParam);

        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Constants.showToast(this, e.toString());
            return;
        }

        showProgress();
        RetrofitTask.getInstance().executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {
                dismissProgress();

                if (!isSuccess) {
                    new OkDialog(CashInConfirmActivity.this, response, null, null);
                    return;
                }


                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);

                    Map<String, String> responseMap = parseResponse(decrypted);

                    if (responseMap.get("ResponseType").equals("Success")) {

                        Intent intent = new Intent(CashInConfirmActivity.this,CashInSuccessActivity.class);
                        intent.putExtra(Constants.TXN_TYPE,Constants.CASH_IN_SUCCESS);
                        intent.putExtra(Constants.TXN_ID,responseMap.get("ERN"));
                        intent.putExtra(Constants.AMOUNT_VALUE,responseMap.get("Amount"));
                        intent.putExtra(Constants.CARD_NUMBER,responseMap.get("Recipient"));
                        intent.putExtra(Constants.FEES,responseMap.get("Fee"));
                        intent.putExtra(Constants.TIME_STAMP, Constants.getFormattedDate(responseMap.get("Timestamp"), "dd-MM-yy hh:mm:ss", "dd MMM'' yyyy | HH:mm"));

                      //  LogUtils.Verbose("TAG CashInSuccessActivity",responseMap.toString()+","+responseMap.get("ERN")+","+responseMap.get("Recipient")+","+responseMap.get("Recipient"));


                        intent.putExtra(Constants.TOTAL_AMOUNT,subtractStrings(Constants.formatAmount(responseMap.get("Amount")),Constants.formatAmount(responseMap.get("Fee"))) + "");

                        IMBAgentApp.getInstance().setBalance(Constants.formatAmount(responseMap.get("Balance")));

                        startActivity(intent);


                    } else {
                        new OkDialog(CashInConfirmActivity.this, responseMap.get("Reason"), null, null);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    new OkDialog(CashInConfirmActivity.this, e.getMessage(), null, null);
                }


            }
        });



    }

    private boolean validate() {
        strMPIN = etMPIN.getText().toString();

        if (strMPIN.trim().length() == 0) {
            new OkDialog(this, getString(R.string.blankerrorMpin), null, null);
            return false;
        }

        if (strMPIN.trim().length() != getResources().getInteger(R.integer.MPINLength)) {
            new OkDialog(this, getString(R.string.errorMpin), null, null);
            return false;
        }

        return true;
    }

    private Map<String, String> parseResponse(String xml) {

        Map<String, String> responseMap = new HashMap<>();

        try {

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new StringReader(xml));
            int eventType = xpp.getEventType();
            String text = "";

            while (eventType != XmlPullParser.END_DOCUMENT) {

                String tagname = xpp.getName();

                if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();

                } else if (eventType == XmlPullParser.END_TAG) {
                    if (tagname.equalsIgnoreCase("ResponseType")) {
                        responseMap.put("ResponseType", text);
                    }
                    if (tagname.equalsIgnoreCase("Message"))
                        responseMap.put("Message", text);


                    if (tagname.equalsIgnoreCase("TxnId"))
                        responseMap.put("TxnId", text);
                    if (tagname.equalsIgnoreCase("ERN"))
                        responseMap.put("ERN", text);
                    if (tagname.equalsIgnoreCase("Amount"))
                        responseMap.put("Amount", text);
                    if (tagname.equalsIgnoreCase("Fee"))
                        responseMap.put("Fee", text);
                    if (tagname.equalsIgnoreCase("Balance"))
                        responseMap.put("Balance", text);
                    if (tagname.equalsIgnoreCase("SuperAgentId"))
                        responseMap.put("SuperAgentId", text);
                    if (tagname.equalsIgnoreCase("OTP"))
                        responseMap.put("OTP", text);
                    if (tagname.equalsIgnoreCase("Recipient"))
                        responseMap.put("Recipient", text);
                    if (tagname.equalsIgnoreCase("Timestamp"))
                        responseMap.put("Timestamp", text);
                    if (tagname.equalsIgnoreCase("ErrorCode"))
                        responseMap.put("ErrorCode", text);
                    if (tagname.equalsIgnoreCase("Reason"))
                        responseMap.put("Reason", text);

                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return responseMap;
    }


    public int subtractStrings(String amount,String fee){
        int amountValue, feeValue;

        amountValue = Integer.parseInt(amount);
        feeValue = Integer.parseInt(fee);

       return (amountValue - feeValue);

    }

}
