package timesofmoney.iandmsuperagent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.utilities.Constants;


/**
 * Created by vishwanathp on 4/5/17.
 */
public class CashInSuccessActivity extends BaseActivity{

    RelativeLayout rlForIconandMessage;
    ImageView ivIconSuccess;
    CustomTextView ctvSuccessText,ctvMobileNumber,ctvTransactionId,ctvDate,ctvAmount,ctvFees,ctvCardNumberText,ctvCardNumber,ctvTotalAmount,ctvSuccessMessage;
    Button bBackToDashBoard;
    LinearLayout llCashOutFailure,llCashOutSuccess,llCashInSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_in_success);


        setFooter(false,"","");

        //Components of header part
        rlForIconandMessage = (RelativeLayout)findViewById(R.id.act_cis_rl_for_icon_and_message);
        ivIconSuccess = (ImageView)findViewById(R.id.act_cis_iv_icon_success);
        ctvSuccessText = (CustomTextView)findViewById(R.id.act_cs_ctv_success_text);
        ctvMobileNumber = (CustomTextView)findViewById(R.id.act_cs_ctv_mobile_number);


        //Three different layout for success scenario.
        llCashInSuccess = (LinearLayout)findViewById(R.id.frag_ll_cash_in_success);
        llCashOutSuccess = (LinearLayout)findViewById(R.id.frag_ll_cash_out_success);
        llCashOutFailure = (LinearLayout)findViewById(R.id.frag_ll_cash_out_failure);

        //Used in Cash Out Success and Request Cash Success scenario
        ctvSuccessMessage = (CustomTextView)findViewById(R.id.act_cs_ctv_success_message);


        switch (getIntent().getStringExtra(Constants.TXN_TYPE)){
            case Constants.CASH_IN_SUCCESS:
                llCashInSuccess.setVisibility(View.VISIBLE);
                llCashOutSuccess.setVisibility(View.GONE);
                llCashOutFailure.setVisibility(View.GONE);

//                ctvMobileNumber.setText("for +250" + );
//                ctvMobileNumber.setVisibility(View.VISIBLE);


                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.TXN_ID))){
                    ctvTransactionId = (CustomTextView)findViewById(R.id.frag_cis_ctv_transaction_id);
                    ctvTransactionId.setText(getIntent().getStringExtra(Constants.TXN_ID));
                }

                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.TIME_STAMP))) {
                    ctvDate = (CustomTextView) findViewById(R.id.frag_cis_ctv_date);
                    ctvDate.setText(getIntent().getStringExtra(Constants.TIME_STAMP) +" "+getString(R.string.hrs));
                }

                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.AMOUNT_VALUE))) {
                    ctvAmount = (CustomTextView) findViewById(R.id.frag_cis_ctv_amount);
                    ctvAmount.setText(getString(R.string.rwf)+" "+getIntent().getStringExtra(Constants.AMOUNT_VALUE));
                }

                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.FEES))) {
                    ctvFees = (CustomTextView) findViewById(R.id.frag_cis_ctv_fees);
                    ctvFees.setText(getString(R.string.rwf)+" "+getIntent().getStringExtra(Constants.FEES));
                }



                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.TOTAL_AMOUNT))) {
                    ctvTotalAmount = (CustomTextView) findViewById(R.id.frag_cis_ctv_total_amount);
                    ctvTotalAmount.setText(getString(R.string.rwf)+" "+getIntent().getStringExtra(Constants.TOTAL_AMOUNT));
                }

                break;

            case Constants.CASH_OUT_SUCCESS :
                llCashInSuccess.setVisibility(View.GONE);
                llCashOutSuccess.setVisibility(View.VISIBLE);
                llCashOutFailure.setVisibility(View.GONE);


                ctvSuccessText.setText(getString(R.string.text_float_request_successful));

                ctvSuccessMessage.setText(getString(R.string.float_request_done_successfully));


                //LogUtils.Verbose(Constants.TXN_ID,getIntent().getStringExtra(Constants.TXN_ID));
                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.TXN_ID))){
                    ctvTransactionId = (CustomTextView)findViewById(R.id.frag_cos_ctv_transaction_id);
                    ctvTransactionId.setText(getIntent().getStringExtra(Constants.TXN_ID));
                }

                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.TIME_STAMP))) {
                    ctvDate = (CustomTextView) findViewById(R.id.frag_cos_ctv_date);
                    ctvDate.setText(getIntent().getStringExtra(Constants.TIME_STAMP)  +" "+getString(R.string.hrs));
                }


                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.MVISA_ID))) {
                    ctvCardNumber = (CustomTextView) findViewById(R.id.frag_cos_ctv_card_number);
                    ctvCardNumber.setText(getIntent().getStringExtra(Constants.MVISA_ID));
                }


                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.AMOUNT_VALUE))) {
                    ctvTotalAmount = (CustomTextView) findViewById(R.id.frag_cos_ctv_total_amount);
                    ctvTotalAmount.setText(getString(R.string.rwf)+" "+getIntent().getStringExtra(Constants.AMOUNT_VALUE));
                }

                break;



            case Constants.CASH_OUT_FAILURE:
                llCashInSuccess.setVisibility(View.GONE);
                llCashOutSuccess.setVisibility(View.GONE);
                llCashOutFailure.setVisibility(View.VISIBLE);


                rlForIconandMessage.setBackgroundColor(ContextCompat.getColor(CashInSuccessActivity.this,R.color.greyColor));
                ivIconSuccess.setImageResource(R.drawable.icon_info_blue);
                ctvSuccessText.setText(getString(R.string.text_flaot_request_declined));
                ctvSuccessText.setTextColor(ContextCompat.getColor(CashInSuccessActivity.this,R.color.black));


                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.MVISA_ID))) {
                    ctvCardNumber = (CustomTextView) findViewById(R.id.frag_cof_ctv_card_number);
                    ctvCardNumber.setText(getIntent().getStringExtra(Constants.MVISA_ID));
                }


                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.AMOUNT_VALUE))) {
                    ctvTotalAmount = (CustomTextView) findViewById(R.id.frag_cof_ctv_total_amount);
                    ctvTotalAmount.setText(getString(R.string.rwf)+" "+getIntent().getStringExtra(Constants.AMOUNT_VALUE));
                }

                break;


            case Constants.REQUEST_CASH :
                llCashInSuccess.setVisibility(View.GONE);
                llCashOutSuccess.setVisibility(View.VISIBLE);
                llCashOutFailure.setVisibility(View.GONE);


                ctvSuccessText.setText(getString(R.string.text_request_cash_successful));

                ctvSuccessMessage.setText(getString(R.string.request_cash_done_successfully));

                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.TXN_ID))){
                    ctvTransactionId = (CustomTextView)findViewById(R.id.frag_cos_ctv_transaction_id);
                    ctvTransactionId.setText(getIntent().getStringExtra(Constants.TXN_ID));
                }

                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.TIME_STAMP))) {
                    ctvDate = (CustomTextView) findViewById(R.id.frag_cos_ctv_date);
                    ctvDate.setText(getIntent().getStringExtra(Constants.TIME_STAMP) +" "+getString(R.string.hrs));
                }


                ctvCardNumberText = (CustomTextView)findViewById(R.id.frag_cos_ctv_card_number_text);
                ctvCardNumberText.setText(getString(R.string.super_agent_mvisa_id));


                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.MVISA_ID))) {
                    ctvCardNumber = (CustomTextView) findViewById(R.id.frag_cos_ctv_card_number);
                    ctvCardNumber.setText(getIntent().getStringExtra(Constants.MVISA_ID));
                }


                if(!TextUtils.isEmpty(getIntent().getStringExtra(Constants.AMOUNT_VALUE))) {
                    ctvTotalAmount = (CustomTextView) findViewById(R.id.frag_cos_ctv_total_amount);
                    ctvTotalAmount.setText(getString(R.string.rwf)+" "+getIntent().getStringExtra(Constants.AMOUNT_VALUE));
                }

                break;

        }



        bBackToDashBoard = (Button) findViewById(R.id.act_cis_b_back_to_dashboard);
        bBackToDashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDashboardActivity();
            }
        });




    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        startDashboardActivity();
    }

    void startDashboardActivity() {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

}
