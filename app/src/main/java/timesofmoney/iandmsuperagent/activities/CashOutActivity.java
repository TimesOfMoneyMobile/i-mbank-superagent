package timesofmoney.iandmsuperagent.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.adapters.CashOutListAdapter;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.models.CashoutResponse;
import timesofmoney.iandmsuperagent.models.WithFloatRequest;
import timesofmoney.iandmsuperagent.models.WithFloatResponse;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.LogUtils;

/**
 * Created by vishwanathp on 3/5/17.
 */
public class CashOutActivity extends BaseActivity {

    ListView lvCashOut;
    LinearLayout llForIconAndMessage;
    ImageView ivInfoIcon;
    CustomTextView ctvMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_out);

        setToolbar(getString(R.string.menu_authorize_float));

        setFooter(false,"","");

        lvCashOut = (ListView) findViewById(R.id.act_co_lv_for_cash_out);

        llForIconAndMessage = (LinearLayout)findViewById(R.id.act_co_ll_for_icon_message);
        ivInfoIcon = (ImageView)findViewById(R.id.act_co_iv_info_icon);
        ctvMessage = (CustomTextView)findViewById(R.id.act_co_ctv_message);


        showProgress();
        getCashOutListing();

    }


    private void getCashOutListing() {

        String xmlParam = "<MobileNumber>" + IMBAgentApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<SuperAgentId>" + IMBAgentApp.getInstance().getAgentID() + "</SuperAgentId>\n" +
                "\t\t<WalletId>" + "" + "</WalletId>";



        xmlParam = Constants.getReqXML("ListWithFloatRequests", xmlParam);

        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            new OkDialog(CashOutActivity.this, e.toString(), null, null);
            return;
        }


        RetrofitTask.getInstance().executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                dismissProgress();

                if (!isSuccess) {
                    new OkDialog(CashOutActivity.this, response, null, null);
                    return;
                }

                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);
                    Serializer serializer = new Persister();

                    WithFloatResponse withFloatResponse = serializer.read(WithFloatResponse.class,decrypted,false);

                    if (withFloatResponse.getResponse().getResponseType().equals("Success")) {
                        lvCashOut.setAdapter(new CashOutListAdapter(CashOutActivity.this, withFloatResponse.getResponseDetails().getWithFloatRequests().getWithFloatRequestList()));
                        // cashoutFragment.DrawList();
                    } else{
                        lvCashOut.setVisibility(View.GONE);
                        llForIconAndMessage.setVisibility(View.VISIBLE);
                    }
                    //new OkDialog(CashOutActivity.this, cashoutResponse.getResponseDetails().getReason(), null, null);




                }catch (Exception e) {
                    e.printStackTrace();
                    lvCashOut.setVisibility(View.GONE);
                    llForIconAndMessage.setVisibility(View.VISIBLE);
                    ivInfoIcon.setVisibility(View.GONE);
                    ctvMessage.setText(e.getMessage());
                   // new OkDialog(CashOutActivity.this, e.getMessage(), null, null);
                }


            }
        });

    }


}
