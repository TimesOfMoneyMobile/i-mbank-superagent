package timesofmoney.iandmsuperagent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.customviews.FloatLabeledEditText;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.LogUtils;

import static timesofmoney.iandmsuperagent.utilities.Constants.getHash;

/**
 * Created by vishwanathp on 9/5/17.
 */
public class CashOutMpinActivity extends BaseActivity implements View.OnClickListener {


    LinearLayout llForData,llForOtpMessage;
    EditText etMPIN,etOTP;
    Button bConfirm;
    CustomTextView ctvCardNumber,ctvAmount;
    TextView ctvMessage;
    String requestStatus,strOTP = "", strMvisaId,strAmountValue,strTxnId,strMPIN;
    FloatLabeledEditText fletOTP;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_out_mpin);

        setToolbar(getString(R.string.menu_authorize_float));
        setFooter(false,"","");

        requestStatus = getIntent().getStringExtra(Constants.REQUEST_STATUS);
        strMvisaId = getIntent().getStringExtra(Constants.MVISA_ID);
        strAmountValue = getIntent().getStringExtra(Constants.AMOUNT_VALUE);
        strTxnId = getIntent().getStringExtra(Constants.TXN_ID);


        llForData = (LinearLayout)findViewById(R.id.frag_maf_ll_for_data);
        llForData.setVisibility(View.GONE);

        ctvCardNumber = (CustomTextView)findViewById(R.id.frag_conform_data_ctv_card_number);
        ctvCardNumber.setText(strMvisaId);
        ctvAmount = (CustomTextView)findViewById(R.id.frag_conform_data_ctv_amount);
        ctvAmount.setText(getString(R.string.rwf)+" "+Constants.formatAmount(strAmountValue));

//        ctvMessage = (TextView)findViewById(R.id.act_com_ctv_message);
//        ctvMessage.setText(Html.fromHtml(getString(R.string.accept_or_decline_cahout_request)));
//

        llForOtpMessage = (LinearLayout) findViewById(R.id.act_co_ll_for_otp_message);

        etMPIN = (EditText)findViewById(R.id.act_com_et_mpin);

        fletOTP = (FloatLabeledEditText) findViewById(R.id.act_com_flet_otp);
        etOTP = (EditText)findViewById(R.id.act_com_et_otp);

        bConfirm = (Button)findViewById(R.id.act_coo_b_confirm);
        bConfirm.setOnClickListener(this);

        if(requestStatus.equals(Constants.REJECT)){
            fletOTP.setVisibility(View.GONE);
            llForOtpMessage.setVisibility(View.GONE);
        }else{
            fletOTP.setVisibility(View.VISIBLE);
            llForOtpMessage.setVisibility(View.VISIBLE);
        }






    }

    @Override
    public void onClick(View v) {

        if(validate()){
          callAuthoriseApi();
        }

    }

    public void callAuthoriseApi() {

        String strMPINHash = "";

        if (IMBAgentApp.getInstance().getSalt() != null && !TextUtils.isEmpty(IMBAgentApp.getInstance().getSalt())) {
            strMPINHash = getHash(strMPIN, IMBAgentApp.getInstance().getSalt());
        } else {
            strMPINHash = Constants.getHash(strMPIN.getBytes());
        }


        String xmlParam = "\t\t<MobileNumber>"+ IMBAgentApp.getInstance().getMobileNumber()+"</MobileNumber>\n"+
                "\t\t<TxnId>"+strTxnId+"</TxnId>\n"+

                "\t\t<OTP>"+strOTP+"</OTP>\n"+
                "\t\t<Tpin>"+strMPINHash+"</Tpin>\n"+
                "\t\t<WalletId></WalletId>\n"+
                "\t\t<Status>"+requestStatus+"</Status>";

        //     "\t\t<Amount>"+strAmountValue+"</Amount>\n"+

        xmlParam = Constants.getReqXML("AuthWithFloat", xmlParam);

        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Constants.showToast(this, e.toString());
            return;
        }

        showProgress();
        RetrofitTask.getInstance().executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {
                dismissProgress();

                if (!isSuccess) {
                    new OkDialog(CashOutMpinActivity.this, response, null, null);
                    return;
                }


                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);

                    final Map<String, String> responseMap = parseResponse(decrypted);

                    if (responseMap.get("ResponseType").equals("Success")) {

                        Intent intent = new Intent(CashOutMpinActivity.this,CashInSuccessActivity.class);
                        if(requestStatus.equals(Constants.REJECT)){

                            intent.putExtra(Constants.TXN_TYPE,Constants.CASH_OUT_FAILURE);
                            intent.putExtra(Constants.MVISA_ID, strMvisaId);
                            intent.putExtra(Constants.AMOUNT_VALUE,Constants.formatAmount(strAmountValue));

                        }else{
                            intent.putExtra(Constants.TXN_TYPE,Constants.CASH_OUT_SUCCESS);
                            intent.putExtra(Constants.TXN_ID,responseMap.get("TxnId"));
                            intent.putExtra(Constants.MVISA_ID, strMvisaId);
                            intent.putExtra(Constants.AMOUNT_VALUE,Constants.formatAmount(strAmountValue));
                            intent.putExtra(Constants.TIME_STAMP, Constants.getFormattedDate(responseMap.get("Timestamp"), "dd-MM-yy hh:mm:ss", "dd MMM'' yyyy | HH:mm"));
                            IMBAgentApp.getInstance().setBalance(Constants.formatAmount(responseMap.get("Balance")));
                        }



                        startActivity(intent);

                    } else {
                        new OkDialog(CashOutMpinActivity.this, responseMap.get("Reason"), null, null);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    new OkDialog(CashOutMpinActivity.this, e.getMessage(), null, null);
                }


            }
        });


    }



    private Map<String, String> parseResponse(String xml) {

        Map<String, String> responseMap = new HashMap<>();

        try {

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new StringReader(xml));
            int eventType = xpp.getEventType();
            String text = "";

            while (eventType != XmlPullParser.END_DOCUMENT) {

                String tagname = xpp.getName();

                if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();

                } else if (eventType == XmlPullParser.END_TAG) {
                    if (tagname.equalsIgnoreCase("ResponseType")) {
                        responseMap.put("ResponseType", text);
                    }
                    if (tagname.equalsIgnoreCase("Message"))
                        responseMap.put("Message", text);


                    if (tagname.equalsIgnoreCase("TxnId"))
                        responseMap.put("TxnId", text);
                    if (tagname.equalsIgnoreCase("ERN"))
                        responseMap.put("ERN", text);
                    if (tagname.equalsIgnoreCase("Amount"))
                        responseMap.put("Amount", text);
                    if (tagname.equalsIgnoreCase("Fee"))
                        responseMap.put("Fee", text);
                    if (tagname.equalsIgnoreCase("Balance"))
                        responseMap.put("Balance", text);
                    if (tagname.equalsIgnoreCase("SuperAgentId"))
                        responseMap.put("SuperAgentId", text);
                    if (tagname.equalsIgnoreCase("OTP"))
                        responseMap.put("OTP", text);
                    if (tagname.equalsIgnoreCase("Recipient"))
                        responseMap.put("Recipient", text);
                    if (tagname.equalsIgnoreCase("Timestamp"))
                        responseMap.put("Timestamp", text);
                    if (tagname.equalsIgnoreCase("ErrorCode"))
                        responseMap.put("ErrorCode", text);
                    if (tagname.equalsIgnoreCase("Reason"))
                        responseMap.put("Reason", text);

                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return responseMap;
    }




    private boolean validate() {


        if(fletOTP.getVisibility() == View.VISIBLE){
            strOTP = etOTP.getText().toString();

            if (strOTP.trim().length() == 0) {
                new OkDialog(this, getString(R.string.errorOTP), null, null);
                return false;
            }

            if (strOTP.trim().length() != getResources().getInteger(R.integer.OTPLength)) {
                new OkDialog(this, getString(R.string.errorOTPSixDigit), null, null);
                return false;
            }
        }

        strMPIN = etMPIN.getText().toString();

        if (strMPIN.trim().length() == 0) {
            new OkDialog(this, getString(R.string.blankerrorMpin), null, null);
            return false;
        }

        if (strMPIN.trim().length() != getResources().getInteger(R.integer.MPINLength)) {
            new OkDialog(this, getString(R.string.errorMpin), null, null);
            return false;
        }

        return true;
    }





}
