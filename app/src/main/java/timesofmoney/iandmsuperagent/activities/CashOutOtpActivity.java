package timesofmoney.iandmsuperagent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.utilities.Constants;

/**
 * Created by vishwanathp on 9/5/17.
 */
public class CashOutOtpActivity extends BaseActivity{

    LinearLayout llForData;
    Button bConfirm;
    EditText etOTP;
    private String strCardNumber,strAmountValue,strTxnId,strRequestStatus;

    CashOutMpinActivity cashOutMpinActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_out_otp);

        setToolbar(getString(R.string.menu_cash_out));
        setFooter(false,"","");
        cashOutMpinActivity = new CashOutMpinActivity();


        llForData = (LinearLayout)findViewById(R.id.frag_maf_ll_for_data);
        llForData.setVisibility(View.GONE);

        //strRequestStatus = getIntent().getStringExtra(Constants.REQUEST_STATUS);
        strCardNumber = getIntent().getStringExtra(Constants.CARD_NUMBER);
        strAmountValue = getIntent().getStringExtra(Constants.AMOUNT_VALUE);
        strTxnId = getIntent().getStringExtra(Constants.TXN_ID);

        etOTP = (EditText)findViewById(R.id.act_coo_et_otp);
        bConfirm = (Button)findViewById(R.id.act_coo_b_confirm);
        bConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){

                   // cashOutMpinActivity.callAuthoriseApi(strRequestStatus,etOTP.getText().toString());

                    Intent intent = new Intent(CashOutOtpActivity.this,CashOutMpinActivity.class);
                    intent.putExtra(Constants.CARD_NUMBER,strCardNumber);
                    intent.putExtra(Constants.TXN_ID,strTxnId);
                    intent.putExtra(Constants.AMOUNT_VALUE,strAmountValue);
                    intent.putExtra(Constants.OTP,etOTP.getText().toString());
                    startActivity(intent);
                }
            }
        });




    }

    private boolean validate() {

       String strOTP = etOTP.getText().toString();

        if (strOTP.trim().length() == 0) {
            new OkDialog(this, getString(R.string.errorOTP), null, null);
            return false;
        }

        if (strOTP.trim().length() != getResources().getInteger(R.integer.OTPLength)) {
            new OkDialog(this, getString(R.string.errorOTPSixDigit), null, null);
            return false;
        }

        return true;
    }
}
