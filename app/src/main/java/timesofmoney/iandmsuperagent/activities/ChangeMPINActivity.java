package timesofmoney.iandmsuperagent.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Map;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.customviews.CustomEditText;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.dialogs.ProgressDialog;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.ErrorUtils;
import timesofmoney.iandmsuperagent.utilities.GenericResponseHandler;
import timesofmoney.iandmsuperagent.utilities.LogUtils;

import static timesofmoney.iandmsuperagent.utilities.Constants.getHash;

public class ChangeMPINActivity extends BaseActivity {


    Button btnChangeMPIN;
    String oldMpin, newMpin, confirmMpin;
    ArrayList<String> pinlist, NewList, ReList;
    CustomEditText edtPIN1, edtPIN2, edtPIN3, edtPIN4, edtPIN5, edtPIN6, edtNewPIN1, edtNewPIN2, edtNewPIN3, edtNewPIN4, edtNewPIN5, edtNewPIN6, edtRePIN1, edtRePIN2, edtRePIN3, edtRePIN4, edtRePIN5, edtRePIN6;
    String placeHolder = "A";
    String strChangeMPIN = "N";
    EditText etExistingMPIN, etNewMPIN, etConfirmMPIN;
    View view3, view1, view2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mpin);

        setToolbar(getString(R.string.change_mpin));


        btnChangeMPIN = (Button) findViewById(R.id.btnChangeMPIN);



        etExistingMPIN = (EditText) findViewById(R.id.act_cmpin_et_existing_mpin);
        etNewMPIN = (EditText) findViewById(R.id.act_cmpin_et_new_mpin);
        etConfirmMPIN = (EditText) findViewById(R.id.act_cmpin_et_confirm_mpin);

        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);
        view3 = findViewById(R.id.view3);

        etExistingMPIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(etExistingMPIN.getText().length() == getResources().getInteger(R.integer.MPINLength)){
                   // etNewMPIN.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        etNewMPIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(etNewMPIN.getText().length() == getResources().getInteger(R.integer.MPINLength)){
                   // etConfirmMPIN.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });




//        pinlist = new ArrayList<>();
//        pinlist.add(placeHolder);
//        pinlist.add(placeHolder);
//        pinlist.add(placeHolder);
//        pinlist.add(placeHolder);
//        pinlist.add(placeHolder);
//        pinlist.add(placeHolder);
//
//        NewList = new ArrayList<>();
//        NewList.add(placeHolder);
//        NewList.add(placeHolder);
//        NewList.add(placeHolder);
//        NewList.add(placeHolder);
//        NewList.add(placeHolder);
//        NewList.add(placeHolder);
//
//        ReList = new ArrayList<>();
//        ReList.add(placeHolder);
//        ReList.add(placeHolder);
//        ReList.add(placeHolder);
//        ReList.add(placeHolder);
//        ReList.add(placeHolder);
//        ReList.add(placeHolder);
//
//        edtPIN1 = (CustomEditText) findViewById(R.id.edtPin1);
//        edtPIN2 = (CustomEditText) findViewById(R.id.edtPin2);
//        edtPIN3 = (CustomEditText) findViewById(R.id.edtPin3);
//        edtPIN4 = (CustomEditText) findViewById(R.id.edtPin4);
//        edtPIN5 = (CustomEditText) findViewById(R.id.edtPin5);
//        edtPIN6 = (CustomEditText) findViewById(R.id.edtPin6);
//        edtNewPIN1 = (CustomEditText) findViewById(R.id.edtNewPin1);
//        edtNewPIN2 = (CustomEditText) findViewById(R.id.edtNewPin2);
//        edtNewPIN3 = (CustomEditText) findViewById(R.id.edtNewPin3);
//        edtNewPIN4 = (CustomEditText) findViewById(R.id.edtNewPin4);
//        edtNewPIN5 = (CustomEditText) findViewById(R.id.edtNewPin5);
//        edtNewPIN6 = (CustomEditText) findViewById(R.id.edtNewPin6);
//        edtRePIN1 = (CustomEditText) findViewById(R.id.edtRePin1);
//        edtRePIN2 = (CustomEditText) findViewById(R.id.edtRePin2);
//        edtRePIN3 = (CustomEditText) findViewById(R.id.edtRePin3);
//        edtRePIN4 = (CustomEditText) findViewById(R.id.edtRePin4);
//        edtRePIN5 = (CustomEditText) findViewById(R.id.edtRePin5);
//        edtRePIN6 = (CustomEditText) findViewById(R.id.edtRePin6);

        if (getIntent().hasExtra("changepin")) {
            strChangeMPIN = getIntent().getStringExtra("changepin");
        }


        btnChangeMPIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Constants.isNetworkAvailable(ChangeMPINActivity.this)) {
                    new OkDialog(ChangeMPINActivity.this, getString(R.string.errorNetwork), "", null);
                    return;
                }
                if (validate())
                    callAPI();

            }
        });

//        edtPIN1.setCustomTextWatcher(new CustomEditWatcher(edtPIN1, edtPIN2.getEditText(), null, 1, 0, pinlist));
//        edtPIN2.setCustomTextWatcher(new CustomEditWatcher(edtPIN2, edtPIN3.getEditText(), edtPIN1.getEditText(), 1, 1, pinlist));
//        edtPIN3.setCustomTextWatcher(new CustomEditWatcher(edtPIN3, edtPIN4.getEditText(), edtPIN2.getEditText(), 1, 2, pinlist));
//        edtPIN4.setCustomTextWatcher(new CustomEditWatcher(edtPIN4, edtPIN5.getEditText(), edtPIN3.getEditText(), 1, 3, pinlist));
//        edtPIN5.setCustomTextWatcher(new CustomEditWatcher(edtPIN5, edtPIN6.getEditText(), edtPIN4.getEditText(), 1, 4, pinlist));
//        edtPIN6.setCustomTextWatcher(new CustomEditWatcher(edtPIN6, edtNewPIN1.getEditText(), edtPIN5.getEditText(), 1, 5, pinlist));
//
//
//        edtNewPIN1.setCustomTextWatcher(new CustomEditWatcher(edtNewPIN1, edtNewPIN2.getEditText(), null, 1, 0, NewList));
//        edtNewPIN2.setCustomTextWatcher(new CustomEditWatcher(edtNewPIN2, edtNewPIN3.getEditText(), edtNewPIN1.getEditText(), 1, 1, NewList));
//        edtNewPIN3.setCustomTextWatcher(new CustomEditWatcher(edtNewPIN3, edtNewPIN4.getEditText(), edtNewPIN2.getEditText(), 1, 2, NewList));
//        edtNewPIN4.setCustomTextWatcher(new CustomEditWatcher(edtNewPIN4, edtNewPIN5.getEditText(), edtNewPIN3.getEditText(), 1, 3, NewList));
//        edtNewPIN5.setCustomTextWatcher(new CustomEditWatcher(edtNewPIN5, edtNewPIN6.getEditText(), edtNewPIN4.getEditText(), 1, 4, NewList));
//        edtNewPIN6.setCustomTextWatcher(new CustomEditWatcher(edtNewPIN6, edtRePIN1.getEditText(), edtNewPIN5.getEditText(), 1, 5, NewList));
//
//
//        edtRePIN1.setCustomTextWatcher(new CustomEditWatcher(edtRePIN1, edtRePIN2.getEditText(), null, 1, 0, ReList));
//        edtRePIN2.setCustomTextWatcher(new CustomEditWatcher(edtRePIN2, edtRePIN3.getEditText(), edtRePIN1.getEditText(), 1, 1, ReList));
//        edtRePIN3.setCustomTextWatcher(new CustomEditWatcher(edtRePIN3, edtRePIN4.getEditText(), edtRePIN2.getEditText(), 1, 2, ReList));
//        edtRePIN4.setCustomTextWatcher(new CustomEditWatcher(edtRePIN4, edtRePIN5.getEditText(), edtRePIN3.getEditText(), 1, 3, ReList));
//        edtRePIN5.setCustomTextWatcher(new CustomEditWatcher(edtRePIN5, edtRePIN6.getEditText(), edtRePIN4.getEditText(), 1, 4, ReList));
//        edtRePIN6.setCustomTextWatcher(new CustomEditWatcher(edtRePIN6, null, edtRePIN5.getEditText(), 1, 5, ReList));


    }





    private boolean validate() {

//        oldMpin = pinlist.get(0) + "" + pinlist.get(1) + "" + pinlist.get(2) + "" + pinlist.get(3) + "" + pinlist.get(4) + "" + pinlist.get(5);
//        newMpin = NewList.get(0) + "" + NewList.get(1) + "" + NewList.get(2) + "" + NewList.get(3) + "" + NewList.get(4) + "" + NewList.get(5);
//        confirmMpin = ReList.get(0) + "" + ReList.get(1) + "" + ReList.get(2) + "" + ReList.get(3) + "" + ReList.get(4) + "" + ReList.get(5);
//        LogUtils.Verbose("MPIN", newMpin + "  " + confirmMpin + "  " + oldMpin);

        oldMpin = etExistingMPIN.getText().toString();
        newMpin = etNewMPIN.getText().toString();
        confirmMpin = etConfirmMPIN.getText().toString();

        if (oldMpin.equals("")) {
            new OkDialog(this, getString(R.string.erroroldmpin), null, null);
            return false;
        }else if (oldMpin.contains(" ") || oldMpin.length() < getResources().getInteger(R.integer.MPINLength)) {
            new OkDialog(this, getString(R.string.oldmpinerror), null, null);
            return false;
        }else if (newMpin.equals("")) {
            new OkDialog(this, getString(R.string.errornewmpin), null, null);
            return false;
        }else if (newMpin.contains(" ") || newMpin.length() < getResources().getInteger(R.integer.MPINLength)) {
            new OkDialog(this, getString(R.string.newmpinerror), null, null);
            return false;
        }else  if (!Constants.isValidMPIN(newMpin)) {
            new OkDialog(this, getString(R.string.errorEasyMpin), null, null);
            return false;
        }else if (oldMpin.equals(newMpin)) {
             new OkDialog(this, getString(R.string.old_new_mpin_can_not_be_same), null, null);
            return false;
        } else if (confirmMpin.equals("")) {
            new OkDialog(this, getString(R.string.errorconfirmmpin), null, null);
            return false;
        }else if (confirmMpin.contains(" ") || confirmMpin.length() < getResources().getInteger(R.integer.MPINLength)) {
            new OkDialog(this, getString(R.string.please_enter_valid_confirm_mpin), null, null);
            return false;
        }else  if (!confirmMpin.equals(newMpin)) {
            new OkDialog(this, getString(R.string.errormpinnotconfirm), null, null);
            return false;
        }



//        if (oldMpin.equals("AAAAAA")) {
//            new OkDialog(this, getString(R.string.erroroldmpin), null, null);
//            return false;
//        } else if (oldMpin.contains("A") || oldMpin.length() < 6) {
//            new OkDialog(this, getString(R.string.oldmpinerror), null, null);
//            return false;
//        } else if (newMpin.equals("AAAAAA")) {
//            new OkDialog(this, getString(R.string.errornewmpin), null, null);
//            return false;
//        } else if (newMpin.contains("A") || newMpin.length() < 6) {
//            new OkDialog(this, getString(R.string.newmpinerror), null, null);
//            return false;
//        } else if (confirmMpin.equals("AAAAAA")) {
//            new OkDialog(this, getString(R.string.errorconfirmmpin), null, null);
//            return false;
//        } else if (confirmMpin.contains("A") || confirmMpin.length() < 6) {
//            new OkDialog(this, getString(R.string.please_enter_valid_confirm_mpin), null, null);
//            return false;
//        } else if (!Constants.isValidMPIN(newMpin)) {
//            new OkDialog(this, getString(R.string.errorEasyMpin), null, null);
//            return false;
//        } else if (oldMpin.equals(newMpin)) {
//            new OkDialog(this, getString(R.string.old_new_mpin_can_not_be_same), null, null);
//            return false;
//        } else if (!confirmMpin.equals(newMpin)) {
//            new OkDialog(this, getString(R.string.errormpinnotconfirm), null, null);
//            return false;
//        }


        return true;
    }


    private void callAPI() {

        String strOldMPINHash = "";

        if (IMBAgentApp.getInstance().getSalt() != null && !TextUtils.isEmpty(IMBAgentApp.getInstance().getSalt())) {
            strOldMPINHash = getHash(oldMpin, IMBAgentApp.getInstance().getSalt());
        } else {
            strOldMPINHash = Constants.getHash(oldMpin.getBytes());
        }


        String strNewMPINHash = "";

        if (IMBAgentApp.getInstance().getSalt() != null && !TextUtils.isEmpty(IMBAgentApp.getInstance().getSalt())) {
            strNewMPINHash = getHash(newMpin, IMBAgentApp.getInstance().getSalt());
        } else {
            strNewMPINHash = Constants.getHash(newMpin.getBytes());
        }


        String xmlParam = "\t\t<MobileNumber>" + IMBAgentApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<OldMpin>" + strOldMPINHash + "</OldMpin>\n" +
                "\t\t<NewMpin>" + strNewMPINHash + "</NewMpin>";

        xmlParam = Constants.getReqXML("ChangeMpin", xmlParam);

        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Constants.showToast(this, e.toString());
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(((Activity) this), this.getString(R.string.msgWait));
        progressDialog.show();
        RetrofitTask retrofitTask = RetrofitTask.getInstance();

        retrofitTask.executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                progressDialog.dismiss();

                if (!isSuccess) {
                    new OkDialog(ChangeMPINActivity.this, response, null, null);
                    return;
                }

                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);
                    Map<String, String> responseMap = GenericResponseHandler.handleResponse(decrypted);

                    if (responseMap.get("ResponseType").equals("Success")) {

                        new OkDialog(ChangeMPINActivity.this, getString(R.string.your_mpin_changed_successfully), getString(R.string.alert_title), new OkDialog.IOkDialogCallback() {
                            @Override
                            public void handleResponse() {
                                finish();
                            }
                        });

                    } else {
                        new OkDialog(ChangeMPINActivity.this, ErrorUtils.getErrorMessage(responseMap.get("ErrorCode"), responseMap.get("Reason")), null, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    new OkDialog(ChangeMPINActivity.this, e.getMessage(), null, null);
                }

            }
        });
    }

    @Override
    public void onBackPressed() {

        if (strChangeMPIN.equalsIgnoreCase("Y")) {
            Intent intent = new Intent(ChangeMPINActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            // ActivityCompat.finishAffinity(NavigationActivity.this);
            finish();
        } else {

            super.onBackPressed();
        }
    }
}
