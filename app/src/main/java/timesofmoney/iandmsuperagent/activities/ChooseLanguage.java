package timesofmoney.iandmsuperagent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import timesofmoney.iandmsuperagent.BuildConfig;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.LogUtils;


public class ChooseLanguage extends BaseActivity implements View.OnClickListener {

    CustomTextView txtEnglish, txtArebic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Constants.changeLanguage(AppSettings.getData(this, AppSettings.APPLANGUAGE), this);
        setContentView(R.layout.activity_splash);

        txtEnglish = (CustomTextView) findViewById(R.id.act_splash_txt_english);
        txtArebic = (CustomTextView) findViewById(R.id.act_splash_txt_rawanda);

        txtEnglish.setOnClickListener(this);
        txtArebic.setOnClickListener(this);

        //Check device is rooted or not

        /*if (Constants.isDeviceRooted()) {
            Constants.showToast(this, "Device Rooted!!!");
        } else {
            Constants.showToast(this, "Device Not Rooted!!!");
        }*/

        if (Constants.isDeviceRooted() && !BuildConfig.DEBUG) {
            new OkDialog(this, getString(R.string.rooted_device_error), getString(R.string.alert_title), new OkDialog.IOkDialogCallback() {
                @Override
                public void handleResponse() {
                    finish();
                }
            });

        }





       /* if(!TextUtils.isEmpty(AppSettings.getData(this,AppSettings.APPLANGUAGE)))
        {
            Constants.changeLanguage(AppSettings.getData(this,AppSettings.APPLANGUAGE),this);
            Intent intent=new Intent(this,LoginActivity.class);
            startActivity(intent);
            finish();
        }*/
    }

    @Override
    public void onClick(View v) {

        String selectedLanguage = "";

        if (v == txtEnglish)
            selectedLanguage = Constants.EN_LANGUAGE_CODE;

        if (v == txtArebic)
            selectedLanguage = Constants.RW_LANGUAGE_CODE;

        LogUtils.Verbose("Lang", selectedLanguage);

        AppSettings.putData(this, AppSettings.APPLANGUAGE, selectedLanguage);

        Constants.changeLanguage(selectedLanguage, this);

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
