package timesofmoney.iandmsuperagent.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.simonvt.menudrawer.MenuDrawer;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.customviews.MenuDrawerView;

import timesofmoney.iandmsuperagent.services.MyFirebaseInstanceIDService;
import timesofmoney.iandmsuperagent.utilities.AnimationUtils;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.Constants;

public class DashboardActivity extends BaseActivity {

    MenuDrawer menuDrawer;


    RelativeLayout rlForIcon;
    CustomTextView txtvwComapnyName, txtvwmVisaID;
    RelativeLayout layout_notification;
    LinearLayout layout_generateQr;
    CustomTextView txtNotificationCount;
    ImageView imageViewRefresh, ivHamIcon, ivCrossIcon;
    FrameLayout recentFrame;
    AnimationUtils rotateanimation;

    CustomTextView count, balance,ctvAgentName,ctvMvisaId,ctvCardBalance,ctvCommission;
    String refresh = "true";
    TextView viewAllTransactions;

    IntentFilter intentFilter;
    MessageBroadcast messageBroadcast;
    boolean checForHamIcon = false;
    //public static boolean bIsFromDashboardActivity;

    LinearLayout llAuthorizeFloat,llSettings, llCashIn,llBillPay,llTransactionStatement;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_dashboard);



        menuDrawer = MenuDrawer.attach(this, MenuDrawer.MENU_DRAG_WINDOW);
        menuDrawer.setContentView(R.layout.activity_dashboard);
        menuDrawer.setMenuView(R.layout.menu_list);

        setFooter(true,"","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        rlForIcon = (RelativeLayout) toolbar.findViewById(R.id.toolbar_dashboard_rl_for_icon);
        rlForIcon.setVisibility(View.VISIBLE);
        ivHamIcon = (ImageView)findViewById(R.id.toolbar_dashboard_iv_ham_icon);
        ivCrossIcon = (ImageView)findViewById(R.id.toolbar_dashboard_iv_cross_icon);

        layout_notification = (RelativeLayout) findViewById(R.id.toolbar_dashboard_rl_for_notification);
        txtNotificationCount = (CustomTextView) findViewById(R.id.toolbar_dashboard_ctv_notification_count);

        ctvAgentName = (CustomTextView) findViewById(R.id.toolbar_dashboard_ctv_agent_name);
        ctvMvisaId = (CustomTextView) findViewById(R.id.toolbar_dashboard_ctv_mvisa_id);

        if(!TextUtils.isEmpty(AppSettings.getData(getApplicationContext(),AppSettings.FIRST_NAME))){
            ctvAgentName.setText(AppSettings.getData(getApplicationContext(),AppSettings.FIRST_NAME)+" "+AppSettings.getData(getApplicationContext(),AppSettings.LAST_NAME));
        }


        // Displaying mVISA ID and Name here

        if (AppSettings.getData(DashboardActivity.this, AppSettings.APPLANGUAGE).equalsIgnoreCase(Constants.RW_LANGUAGE_CODE)) {
            if (IMBAgentApp.getInstance().getAgentID() != null) {
                ctvMvisaId.setText(getResources().getString(R.string.mvisa_id) + " " + IMBAgentApp.getInstance().getAgentID());
            }
        } else {
            if (IMBAgentApp.getInstance().getAgentID() != null) {
                ctvMvisaId.setText(getResources().getString(R.string.mvisa_id) + " " + IMBAgentApp.getInstance().getAgentID());
            }
        }





//        ctvCardBalance = (CustomTextView) findViewById(R.id.footer_dashboard_ctv_card_balance);
//        ctvCommission = (CustomTextView) findViewById(R.id.footer_dashboard_ctv_commission);
//
//        ctvCardBalance.setText(getString(R.string.rwf)+" "+IMBAgentApp.getInstance().getBalance());
//        ctvCommission.setText(getString(R.string.rwf)+" "+IMBAgentApp.getInstance().getCommission());

        llAuthorizeFloat = (LinearLayout)findViewById(R.id.act_dashboard_ll_authorize_float);
        llSettings = (LinearLayout)findViewById(R.id.act_dashboard_ll_settings);

        llTransactionStatement = (LinearLayout)findViewById(R.id.act_dashboard_ll_transaction_statement);



        llAuthorizeFloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.startNewActivity(DashboardActivity.this,CashOutActivity.class);

            }
        });

        llSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.startNewActivity(DashboardActivity.this,SettingsActivity.class);
            }
        });



        llTransactionStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.startNewActivity(DashboardActivity.this,TransactionStatementActivity.class);
            }
        });

        layout_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.startNewActivity(DashboardActivity.this,NotificationActivity.class);
            }
        });



        rlForIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuDrawer.toggleMenu();
            }
        });

        menuDrawer.setOnDrawerStateChangeListener(new MenuDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                if (menuDrawer.isMenuVisible()) {
                    // starts opening
                    ivHamIcon.setVisibility(View.VISIBLE);
                   // ivCrossIcon.setVisibility(View.VISIBLE);
                } else {
                    // closing drawer
                    ivHamIcon.setVisibility(View.VISIBLE);
                   // ivCrossIcon.setVisibility(View.GONE);
                }
            }
        });


        if (getIntent().hasExtra("changepin") && getIntent().getStringExtra("changepin").equalsIgnoreCase("Y")) {
            Intent intent = new Intent(getApplicationContext(), ChangeMPINActivity.class);
            intent.putExtra("changepin", getIntent().getStringExtra("changepin"));
            startActivity(intent);
        }


        new MenuDrawerView(this, findViewById(R.id.menu_root));


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                callBalanceInquiryAPI();
            }
        }, 50);





       // if (!AppSettings.getData(getApplicationContext(), AppSettings.isTOKENSENDTOSERVER).equals("true")) {
            Intent intent = new Intent(getApplicationContext(), MyFirebaseInstanceIDService.class);
            startService(intent);
        //}

//        viewAllTransactions.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Intent intent = new Intent(getApplicationContext(), TransactionHistoryActivity.class);
////                startActivity(intent);
//                fragment.clickList();
//            }
//        });

        intentFilter = new IntentFilter("message");
        messageBroadcast = new MessageBroadcast();
        registerReceiver(messageBroadcast, intentFilter);


    }


    @Override
    protected void onStart() {
        super.onStart();
        handleNotificationCount();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //bIsFromDashboardActivity = true;
    }

    @Override
    public void onStop() {
        super.onStop();

        if (menuDrawer.isMenuVisible()) {
            menuDrawer.closeMenu();

        }
    }

    @Override
    protected void onDestroy() {

        if (messageBroadcast != null) {
            unregisterReceiver(messageBroadcast);
        }

        super.onDestroy();
    }



//    public void startanim() {
//        rotateanimation = new AnimationUtils();
//        rotateanimation.rotateView(imageViewRefresh, 0, 360, imageViewRefresh.getWidth() / 2, imageViewRefresh.getHeight() / 2, null);
//    }
//
//    public void stopanim() {
//        if (rotateanimation != null) {
//            rotateanimation.stopRotateAnimation(imageViewRefresh);
//        }
//    }

    public void hideViewAllTransaction() {
        viewAllTransactions.setVisibility(View.GONE);
    }

    private void handleNotificationCount() {
        String c = (AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT).equals("")) ? "0" : AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT);
        int count = Integer.parseInt(c);
        if (count > 0) {
            txtNotificationCount.setText(count + "");
             txtNotificationCount.setVisibility(View.VISIBLE);
        } else{
             txtNotificationCount.setVisibility(View.GONE);

        }
    }

    class MessageBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            handleNotificationCount();

        }
    }


}

