package timesofmoney.iandmsuperagent.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Map;

import timesofmoney.iandmsuperagent.BuildConfig;
import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.dialogs.YesNoDialog;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.ErrorUtils;
import timesofmoney.iandmsuperagent.utilities.GenericResponseHandler;
import timesofmoney.iandmsuperagent.utilities.LogUtils;

public class ForgotMPINActivity extends BaseActivity {

    CustomTextView txtvwThanksMsg, txtvwGenerateNewMPIN;
    Button bGenerateMPIN;
    LinearLayout linlayGenerateMPINContainer;

    EditText etMobileNumber, etEmailId,etTaxIdentificationNumber;
    String strMobileNumber, strEmailId,strTaxIdentificationNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_mpin);

        setToolbar(getString(R.string.forgot_mpin));


        etMobileNumber = (EditText) findViewById(R.id.act_fmpin_et_mobile_number);
        etEmailId = (EditText) findViewById(R.id.act_fmpin_et_email_id);
        etTaxIdentificationNumber = (EditText) findViewById(R.id.act_fmpin_et_tax_identification_number);

        bGenerateMPIN = (Button) findViewById(R.id.act_fmpin_b_generate_mpin);

//        if (AppSettings.getData(getApplicationContext(), AppSettings.APPLANGUAGE).equals("ar")) {
//            txtvwGenerateNewMPIN.setGravity(Gravity.RIGHT);
//        }


        bGenerateMPIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Constants.isNetworkAvailable(ForgotMPINActivity.this)) {
                    new OkDialog(ForgotMPINActivity.this, getString(R.string.errorNetwork), null, null);
                    return;
                }
                if (validate()) {
                    getKey();

                }

               /* linlayGenerateMPINContainer.setVisibility(View.GONE);
                txtvwThanksMsg.setVisibility(View.VISIBLE);*/
            }
        });


    }

    private void getKey() {

        showProgress();
        // AppSettings.putData(this, AppSettings.MOBILE_NUMBER, strMobileNumber);

        IMBAgentApp.getInstance().setMobileNumber(strMobileNumber);


        RetrofitTask.getInstance().executeGetKey(etMobileNumber.getText().toString().trim(), new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (isSuccess) {
                    LogUtils.Verbose("TAG", " Key is " + response);
                    ArrayList<String> list = new ArrayList<>();
                    list.add("ResponseType");
                    list.add("key");
                    list.add("salt");
                    list.add("SecurityIv");


                    Map<String, String> responseMap = GenericResponseHandler.parseElements(response, list);

                    if (responseMap.get("ResponseType").equals("Success")) {
                        IMBAgentApp.getInstance().setKey(responseMap.get("key"));
                        IMBAgentApp.getInstance().setSalt(responseMap.get("salt"));
                        IMBAgentApp.getInstance().setIV(responseMap.get("SecurityIv"));
                        callAPI();
                    }
                } else {
                    dismissProgress();
                    new OkDialog(ForgotMPINActivity.this, response, "", null);
                }
            }
        });

    }


    private void getNewKey() {

        showProgress();
        // AppSettings.putData(this, AppSettings.MOBILE_NUMBER, strMobileNumber);

        IMBAgentApp.getInstance().setMobileNumber(strMobileNumber);


        String xmlParam = "\t\t<MobileNumber>" + strMobileNumber + "</MobileNumber>\n" +
                "\t\t<AppVersion>" + "A-"+ BuildConfig.VERSION_NAME   + "</AppVersion>";

        xmlParam = Constants.getReqXML("GetKey", xmlParam);


        RetrofitTask.getInstance().executeNewGetKey(etMobileNumber.getText().toString().trim(),xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (isSuccess) {



                    try {
                        LogUtils.Verbose("TAG", " Key is " + response);
                        ArrayList<String> list = new ArrayList<>();
                        list.add("ResponseType");
                        list.add("SecurityKey");
                        list.add("salt");
                        list.add("SecurityIv");
                        list.add("ErrorCode");
                        list.add("Reason");
                        list.add("AppUpdateFlag");


                        Map<String, String> responseMap = GenericResponseHandler.parseElements(response, list);

                        if (responseMap.get("ResponseType").equals("Success")) {
                            IMBAgentApp.getInstance().setKey(responseMap.get("SecurityKey"));
                            IMBAgentApp.getInstance().setSalt(responseMap.get("salt"));
                            IMBAgentApp.getInstance().setIV(responseMap.get("SecurityIv"));
                            if (responseMap.get("AppUpdateFlag").equals("O")) {
                                new YesNoDialog(ForgotMPINActivity.this, getString(R.string.optinal_update), getString(R.string.update), getString(R.string.skip), null, new YesNoDialog.IYesNoDialogCallback() {
                                    @Override
                                    public void handleResponse(int responsecode) {
                                        if (responsecode != 1) {
                                            callAPI();
                                        }
                                    }
                                });
                            } else {
                                callAPI();
                            }
                        } else {

                            dismissProgress();
                            if (responseMap.get("ErrorCode").equals(Constants.FORCEFULL_UPDATE_ERROR_CODE)) {
                                new OkDialog(ForgotMPINActivity.this, ErrorUtils.getErrorMessage(responseMap.get("ErrorCode"), responseMap.get("Reason")), Constants.FORCEFULL_UPDATE_ERROR_CODE, new OkDialog.IOkDialogCallback() {
                                    @Override
                                    public void handleResponse() {
                                        Constants.openPlayStore(ForgotMPINActivity.this);

                                        finish();
                                    }
                                });
                            } else
                                new OkDialog(ForgotMPINActivity.this, ErrorUtils.getErrorMessage(responseMap.get("ErrorCode"), responseMap.get("Reason")), null, null);
                        }

                    }catch (Exception e) {
                        e.printStackTrace();
                        dismissProgress();
                        new OkDialog(ForgotMPINActivity.this, getString(R.string.errorAPI), null, null);
                    }

                } else {
                    dismissProgress();
                    new OkDialog(ForgotMPINActivity.this, response, "", null);
                }
            }
        });

    }


    private void callAPI() {

        String xmlParam = "\t\t<MobileNumber>" + strMobileNumber.trim() + "</MobileNumber>\n" +
//                "\t\t<CitizenId>" + edtxtCitizenID.getText().toString().trim() + "</CitizenId>\n" +
                "\t\t<TIN>" + strTaxIdentificationNumber + "</TIN>\n" +
                "\t\t<PinCode>" + "" + "</PinCode>\n" +
                "\t\t<DeviceId>" + IMBAgentApp.getInstance().getIMEINo() + "</DeviceId>\n" +
                "\t\t<EmailId>" + strEmailId + "</EmailId>";

        xmlParam = Constants.getReqXML("ForgotMpin", xmlParam);

        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Constants.showToast(this, e.toString());
            return;
        }


        RetrofitTask retrofitTask = RetrofitTask.getInstance();

        retrofitTask.executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                dismissProgress();

                if (!isSuccess) {
                    new OkDialog(ForgotMPINActivity.this, response, null, null);
                    return;
                }

                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);
                    Map<String, String> responseMap = GenericResponseHandler.handleResponse(decrypted);

                    if (responseMap.get("ResponseType").equals("Success")) {

//                        Intent intent = new Intent(ForgotMPINActivity.this, ForgotMPINSuccess.class);
//                        startActivity(intent);
//                        finish();
                        new OkDialog(ForgotMPINActivity.this, getString(R.string.forgot_mpin_success), getString(R.string.alert_title), new OkDialog.IOkDialogCallback() {
                            @Override
                            public void handleResponse() {
                                finish();
                            }
                        });

                    } else {
                        new OkDialog(ForgotMPINActivity.this, ErrorUtils.getErrorMessage(responseMap.get("ErrorCode"), responseMap.get("Reason")), null, null);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    new OkDialog(ForgotMPINActivity.this, getString(R.string.please_try_again), null, null);
                }
            }
        });


    }

    private boolean validate() {
        strMobileNumber = etMobileNumber.getText().toString();
        strEmailId = etEmailId.getText().toString();
        strTaxIdentificationNumber = etTaxIdentificationNumber.getText().toString();

        if (strMobileNumber.trim().length() == 0) {
            new OkDialog(this, getString(R.string.errorMobileNumber), null, null);
            return false;
        } else if (strMobileNumber.trim().length() < getResources().getInteger(R.integer.mobileNoLength)) {
            new OkDialog(this, getString(R.string.errorMobileNumberTenDigit), null, null);
            return false;
        }
        else if (TextUtils.isEmpty(strEmailId)) {
            new OkDialog(this, getString(R.string.error_enter_email), null, null);
            return false;
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(strEmailId).matches()) {
            new OkDialog(this, getString(R.string.error_enter_valid_email), null, null);
            return false;
        }  else if (TextUtils.isEmpty(strTaxIdentificationNumber)) {
            new OkDialog(this, getString(R.string.error_enter_tax_identification_number), null, null);
            return false;
        }


        return true;
    }
}
