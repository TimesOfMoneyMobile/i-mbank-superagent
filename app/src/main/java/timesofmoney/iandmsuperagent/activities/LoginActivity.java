package timesofmoney.iandmsuperagent.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;
import java.util.Map;

import timesofmoney.iandmsuperagent.BuildConfig;
import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.customviews.LoginCustomEditLayout;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.dialogs.YesNoDialog;
import timesofmoney.iandmsuperagent.models.LoginResponse;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.ErrorUtils;
import timesofmoney.iandmsuperagent.utilities.GenericResponseHandler;
import timesofmoney.iandmsuperagent.utilities.LogUtils;

import static timesofmoney.iandmsuperagent.utilities.Constants.getHash;


public class LoginActivity extends BaseActivity {
    // CheckBox chkboxTermsAndConditions;
    ToggleButton toggleButton;
    CustomTextView tvTermsAndConditions;
    RelativeLayout rellayBottom;
    Button btnLogin;
    TextView tvForgotMPIN;
    LoginCustomEditLayout etMobile, etMPIN;
    CustomTextView changeIp;
    private String strMobileNumber, strMPIN;
    View mobileNoView;
    ToggleButton toogle;

    LinearLayout llForTermsAndConditions;

    ImageView imgvwIMBLogo;

    int paddingPixel = 15,drawablePaddingPixel= 20;
    int paddingDp,drawablePaddingDp;

    private static final char space = ' ';

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etMobile = (LoginCustomEditLayout) findViewById(R.id.act_login_et_mobile);
        etMPIN = (LoginCustomEditLayout) findViewById(R.id.act_login_et_MPIN);

        llForTermsAndConditions = (LinearLayout) findViewById(R.id.act_login_ll_for_t_c);
        toogle = (ToggleButton) findViewById(R.id.act_login_toggle_t_c);
        tvTermsAndConditions = (CustomTextView) findViewById(R.id.act_login_tv_t_c);

        btnLogin = (Button) findViewById(R.id.act_login_btn_login);
        tvForgotMPIN = (TextView) findViewById(R.id.act_login_tv_forgot_MPIN);




        float density = getResources().getDisplayMetrics().density;
        paddingDp = (int)(paddingPixel * density);
        drawablePaddingDp = (int)(drawablePaddingPixel * density);





        imgvwIMBLogo = (ImageView) findViewById(R.id.imgvwIMBLogo);

        //AppSettings.putData(LoginActivity.this, AppSettings.MERCHANT_ID, null);
        //IMBAgentApp.getInstance().setAgentID(null);
        AppSettings.putData(LoginActivity.this, AppSettings.ISSESSIONEXPIRED, "No");


        if(BuildConfig.DEBUG) {
            imgvwIMBLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (BuildConfig.DEBUG) {

                    if (Constants.DEVICE_ID) {
                        Constants.DEVICE_ID = false;
                        // changeIp.setVisibility(View.VISIBLE);
                    } else {
                        Constants.DEVICE_ID = true;
                        // changeIp.setVisibility(View.GONE);
                    }

                    Constants.showToast(LoginActivity.this, Constants.DEVICE_ID ? "REAL DEVICE ID" : "TEST DEVICE ID");
                     }

                }
            });
        }


        SpannableString content = new SpannableString(tvTermsAndConditions.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, tvTermsAndConditions.getText().toString().length(), 0);
        tvTermsAndConditions.setText(content);


        tvTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AboutmVisa.class);
                intent.putExtra("terms", true);
                startActivity(intent);
            }
        });

        if (AppSettings.getData(getApplicationContext(), AppSettings.ISCHECKEDTERMS).equals("true")) {
            // toogle.setVisibility(View.GONE);
            //  tvTermsAndConditions.setVisibility(View.GONE);

            llForTermsAndConditions.setVisibility(View.GONE);
            toogle.setChecked(true);
        }


        toogle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked) {
                    AppSettings.putData(getApplicationContext(), AppSettings.ISCHECKEDTERMS, "true");
                } else {
                    AppSettings.putData(getApplicationContext(), AppSettings.ISCHECKEDTERMS, "false");
                }

            }
        });



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    getKey();
                }
            }
        });

        tvForgotMPIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotMPINActivity.class);
                startActivity(intent);
            }
        });



        etMobile.setGenericTextWatcher(new LoginCustomEditLayout.GenericTextWatcher() {
            @Override
            public void handleCallback() {
                if (etMobile.getText().length() == getResources().getInteger(R.integer.mobileNoLength)) {
                    etMPIN.requestFocus();
                }
            }
        });



        etMPIN.setGenericTextWatcher(new LoginCustomEditLayout.GenericTextWatcher() {
            @Override
            public void handleCallback() {
                if (etMPIN.getText().length() == getResources().getInteger(R.integer.MPINLength)) {
                    if (validate()) {
                        getKey();
                    }
                }
            }
        });


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    2);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        Constants.DEVICE_ID = true;
        if (Constants.DEVICE_ID) {
            //changeIp.setVisibility(View.GONE);
        }
    }

    private boolean validate() {

        if (!Constants.isNetworkAvailable(this)) {
            new OkDialog(this, getString(R.string.errorNetwork), null, null);
            return false;
        }

        strMobileNumber = etMobile.getText().toString();
        strMPIN = etMPIN.getText().toString();

        if (strMobileNumber.trim().length() == 0) {
            new OkDialog(this, getString(R.string.errorMobileNumber), null, null);
            return false;
        }

        if (strMobileNumber.trim().length() != getResources().getInteger(R.integer.mobileNoLength)) {
            new OkDialog(this, getString(R.string.errorMobileNumberTenDigit), null, null);
            return false;
        }

        if (strMPIN.trim().length() == 0) {
            new OkDialog(this, getString(R.string.blankerrorMpin), null, null);
            return false;
        }

        if (strMPIN.trim().length() != getResources().getInteger(R.integer.MPINLength)) {
            new OkDialog(this, getString(R.string.errorMpin), null, null);
            return false;
        }


        if (!toogle.isChecked()) {
            new OkDialog(this, getString(R.string.errorTerms), null, null);
            return false;
        }

        return true;
    }

    private void getKey() {

        showProgress();

        IMBAgentApp.getInstance().setMobileNumber(strMobileNumber);

        RetrofitTask.getInstance().executeGetKey(strMobileNumber, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (isSuccess) {
                    LogUtils.Verbose("Key-Salt Response", " Key is " + response);

                    try {
                        //  String decrypted = AESecurity.getInstance().decryptString(response);
                        // LogUtils.Verbose("TAG", " Decrypted " + decrypted);

                        ArrayList<String> list = new ArrayList<>();
                        list.add("ResponseType");
                        list.add("key");
                        list.add("salt");
                        list.add("SecurityIv");


                        Map<String, String> responseMap = GenericResponseHandler.parseElements(response, list);


                        if (responseMap.get("ResponseType").equals("Success")) {
                            //Constants.showToast(LoginActivity.this, responseMap.get("Message"));
                            IMBAgentApp.getInstance().setKey(responseMap.get("key"));
                            IMBAgentApp.getInstance().setSalt(responseMap.get("salt"));
                            IMBAgentApp.getInstance().setIV(responseMap.get("SecurityIv"));


                            callAPI();
                            //finish();
                        } else {
                            dismissProgress();
                            new OkDialog(LoginActivity.this, ErrorUtils.getErrorMessage(responseMap.get("ErrorCode"), responseMap.get("Reason")), null, null);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        dismissProgress();
                        new OkDialog(LoginActivity.this, getString(R.string.errorAPI), null, null);
                    }


                } else {
                    dismissProgress();
                    new OkDialog(LoginActivity.this, response, "", null);
                }
            }
        });

    }


    private void getNewKey() {

        showProgress();

        // AppSettings.putData(this, AppSettings.MOBILE_NUMBER, mobilenumber);

        IMBAgentApp.getInstance().setMobileNumber(strMobileNumber);

        String xmlParam = "\t\t<MobileNumber>" + strMobileNumber + "</MobileNumber>\n" +
                "\t\t<AppVersion>" + "A-"+BuildConfig.VERSION_NAME   + "</AppVersion>";

        xmlParam = Constants.getReqXML("GetKey", xmlParam);


        RetrofitTask.getInstance().executeNewGetKey(strMobileNumber,xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (isSuccess) {



                    // IMBAgentApp.getInstance().setKey(response);

                    // POST Method

                   /* Serializer serializer = new Persister();
                    KeySalt keySaltResponse = null;
                    try {
                        keySaltResponse = serializer.read(KeySalt.class, response, false);
                        IMBAgentApp.getInstance().setKey(keySaltResponse.getKey());
                        IMBAgentApp.getInstance().setSalt(keySaltResponse.getSalt());
                        callAPI();
                    } catch (Exception e) {
                        e.printStackTrace();
                        IMBAgentApp.getInstance().setKey("");
                    }*/


                    try {
                        LogUtils.Verbose("Key-Salt Response", " Key is " + response);
                        //  String decrypted = AESecurity.getInstance().decryptString(response);
                        // LogUtils.Verbose("TAG", " Decrypted " + decrypted);

                        ArrayList<String> list = new ArrayList<>();
                        list.add("ResponseType");
                        list.add("SecurityKey");
                        list.add("salt");
                        list.add("SecurityIv");
                        list.add("ErrorCode");
                        list.add("Reason");
                        list.add("AppUpdateFlag");


                        Map<String, String> responseMap = GenericResponseHandler.parseElements(response, list);

                        if (responseMap.get("ResponseType").equals("Success")) {

                            //Constants.showToast(LoginActivity.this, responseMap.get("Message"));
                            IMBAgentApp.getInstance().setKey(responseMap.get("SecurityKey"));
                            IMBAgentApp.getInstance().setSalt(responseMap.get("salt"));
                            IMBAgentApp.getInstance().setIV(responseMap.get("SecurityIv"));

                            if(responseMap.get("AppUpdateFlag").equals("O")){
                                new YesNoDialog(LoginActivity.this, getString(R.string.optinal_update), getString(R.string.update), getString(R.string.skip), null, new YesNoDialog.IYesNoDialogCallback() {
                                    @Override
                                    public void handleResponse(int responsecode) {
                                        if(responsecode != 1){
                                            callAPI();
                                        }
                                    }
                                });
                            }else{
                                callAPI();
                            }


                            //finish();
                        } else {
                            dismissProgress();
                            if(responseMap.get("ErrorCode").equals(Constants.FORCEFULL_UPDATE_ERROR_CODE)) {
                                new OkDialog(LoginActivity.this, ErrorUtils.getErrorMessage(responseMap.get("ErrorCode"), responseMap.get("Reason")), Constants.FORCEFULL_UPDATE_ERROR_CODE, new OkDialog.IOkDialogCallback() {
                                    @Override
                                    public void handleResponse() {
                                        Constants.openPlayStore(LoginActivity.this);

                                        finish();
                                    }
                                });
                            }else
                                new OkDialog(LoginActivity.this, ErrorUtils.getErrorMessage(responseMap.get("ErrorCode"), responseMap.get("Reason")), null, null);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        dismissProgress();
                        new OkDialog(LoginActivity.this, getString(R.string.errorAPI), null, null);
                    }


                } else {
                    dismissProgress();
                    new OkDialog(LoginActivity.this, response, "", null);
                }
            }
        });

    }



    private void callAPI() {


        System.out.println("Login API called...");

        String strMPINHash = "";

        if (IMBAgentApp.getInstance().getSalt() != null && !TextUtils.isEmpty(IMBAgentApp.getInstance().getSalt())) {
            strMPINHash = getHash(strMPIN, IMBAgentApp.getInstance().getSalt());
        } else {
            strMPINHash = Constants.getHash(strMPIN.getBytes());
        }


        String xmlParam = "\t\t<MobileNumber>" + strMobileNumber + "</MobileNumber>\n" +
                "\t\t<Mpin>" + strMPINHash + "</Mpin>\n" +
                "\t\t<AppVersion>AND-1</AppVersion>\n" +
                "\t\t<DeviceId>" + IMBAgentApp.getInstance().getIMEINo() + "</DeviceId>";


        xmlParam = Constants.getReqXML("Login", xmlParam);

        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            new OkDialog(LoginActivity.this, e.toString(), null, null);
            return;
        }

        RetrofitTask.getInstance().executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                dismissProgress();

                if (!isSuccess) {
                    new OkDialog(LoginActivity.this, response, null, null);
                    return;
                }

                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", "Response " + decrypted);

                    Serializer serializer = new Persister();
                    LoginResponse loginResponse = serializer.read(LoginResponse.class, decrypted, false);
                    if (loginResponse.getResponse().getResponseType().equals("Success")) {

                        // IMBAgentApp.getInstance().setWallets(loginResponse.getResponseDetails().getWallet().getWallets());
                        AppSettings.putData(getApplicationContext(), AppSettings.FIRST_NAME, loginResponse.getResponseDetails().getFirstName());
                        AppSettings.putData(getApplicationContext(), AppSettings.LAST_NAME, loginResponse.getResponseDetails().getLastName());
                        AppSettings.putData(getApplicationContext(), AppSettings.EMAIL_ID, loginResponse.getResponseDetails().getEmailID());
                        AppSettings.putData(getApplicationContext(), AppSettings.ADDRESS, loginResponse.getResponseDetails().getAddress());
                        AppSettings.putData(getApplicationContext(), AppSettings.LAST_LOGIN, loginResponse.getResponseDetails().getLastLogin());
                        AppSettings.putData(getApplicationContext(), AppSettings.LASTTRANSACTION, loginResponse.getResponseDetails().getLastTransaction());
                        AppSettings.putData(getApplicationContext(), AppSettings.COMPANYNAME, loginResponse.getResponseDetails().getCompanyName());

                        IMBAgentApp.getInstance().setMobileNumber(strMobileNumber);
                        IMBAgentApp.getInstance().setSessionID(loginResponse.getHeader().getSessionId());
                        IMBAgentApp.getInstance().setAgentID(loginResponse.getResponseDetails().getAgentId());
                        IMBAgentApp.getInstance().setWallet(loginResponse.getResponseDetails().getWallet());

//                        for (int i = 0; i < loginResponse.getResponseDetails().getWallet().getWallets().size(); i++) {
//                            Wallet.WalletId walletId = loginResponse.getResponseDetails().getWallet().getWallets().get(i);
//
//                            if (walletId.getAccountType().equalsIgnoreCase("Prepaid A/C")) {
//                                //AppSettings.putData(getApplicationContext(), AppSettings.PREAPID_WALLED_ID, walletId.getValue());
//                                IMBAgentApp.getInstance().setPrepaidWalledID(walletId.getValue());
//                            } else if (walletId.getAccountType().equalsIgnoreCase("General A/C")) {
//                                // AppSettings.putData(getApplicationContext(), AppSettings.GENERAL_WALLED_ID, walletId.getValue());
//                            }
//                        }



                        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                        intent.putExtra("changepin", loginResponse.getResponseDetails().getChangeMpin());
                        startActivity(intent);
                        finish();

                    } else {
//                        if (loginResponse.getResponseDetails().getReason().contains("2")) {
//                            new OkDialog(LoginActivity.this, getString(R.string.left_with_2_attempts), null, null);
//                        } else if (loginResponse.getResponseDetails().getReason().contains("1")) {
//                            new OkDialog(LoginActivity.this, getString(R.string.left_with_1_attempts), null, null);
//                        } else {
                        new OkDialog(LoginActivity.this, ErrorUtils.getErrorMessage(loginResponse.getResponseDetails().getErrorCode(), loginResponse.getResponseDetails().getReason()), null, null);
                        //}
                    }

                } catch (Exception e) {
                    LogUtils.Exception(e);
                    new OkDialog(LoginActivity.this, getString(R.string.please_try_again), "", null);
                }
            }
        });
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_ip);
        final EditText editText = (EditText) dialog.findViewById(R.id.edt1);
        editText.setText(IMBAgentApp.getInstance().getBaseURL());
        Button btn1 = (Button) dialog.findViewById(R.id.btn1);


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editText.getText().toString().equals("")) {
                    new OkDialog(LoginActivity.this, " Please enter IP/ Host ctvName", null, null);
                    return;
                }

                InputMethodManager imm = (InputMethodManager) LoginActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                View f = getCurrentFocus();
                if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
                    imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
                else
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                LogUtils.Verbose("TAG", " IP " + editText.getText().toString());
                // IMBAgentApp.getInstance().setBaseURL(editText.getText().toString());
                dialog.dismiss();
                RetrofitTask.getInstance().makeNull();

            }
        });

        dialog.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        LogUtils.Verbose("TAG", " Call here");
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            finish();
        }
    }
}
