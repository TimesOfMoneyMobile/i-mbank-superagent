package timesofmoney.iandmsuperagent.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.adapters.NotificationAdapter;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.dialogs.YesNoDialog;
import timesofmoney.iandmsuperagent.models.IMBankAgentNotification;
import timesofmoney.iandmsuperagent.utilities.AppSettings;


public class NotificationActivity extends BaseActivity {

    ListView list;
    Button btnClearAll;
    CustomTextView noNotificationView;
    NotificationAdapter notificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        setToolbar(getString(R.string.notification));

        list = (ListView) findViewById(R.id.list);
        btnClearAll = (Button) findViewById(R.id.btnClearAll);
        noNotificationView = (CustomTextView) findViewById(R.id.nonotificationview);
        noNotificationView.setVisibility(View.GONE);


        AppSettings.putData(this, AppSettings.NOTIFICATION_COUNT, "0");


        Gson gson = new Gson();
        String json = AppSettings.getData(this, AppSettings.NOTIFICATIONS);

        Type type = new TypeToken<ArrayList<IMBankAgentNotification>>() {
        }.getType();
        final List<IMBankAgentNotification> agentNotificationList = gson.fromJson(json, type);


        if (agentNotificationList != null) {
            notificationAdapter = new NotificationAdapter(this, agentNotificationList);
            list.setAdapter(notificationAdapter);


        } else {
            noNotificationView.setVisibility(View.VISIBLE);
            btnClearAll.setVisibility(View.GONE);
        }

        btnClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new YesNoDialog(NotificationActivity.this, getString(R.string.alertdeleteNotification), null, new YesNoDialog.IYesNoDialogCallback() {
                    @Override
                    public void handleResponse(int responsecode) {

                        if (responsecode == 1) {
                            AppSettings.putData(NotificationActivity.this, AppSettings.NOTIFICATIONS, "");
                            agentNotificationList.clear();
                            notificationAdapter.notifyDataSetChanged();
                            btnClearAll.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
    }
}
