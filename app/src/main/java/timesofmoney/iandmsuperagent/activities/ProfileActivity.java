package timesofmoney.iandmsuperagent.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.utilities.AppSettings;

public class ProfileActivity extends BaseActivity {

    TextView merchantName, merchantAddress, merchantContactNumber, merchantEmailID, txtvwCompanyName, txtvwmVisaID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setToolbar(getString(R.string.menu_profile));
        merchantName = (TextView) findViewById(R.id.txtvwMerchantName);
        merchantAddress = (TextView) findViewById(R.id.txtvwMerchantAddress);
        merchantContactNumber = (TextView) findViewById(R.id.txtvwMerchantContactNumber);
        merchantEmailID = (TextView) findViewById(R.id.txtvwMerchantEmail);
        txtvwCompanyName = (TextView) findViewById(R.id.txtvwCompanyName);
        txtvwmVisaID = (TextView) findViewById(R.id.txtvwmVisaID);

//        objMerchantProfile = IMBAgentApp.getMerchantProfile();
//
//        if (objMerchantProfile != null) {


        if(!TextUtils.isEmpty(AppSettings.getData(getApplicationContext(),AppSettings.COMPANYNAME))){
            txtvwCompanyName.setText(AppSettings.getData(getApplicationContext(),AppSettings.COMPANYNAME));
        }else{
            txtvwCompanyName.setVisibility(View.GONE);
        }



        if(!TextUtils.isEmpty(IMBAgentApp.getInstance().getAgentID())){
            txtvwmVisaID.setText(getString(R.string.mvisa_id)+" "+IMBAgentApp.getInstance().getAgentID());
        }else{
            txtvwmVisaID.setVisibility(View.GONE);
        }



        merchantName.setText(AppSettings.getData(getApplicationContext(),AppSettings.FIRST_NAME) + " " + AppSettings.getData(getApplicationContext(),AppSettings.LAST_NAME));

        if(!TextUtils.isEmpty(AppSettings.getData(getApplicationContext(),AppSettings.ADDRESS))){
            merchantAddress.setText(AppSettings.getData(getApplicationContext(),AppSettings.ADDRESS));
        }else{
            merchantAddress.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(IMBAgentApp.getInstance().getMobileNumber())) {
            merchantContactNumber.setText(getString(R.string.country_code) + " " + IMBAgentApp.getInstance().getMobileNumber());
        }else{
            merchantContactNumber.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(AppSettings.getData(getApplicationContext(),AppSettings.EMAIL_ID))) {
            merchantEmailID.setText(AppSettings.getData(getApplicationContext(), AppSettings.EMAIL_ID));
        }else{
            merchantEmailID.setVisibility(View.GONE);
        }

    }
}
