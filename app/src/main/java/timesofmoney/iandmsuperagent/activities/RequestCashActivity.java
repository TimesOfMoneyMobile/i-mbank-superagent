package timesofmoney.iandmsuperagent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.utilities.Constants;

public class RequestCashActivity extends BaseActivity {

    EditText etMvisaId,etAmount;
    Button bProceed;
    String strMvisaId,strAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_cash);

        setToolbar(getString(R.string.menu_request_cash));

        setFooter(false,"","");
        etMvisaId = (EditText)findViewById(R.id.act_rc_et_mvisa_id);
        etAmount   = (EditText)findViewById(R.id.act_rc_et_amount);
        bProceed = (Button) findViewById(R.id.act_rc_b_proceed);


        bProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Constants.startNewActivity(CashInActivity.this,CashInConfirmActivity.class);


                if(validate()) {
                    Intent intent = new Intent(getApplicationContext(), CashInConfirmActivity.class);
                    intent.putExtra(Constants.SCREEN_TYPE, Constants.REQUEST_CASH);
                    intent.putExtra(Constants.MVISA_ID, strMvisaId);
                    intent.putExtra(Constants.AMOUNT_VALUE, strAmount);
                    startActivity(intent);
                }
            }
        });

    }




    private boolean validate() {
        strMvisaId = etMvisaId.getText().toString().trim();
        strAmount = etAmount.getText().toString().trim();



        String message = null;

        if (TextUtils.isEmpty(strMvisaId))
            message = getString(R.string.errorMvisaEmpty);

//        else if (strMvisaId.length()!=getResources().getInteger(R.integer.cardNumberLength))
//            message = getString(R.string.error_valid_card_number);

        else if (TextUtils.isEmpty(strAmount))
            message = getString(R.string.errorEmptyAmount);

        if (message != null) {
            new OkDialog(this, message, null, null);
            return false;
        }

        return true;
    }
}
