package timesofmoney.iandmsuperagent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.Constants;

public class SettingsActivity extends BaseActivity {

    TextView txtvwChangeMPIN, txtvwChangeLanguage ,currentLanguage;
    String selectedLanguage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setToolbar(getString(R.string.settings));

        txtvwChangeMPIN = (TextView) findViewById(R.id.txtvwChangeMPIN);
        txtvwChangeLanguage = (TextView) findViewById(R.id.txtvwChangeLanguage);
        currentLanguage = (TextView) findViewById(R.id.txtvwCurrentLanguage);


        selectedLanguage=AppSettings.getData(this, AppSettings.APPLANGUAGE);

        if(selectedLanguage.equals(Constants.RW_LANGUAGE_CODE))
            currentLanguage.setText(getString(R.string.text_english));
        else
            currentLanguage.setText(getString(R.string.kinyarwanda));
        txtvwChangeMPIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, ChangeMPINActivity.class);
                startActivity(intent);
            }
        });


        currentLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedLanguage.equals(Constants.RW_LANGUAGE_CODE))
                    selectedLanguage = Constants.EN_LANGUAGE_CODE;
                else
                    selectedLanguage = Constants.RW_LANGUAGE_CODE;

                AppSettings.putData(getApplicationContext(), AppSettings.APPLANGUAGE, selectedLanguage);
                Constants.changeLanguage(selectedLanguage, SettingsActivity.this);
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });
    }
}
