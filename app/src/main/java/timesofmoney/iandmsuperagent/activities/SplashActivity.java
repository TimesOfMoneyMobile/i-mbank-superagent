package timesofmoney.iandmsuperagent.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.utilities.AppSettings;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash2);

        //AppSettings.putData(SplashActivity.this, AppSettings.MERCHANT_ID, null);
        IMBAgentApp.getInstance().setAgentID(null);
        AppSettings.putData(SplashActivity.this, AppSettings.ISSESSIONEXPIRED, "No");


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(getApplicationContext(), ChooseLanguage.class);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }
}
