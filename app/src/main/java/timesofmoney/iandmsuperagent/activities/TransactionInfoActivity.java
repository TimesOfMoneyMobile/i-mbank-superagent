package timesofmoney.iandmsuperagent.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.Constants;

public class TransactionInfoActivity extends BaseActivity {

    CustomTextView ctvValueFrom,ctvValueTo,ctvValueVisaCard,ctvValueVisaID,ctvValueID,ctvValueAmount,ctvValueFees,ctvValueTotal;
    String strTransactionId,strAmount,strFee,strTotalAmount,strTxnDate,strFirstName,strLastName,strCardNumber,strSenderCardNumber;
View vDivider;
    RelativeLayout rlFees,rlTotal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_info);

        setToolbar(getString(R.string.transaction_detail));

        strTransactionId = getIntent().getStringExtra("transaction_id");
        strAmount = getIntent().getStringExtra("amount");
        strFee = getIntent().getStringExtra("fee");
        strTotalAmount = getIntent().getStringExtra("total");
        strTxnDate = getIntent().getStringExtra("txn_date");
        strFirstName = getIntent().getStringExtra("firstName");
        strLastName = getIntent().getStringExtra("lastName");
        strCardNumber = getIntent().getStringExtra("cardnumber");
        strSenderCardNumber = getIntent().getStringExtra("sendercardnumber");


        ctvValueFrom = (CustomTextView)findViewById(R.id.valueFrom);
        ctvValueTo =(CustomTextView)findViewById(R.id.valueTo);
        ctvValueVisaCard =(CustomTextView)findViewById(R.id.valueVisacard);
        ctvValueVisaID =(CustomTextView)findViewById(R.id.valuemVisaID);
        ctvValueID =(CustomTextView)findViewById(R.id.valueID);
        ctvValueAmount =(CustomTextView)findViewById(R.id.valueamount);
        ctvValueFees =(CustomTextView)findViewById(R.id.valuefees);
        ctvValueTotal =(CustomTextView)findViewById(R.id.valueTotal);
        vDivider = (View) findViewById(R.id.act_ti_v_divider);
        rlFees = (RelativeLayout) findViewById(R.id.rl_fees);
        rlTotal = (RelativeLayout) findViewById(R.id.rl_total);
        if(TextUtils.isEmpty(strFirstName) && TextUtils.isEmpty(strLastName))
            ctvValueFrom.setText("N.A");
        else if(TextUtils.isEmpty(strLastName))
            ctvValueFrom.setText(strFirstName);
        else if(TextUtils.isEmpty(strFirstName))
            ctvValueFrom.setText(strLastName);
        else
            ctvValueFrom.setText(strFirstName+" "+strLastName);

        ctvValueTo.setText(AppSettings.getData(TransactionInfoActivity.this,AppSettings.FIRST_NAME)+" "+AppSettings.getData(TransactionInfoActivity.this,AppSettings.LAST_NAME));

        if(TextUtils.isEmpty(strSenderCardNumber))
            ctvValueVisaCard.setText("N.A");

        else
            ctvValueVisaCard.setText(strCardNumber);

        ctvValueVisaID.setText(IMBAgentApp.getInstance().getAgentID());
        ctvValueID.setText(strTransactionId);
        ctvValueAmount.setText(getString(R.string.rwf)+" "+ Constants.formatAmount(strAmount));

        if(TextUtils.isEmpty(strFee))
            rlFees.setVisibility(View.GONE);
        else
            ctvValueFees.setText(getString(R.string.rwf)+" "+Constants.formatAmount(strFee));


        if(TextUtils.isEmpty(strTotalAmount))
            rlTotal.setVisibility(View.GONE);
        else
            ctvValueTotal.setText(getString(R.string.rwf)+" "+Constants.formatAmount(strTotalAmount));

    }


}
