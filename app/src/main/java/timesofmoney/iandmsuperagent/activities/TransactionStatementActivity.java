package timesofmoney.iandmsuperagent.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.adapters.StatementTransactionsListAdapter;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.models.StatementResponse;
import timesofmoney.iandmsuperagent.models.Transaction;
import timesofmoney.iandmsuperagent.models.adaptermodels.Item;
import timesofmoney.iandmsuperagent.models.adaptermodels.ListSection;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.LogUtils;

/**
 * Created by vishwanathp on 3/5/17.
 */
public class TransactionStatementActivity extends BaseActivity {

    private FloatingActionMenu fam;
    private FloatingActionButton fabSixMonth, fabThreeMonth, fabOneMonth, fabSelectDate;

    CustomTextView ctvCardBalance, ctvCommission, ctvFrom, ctvTo,ctvMessage;
    ListView lvForTransactionData;
    LinearLayout llForProgress, llForPagination,llForDate,llFromDate,llToDate;
    ProgressBar progressBar;
    FrameLayout rlOverLay;
    Button bGo;



    ArrayList<Transaction> allTransactions;
    int pageNo = 1, pageSize = 20;
    boolean isLoading;
    int totalRecords;
    private String fromDate = "", toDate = "";

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

    ArrayList<Item> listItems = new ArrayList<>();
    List<ListSection> listSections = new ArrayList<>();

    StatementTransactionsListAdapter statementAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_statement);

        setToolbar(getString(R.string.menu_transaction_statement));

        setFooter(false,"","");

        ctvCardBalance = (CustomTextView) findViewById(R.id.act_ts_ctv_card_balance);
        ctvCardBalance.setText(getString(R.string.rwf)+" "+IMBAgentApp.getInstance().getBalance());
        ctvCommission = (CustomTextView) findViewById(R.id.act_ts_ctv_commission);

        lvForTransactionData = (ListView) findViewById(R.id.act_ts_lv_for_transaction_data);

        llForDate = (LinearLayout) findViewById(R.id.act_ts_ll_for_date);
        llFromDate = (LinearLayout) findViewById(R.id.act_ts_ll_for_from_date);
        ctvFrom = (CustomTextView) findViewById(R.id.act_ts_ctv_from);

        llToDate = (LinearLayout) findViewById(R.id.act_ts_ll_for_to_date);
        ctvTo = (CustomTextView) findViewById(R.id.act_ts_ctv_to);

        bGo = (Button) findViewById(R.id.act_ts_ctv_go);

        llForProgress = (LinearLayout) findViewById(R.id.act_ts_ll_progress);
        progressBar = (ProgressBar) findViewById(R.id.act_ts_progressBar);
        ctvMessage = (CustomTextView) findViewById(R.id.act_ts_ctv_message);

        llForPagination = (LinearLayout) findViewById(R.id.act_ts_ll_for_pagination);


        fam = (FloatingActionMenu) findViewById(R.id.act_ts_fab_menu);


        fabOneMonth = (FloatingActionButton) findViewById(R.id.act_ts_fab_one_month);
        fabThreeMonth = (FloatingActionButton) findViewById(R.id.act_ts_fab_three_month);
        fabSixMonth = (FloatingActionButton) findViewById(R.id.act_ts_fab_six_month);
        fabSelectDate = (FloatingActionButton) findViewById(R.id.act_ts_fab_select_date);

//        rlOverLay = (FrameLayout) findViewById(R.id.act_ts_rl_overlay);
//        rlOverLay.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                if (fam.isOpened()) {
//                    fam.close(true);
//                    return true;
//                }
//                return false;
//            }
//        });


        //handling each floating action button clicked
        llFromDate.setOnClickListener(onButtonClick());
        llToDate.setOnClickListener(onButtonClick());
        bGo.setOnClickListener(onButtonClick());

        fabOneMonth.setOnClickListener(onButtonClick());
        fabThreeMonth.setOnClickListener(onButtonClick());
        fabSixMonth.setOnClickListener(onButtonClick());
        fabSelectDate.setOnClickListener(onButtonClick());

        createCustomAnimation();

        setListScrollListener();

        showProgressDialog();

//        fam.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
//            @Override
//            public void onMenuToggle(boolean opened) {
//                String text;
//                if (opened) {
//                    text = "Menu opened";
//                    rlOverLay.setVisibility(View.VISIBLE);
//                } else {
//                    text = "Menu closed";
//                    rlOverLay.setVisibility(View.GONE);
//                }
//                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
//            }
//        });


        //getTransactionList();

        callAPI(pageNo+"", pageSize+"");
    }

    private void showProgressDialog() {
        LogUtils.Verbose("showProgressDialog","showProgressDialog");
        lvForTransactionData.setVisibility(View.GONE);
        llForPagination.setVisibility(View.GONE);
        llForProgress.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        ctvMessage.setText(getString(R.string.msgWait));
    }

    public void DrawList(List<Transaction> allTransactions) {


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy hh:mm:ss");
        SimpleDateFormat displayFormat = new SimpleDateFormat("MMM dd, yyyy");

        if (listSections != null && listSections.size() > 0)
            listSections.clear();

        if (listItems != null && listItems.size() > 0)
            listItems.clear();


        for (Transaction objTransaction : allTransactions) {

            ListSection listsection = new ListSection();
            try {

                listsection.setHeader(displayFormat.format(simpleDateFormat.parse(objTransaction.getTxnDate())));
                if (!listSections.contains(listsection))
                    listSections.add(listsection);

            } catch (ParseException e) {
                e.printStackTrace();

            }

        }

        for (ListSection listSection : listSections) {
            LogUtils.Verbose("TAG", listSection.getHeader());
            listItems.add(listSection);

            for (Transaction cashoutRequest : allTransactions) {

                try {
                    String datetime = displayFormat.format(simpleDateFormat.parse(cashoutRequest.getTxnDate()));
                    if (datetime.equals(listSection.getHeader()))
                        listItems.add(cashoutRequest);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }

        LogUtils.Verbose("LIST", listItems.size() + "");


        //cashout_list.setAdapter(new CashoutListAdapter(getActivity(), listItems));
    }

    private View.OnClickListener onButtonClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(view == llFromDate){
                    Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
                    calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
                    DatePickerDialog datePickerDialog = new DatePickerDialog(TransactionStatementActivity.this, new CustomDatePicker(ctvFrom,"from"), calendar.get(Calendar.YEAR)
                            , calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                    datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                    datePickerDialog.show();

                }

                if(view == llToDate){
//                    if(ctvFrom.getText().toString().equals(getString(R.string.from))){
//                        Constants.showToast(TransactionStatementActivity.this, getString(R.string.please_select_from_date));
//                    }else{
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
                        DatePickerDialog datePickerDialog = new DatePickerDialog(TransactionStatementActivity.this, new CustomDatePicker(ctvTo,"to"), calendar.get(Calendar.YEAR)
                                , calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                        datePickerDialog.show();
                    //}


                }


                if(view == bGo){

                    LogUtils.Verbose("TAg","bGO");

                    if (TextUtils.isEmpty(ctvFrom.getText())) {
                        Constants.showToast(TransactionStatementActivity.this, getString(R.string.errFromdate));
                        return;
                    }

                    if (TextUtils.isEmpty(ctvTo.getText())) {
                        Constants.showToast(TransactionStatementActivity.this, getString(R.string.errTodate));
                        return;
                    }


                    try {
                        Date from = sdf.parse(ctvFrom.getText().toString());
                        Date to = sdf.parse(ctvTo.getText().toString());

                        if (to.before(from)) {
                            Constants.showToast(TransactionStatementActivity.this, getString(R.string.errdate));
                            return;
                        }

                        fromDate = ctvFrom.getText().toString();
                        toDate = ctvTo.getText().toString();

                        showProgressDialog();
                        if(allTransactions != null){
                            allTransactions.clear();
                        }
                        callAPI(pageNo+"", pageSize+"");



                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                }


                if (view == fabOneMonth) {
                    if(llForDate.getVisibility() == View.VISIBLE){
                        llForDate.setVisibility(View.GONE);
//                        ctvTo.setText(getString(R.string.to));
//                        ctvFrom.setText(getString(R.string.from));
                    }


                    Calendar calendar = Calendar.getInstance();
                    toDate = sdf.format(calendar.getTime());
                    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
                    fromDate = sdf.format(calendar.getTime());

                    showProgressDialog();
                    if(allTransactions != null){
                        allTransactions.clear();
                    }
                    callAPI(pageNo+"", pageSize+"");

                }
                if (view == fabThreeMonth) {
                    if(llForDate.getVisibility() == View.VISIBLE){
                        llForDate.setVisibility(View.GONE);
//                        ctvTo.setText(getString(R.string.to));
//                        ctvFrom.setText(getString(R.string.from));
                    }

                    Calendar calendar = Calendar.getInstance();
                    toDate = sdf.format(calendar.getTime());
                    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 3);
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
                    fromDate = sdf.format(calendar.getTime());

                    showProgressDialog();
                    if(allTransactions != null){
                        allTransactions.clear();
                    }
                    callAPI(pageNo+"", pageSize+"");
                }
                if (view == fabSixMonth) {
                    if(llForDate.getVisibility() == View.VISIBLE){
                        llForDate.setVisibility(View.GONE);
//                        ctvTo.setText(getString(R.string.to));
//                        ctvFrom.setText(getString(R.string.from));
                    }

                    Calendar calendar = Calendar.getInstance();
                    toDate = sdf.format(calendar.getTime());
                    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 6);
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
                    fromDate = sdf.format(calendar.getTime());

                    showProgressDialog();
                    if(allTransactions != null){
                        allTransactions.clear();
                    }

                    callAPI(pageNo+"", pageSize+"");
                }

                if(view == fabSelectDate){
                    if(llForDate.getVisibility() == View.GONE){
                        llForDate.setVisibility(View.VISIBLE);
                    }else{
                        llForDate.setVisibility(View.GONE);
                    }
                }
                fam.close(true);
              //  rlOverLay.setVisibility(View.GONE);
            }
        };
    }


    public void callAPI(String pageNo, String pageSize) {

        String xmlParam = "\t\t<MobileNumber>" + IMBAgentApp.getInstance().getMobileNumber() + "</MobileNumber>\n" +
                "\t\t<Tpin></Tpin>\n" +
                "\t\t<WalletId>" +""+ "</WalletId>\n" +
                "\t\t<FromDate>" + fromDate + "</FromDate>\n" +
                "\t\t<ToDate>" + toDate + "</ToDate>";
//                "\t\t<PageNo>" + pageNo + "</PageNo>\n" +
//                "\t\t<PageSize>" + pageSize + "</PageSize>";

        xmlParam = Constants.getReqXML("TXNHIST", xmlParam);
        final String plainXML = xmlParam;
        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            LogUtils.Exception(e);
            Constants.showToast(this, e.toString());
            return;
        }


        RetrofitTask retrofitTask = RetrofitTask.getInstance();

        retrofitTask.executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (!isSuccess) {
                    llForProgress.setVisibility(View.VISIBLE);
                    ctvMessage.setText(response);
                    llForPagination.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);

                    return;
                }


                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);
                    Serializer serializer = new Persister();
                    StatementResponse statementResponse = serializer.read(StatementResponse.class, decrypted,false);

                    if (statementResponse.getResponse().getResponseType().equals("Success")) {
                        isLoading = false;
                        totalRecords = statementResponse.getStatementResponseDetails().getTransactions().getTransactions().size();

                        if (allTransactions == null)
                            allTransactions = new ArrayList<Transaction>();


                        allTransactions.addAll(statementResponse.getStatementResponseDetails().getTransactions().getTransactions());


                        LogUtils.Verbose("AFTER SIZE", allTransactions.size() + "");

                        DrawList(allTransactions);
                        LogUtils.Verbose("AFTER SIZE LIST", listItems.size() + "");

               /* adapter = (new StatementTransactionsListAdapter(getActivity(), listItems, true));
                transactionStatementList.setAdapter(adapter);*/

                        if (statementAdapter == null) {
                            statementAdapter = new StatementTransactionsListAdapter(TransactionStatementActivity.this, listItems,"");
                            lvForTransactionData.setAdapter(statementAdapter);
                        } else
                            statementAdapter.notifyDataSetChanged();


                        lvForTransactionData.setVisibility(View.VISIBLE);
                        llForPagination.setVisibility(View.GONE);
                        llForProgress.setVisibility(View.GONE);

                    } else{
                        //when error is returned from API
                        llForProgress.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        ctvMessage.setText(statementResponse.getStatementResponseDetails().getReason());
                        llForPagination.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    LogUtils.Exception(e, plainXML);
                    llForPagination.setVisibility(View.GONE);
                    llForProgress.setVisibility(View.GONE);
                    new OkDialog(TransactionStatementActivity.this, e.getMessage(), null, null);
                }

            }
        });
    }



    private void setListScrollListener() {

        lvForTransactionData.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;

                if (allTransactions != null) {

                    if ((lastInScreen == totalItemCount) && !(isLoading) && allTransactions.size() < totalRecords) {
                        isLoading = true;
                        LogUtils.Verbose("TAG", " Loading data");
                        llForPagination.setVisibility(View.VISIBLE);
                        callAPI(String.valueOf(++pageNo), pageSize + "");
                    }
                }
            }
        });
    }


    private void createCustomAnimation() {
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(fam.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(fam.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(fam.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(fam.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(150);
        scaleOutY.setDuration(150);

        scaleInX.setDuration(250);
        scaleInY.setDuration(250);

        scaleInX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                fam.getMenuIconView().setImageResource(fam.isOpened()
                        ? R.mipmap.icon_calendar : R.mipmap.icon_close);
            }
        });

        set.play(scaleOutX).with(scaleOutY);
        set.play(scaleInX).with(scaleInY).after(scaleOutX);
        set.setInterpolator(new OvershootInterpolator(2));

        fam.setIconToggleAnimatorSet(set);
    }



    public class CustomDatePicker implements DatePickerDialog.OnDateSetListener {

        TextView txt;
        String source;

        CustomDatePicker(TextView txt,String sourceOfCall) {
            this.txt = txt;
            source = sourceOfCall;
        }

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            String year1 = String.valueOf(selectedYear);
            String month1 = String.valueOf(selectedMonth + 1);
            String day1 = String.valueOf(selectedDay);

            day1 = day1.length() > 1 ? day1 : "0" + day1;
            month1 = month1.length() > 1 ? month1 : "0" + month1;

//            if(source.equals("to")){
//                try {
//                    Date from = sdf.parse(ctvFrom.getText().toString());
//                    Date to = sdf.parse(day1 + "-" + month1 + "-" + year1);
//
//                    if (to.before(from)) {
//                        Constants.showToast(TransactionStatementActivity.this, getString(R.string.errdate));
//                        return;
//                    }
//
//
//                    txt.setText(day1 + "-" + month1 + "-" + year1);
//
//                    fromDate = ctvFrom.getText().toString();
//                    toDate = ctvTo.getText().toString();
//
//                    showProgressDialog();
//                    allTransactions.clear();
//                    callAPI(pageNo+"", pageSize+"");
//
//                    return;
//
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            }
            txt.setText(day1 + "-" + month1 + "-" + year1);
        }
    }



    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(fam.isOpened()){
            fam.close(true);
        }else{
            super.onBackPressed();
        }
    }


}
