package timesofmoney.iandmsuperagent.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.List;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.activities.CashOutMpinActivity;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.models.CashoutRequest;
import timesofmoney.iandmsuperagent.models.WithFloatRequest;
import timesofmoney.iandmsuperagent.utilities.Constants;

/**
 * Created by vishwanathp on 5/5/17.
 */
public class CashOutListAdapter extends BaseAdapter {

    Context context;
    List<WithFloatRequest> withFloatRequests;
    LayoutInflater inflater;


    public CashOutListAdapter(Context context, List<WithFloatRequest> withFloatRequests) {
        this.context = context;
        this.withFloatRequests = withFloatRequests;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return withFloatRequests.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.cashout_row, parent, false);
            holder.ctvName = (CustomTextView) convertView.findViewById(R.id.row_cash_out_ctv_name);
            holder.ctvCardNumber = (CustomTextView) convertView.findViewById(R.id.row_cash_out_ctv_card_number);
            holder.ctvAmountValue = (CustomTextView) convertView.findViewById(R.id.row_cash_out_ctv_amount_value);
            holder.llForData = (LinearLayout) convertView.findViewById(R.id.row_cash_out_ll_data);
            holder.rlAccept = (RelativeLayout) convertView.findViewById(R.id.row_cash_out_rl_accept);
            holder.rlDecline = (RelativeLayout) convertView.findViewById(R.id.row_cash_out_rl_decline);

            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();

        holder.ctvName.setText(withFloatRequests.get(position).getFirstName());
        holder.ctvCardNumber.setText(withFloatRequests.get(position).getAgentId());
        holder.ctvAmountValue.setText(context.getString(R.string.rwf) + Constants.formatAmount(withFloatRequests.get(position).getAmount()));

        holder.rlAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,CashOutMpinActivity.class);
                intent.putExtra(Constants.REQUEST_STATUS,Constants.AUTHORIZE);
                intent.putExtra(Constants.TXN_ID,withFloatRequests.get(position).getId());
                intent.putExtra(Constants.MVISA_ID,withFloatRequests.get(position).getAgentId());
                intent.putExtra(Constants.AMOUNT_VALUE,withFloatRequests.get(position).getAmount());
                context.startActivity(intent);
            }
        });

        holder.rlDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,CashOutMpinActivity.class);
                intent.putExtra(Constants.REQUEST_STATUS,Constants.REJECT);
                intent.putExtra(Constants.TXN_ID,withFloatRequests.get(position).getId());
                intent.putExtra(Constants.MVISA_ID,withFloatRequests.get(position).getAgentId());
                intent.putExtra(Constants.AMOUNT_VALUE,withFloatRequests.get(position).getAmount());
                context.startActivity(intent);
            }
        });



        return convertView;
    }

    class ViewHolder {
        CustomTextView ctvName,ctvCardNumber,ctvAmountValue;
        LinearLayout llForData;
        RelativeLayout rlAccept,rlDecline;

    }

}
