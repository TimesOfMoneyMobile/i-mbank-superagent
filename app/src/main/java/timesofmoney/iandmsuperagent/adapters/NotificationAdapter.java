package timesofmoney.iandmsuperagent.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.customviews.CustomTextView;
import timesofmoney.iandmsuperagent.models.IMBankAgentNotification;

/**
 * Created by pankajp on 4/28/2016.
 */
public class NotificationAdapter extends BaseAdapter {

    Context context;
    List<IMBankAgentNotification> notificationList;
    LayoutInflater inflater;

    public NotificationAdapter(Context context, List<IMBankAgentNotification> agentNotificationList) {
        this.context = context;
        this.notificationList = agentNotificationList;
        inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return notificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView==null)
        {
            holder=new ViewHolder();

            convertView=inflater.inflate(R.layout.notification_row,parent,false);

            holder.txtDate= (CustomTextView) convertView.findViewById(R.id.txtDate);
            holder.txtMessage= (CustomTextView) convertView.findViewById(R.id.txtMessage);
            holder.txtTime= (CustomTextView) convertView.findViewById(R.id.txtTime);

            convertView.setTag(holder);
        }

        holder= (ViewHolder) convertView.getTag();

        IMBankAgentNotification agentNotification= notificationList.get(position);

        holder.txtDate.setText(agentNotification.getTimestamp());
        holder.txtMessage.setText(agentNotification.getMessage());
        holder.txtTime.setText(agentNotification.getTime());



        return convertView;
    }

    class ViewHolder
    {
        CustomTextView txtDate,txtMessage,txtTime;

    }
}
