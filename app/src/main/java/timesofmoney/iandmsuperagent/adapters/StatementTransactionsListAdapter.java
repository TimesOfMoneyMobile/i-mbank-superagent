package timesofmoney.iandmsuperagent.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.activities.TransactionInfoActivity;
import timesofmoney.iandmsuperagent.models.Customer;
import timesofmoney.iandmsuperagent.models.Transaction;
import timesofmoney.iandmsuperagent.models.adaptermodels.Item;
import timesofmoney.iandmsuperagent.models.adaptermodels.ListSection;
import timesofmoney.iandmsuperagent.utilities.Constants;

/**
 * Created by pankajp on 13-04-2016.
 */
public class StatementTransactionsListAdapter extends BaseAdapter {

    private LayoutInflater inflater;

    //private List<TodaysTransaction> listRecentTransactions;
    List<Item> listItems;
    //List<Transaction> listRecentTransactions;

    private Context mContext;
    private final int SECTIONED_ITEM = 0;
    private final int CONTENT_ITEM = 1;

    private String[] dates = new String[]{"A", "B"};
    boolean isSectional;
    String balance;

    Transaction transaction;

    public StatementTransactionsListAdapter(Context mContext, List<Item> listRecentTransactions, String balance) {
        inflater = LayoutInflater.from(mContext);
        this.listItems = listRecentTransactions;
        this.mContext = mContext;
        this.balance = balance;
    }


    /*@Override
    public long getHeaderId(int position) {
        return listRecentTransactions.get(position).getName().charAt(0);
    }*/

    public int getItemType(int position) {
        if (listItems.get(position) instanceof ListSection)
            return SECTIONED_ITEM;
        return CONTENT_ITEM;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;


        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.todays_transaction_list_item, parent, false);
            holder.txtvwName = (TextView) convertView.findViewById(R.id.txtvwName);
            holder.txtvwCardNo = (TextView) convertView.findViewById(R.id.txtvwCArdNumber);
            holder.txtvwAmount = (TextView) convertView.findViewById(R.id.txtvwTransactionAmount);
            holder.layout_content = (LinearLayout) convertView.findViewById(R.id.l1);
            // holder.imgvwRefund = (ImageView) convertView.findViewById(R.id.imgvwRefund);
            holder.headerSection = (RelativeLayout) convertView.findViewById(R.id.rl_section);
            holder.txtSection = (TextView) convertView.findViewById(R.id.txtsection);
            //  holder.transtype = (ImageView) convertView.findViewById(R.id.transtype);
            holder.paymentType = (TextView) convertView.findViewById(R.id.paymentType);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.headerSection.setVisibility(View.GONE);
        holder.txtSection.setVisibility(View.GONE);
        //holder.transtype.setVisibility(View.VISIBLE);


        if (listItems.get(position) instanceof ListSection) {
            int rowType = getItemType(position);
            holder.headerSection.setVisibility(View.VISIBLE);
            holder.txtSection.setVisibility(View.VISIBLE);

            if (rowType == SECTIONED_ITEM) {
                ListSection listSection = (ListSection) listItems.get(position);
                holder.txtSection.setText(listSection.getHeader());
                holder.layout_content.setVisibility(View.GONE);

            } else {

                Transaction transaction = (Transaction) listItems.get(position);
                // holder.txtvwName.setText(transaction.getOtherDetail().getCustomer().getFirstName()+" "+transaction.getOtherDetail().getCustomer().getFirstName());
                holder.txtvwAmount.setText(Constants.formatAmount(transaction.getAmount()));

                /*if (AppSettings.getData(mContext, AppSettings.APPLANGUAGE).equalsIgnoreCase("ar")) {
                    holder.txtvwAmount.setText(Constants.formatAmount(transaction.getAmount()) + mContext.getString(R.string.egp));
                } else {
                    holder.txtvwAmount.setText(mContext.getString(R.string.egp) + Constants.formatAmount(transaction.getAmount()));
                }*/

            }
        } else {
            holder.layout_content.setVisibility(View.VISIBLE);
            holder.txtvwCardNo.setVisibility(View.VISIBLE);
            transaction = (Transaction) listItems.get(position);

            if (transaction.getType() != null && transaction.getType().equals("D")) {
                holder.txtvwAmount.setTextColor(ContextCompat.getColor(mContext,R.color.red_color));
            } else {
                holder.txtvwAmount.setTextColor(ContextCompat.getColor(mContext,R.color.green_color));

            }



            if (transaction.getTxnType() != null && transaction.getTxnType().equalsIgnoreCase("REFUND")) {
                holder.txtvwCardNo.setVisibility(View.GONE);
            }

            holder.txtvwAmount.setText(mContext.getString(R.string.rwf) +""+Constants.formatAmount(transaction.getAmount()));
            holder.txtvwName.setText("N.A.");
            String maskcard = Constants.setCardNo(mContext.getString(R.string.xxxx_card_number));
            holder.txtvwCardNo.setText(maskcard);
            if (transaction.getTxnType() != null) {
                if (transaction.getOtherDetail() != null) {
                    Customer customer = transaction.getOtherDetail().getCustomer();
                    if (customer != null) {
                        if (customer.getFirstName() != null && customer.getLastName() !=null)
                            holder.txtvwName.setText(customer.getFirstName() +" "+ customer.getLastName());
                        else if(customer.getFirstName() == null)
                            holder.txtvwName.setText(customer.getLastName());
                        else if(customer.getLastName() == null)
                            holder.txtvwName.setText(customer.getFirstName());
                        if (customer.getCardNumber() != null)
                            holder.txtvwCardNo.setText(Constants.setCardNo(customer.getCardNumber()));
                    }
                }
            }

            holder.layout_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Transaction transaction = (Transaction)listItems.get(position);

                    Intent intent = new Intent(mContext, TransactionInfoActivity.class);

                    if(transaction.getId() != null)
                    intent.putExtra("transaction_id",transaction.getId());

                    if(transaction.getAmount() != null)
                    intent.putExtra("amount",transaction.getAmount());

                    if(transaction.getFee() != null)
                    intent.putExtra("fee",transaction.getFee());

                    if(transaction.getTotal() != null)
                        intent.putExtra("total",transaction.getTotal());


                    if(transaction.getTxnDate() != null)
                    intent.putExtra("txn_date",transaction.getTxnDate());

                    if(transaction.getOtherDetail()!= null)
                    intent.putExtra("firstName",transaction.getOtherDetail().getCustomer().getFirstName());

                    if(transaction.getOtherDetail() != null)
                    intent.putExtra("lastName",transaction.getOtherDetail().getCustomer().getLastName());

                    if(transaction.getOtherDetail()!= null)
                    intent.putExtra("cardnumber",Constants.setCardNo(transaction.getOtherDetail().getCustomer().getCardNumber()));

                    if(transaction.getOtherDetail() != null)
                    intent.putExtra("sendercardnumber",Constants.setCardNo(transaction.getOtherDetail().getCustomer().getSenderCardNumber()));



                    mContext.startActivity(intent);

                }
            });





        }

        return convertView;
    }




    private class ViewHolder {
        TextView txtvwName;
        TextView txtvwCardNo;
        TextView txtvwAmount;
        LinearLayout layout_content;
        //ImageView transtype;
        TextView paymentType;
        RelativeLayout headerSection;
        TextView txtSection;

    }

}
