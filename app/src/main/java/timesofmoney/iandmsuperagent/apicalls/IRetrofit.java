package timesofmoney.iandmsuperagent.apicalls;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by kunalk on 1/20/2016.
 */
public interface IRetrofit {
    @FormUrlEncoded
    @POST("mobileRequestHandler")
    Call<ResponseBody> executeAPI(@Field("mobileRequestXML") String mobileRequestXML);


//    @GET("mobileRequestHandler")
//    Call<ResponseBody> getSecurityKey(@Query("mobileRequestXML") String mobileRequestXML);

    @FormUrlEncoded
    @POST("mobileReqHandler")
    Call<ResponseBody> getSecurityKey(@Field("mobileRequestXML") String mobileRequestXML, @Field("SaltRequired") String saltrequired);


    @FormUrlEncoded
    @POST("mobileRequestHandler")
    Call<ResponseBody> getSecurityKey(@Field("mobileRequestXML") String mobileRequestXML);



/*
    @FormUrlEncoded
    @POST("mobileReqHandler")
    Call<ResponseBody> getSecurityKey(@Field("mobileRequestXML") String mobileRequestXML, @Field("SaltRequired") String saltrequired);*/


}
