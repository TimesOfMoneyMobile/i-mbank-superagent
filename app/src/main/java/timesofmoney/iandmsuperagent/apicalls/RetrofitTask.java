package timesofmoney.iandmsuperagent.apicalls;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import timesofmoney.iandmsuperagent.BuildConfig;
import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.ErrorUtils;
import timesofmoney.iandmsuperagent.utilities.LogUtils;


/**
 * Created by kunalk on 1/29/2016.
 */

public class RetrofitTask {

    private IRetrofitTask callback;


    private static volatile RetrofitTask retrofitTask;
    private IRetrofit iRetrofit;

    public static RetrofitTask getInstance() {
        if (retrofitTask == null) {
            retrofitTask = new RetrofitTask();
        }

        return retrofitTask;
    }


    public static void makeNull() {
        retrofitTask = null;
    }

    public RetrofitTask() {


      /*  Proxy proxyTest = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.158.202.86", 8080));
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder().proxy(proxyTest);*/
        LogUtils.Verbose("Server URL _ Retro Fit",IMBAgentApp.getInstance().getBaseURL());
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        OkHttpClient client = builder.build();
        //10.158.202.61:8080


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(IMBAgentApp.getInstance().getBaseURL())
                .client(client)
                .build();

        iRetrofit = retrofit.create(IRetrofit.class);

    }

    public static OkHttpClient getUnsafeOkHttpClient() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient = okHttpClient.newBuilder()
                    .sslSocketFactory(sslSocketFactory)
                    .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public void executeGetKey(String param, final IRetrofitTask callback) {

        if (!Constants.isNetworkAvailable(IMBAgentApp.getInstance().getContext())) {
            callback.handleResponse(false, ErrorUtils.getErrorMessage("9999", null));
            return;
        }

        param = Constants.SERVICE_PROVIDER + Constants.PIPE_SEPARATOR + Constants.USER_TYPE + Constants.PIPE_SEPARATOR + param;   //"qnb|ME|" + param;
        String saltrequired = "Y";

        Call<ResponseBody> responseBodyCall = iRetrofit.getSecurityKey(param, saltrequired);


        //Call<ResponseBody> responseBodyCall = iRetrofit.getSecurityKey(param, saltrequired);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {

                if (response.code() == 200) {
                    try {
                        String apiResponse = response.body().string();
                        callback.handleResponse(true, apiResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                        callback.handleResponse(false, e.getMessage());
                        makeNull();
                    }
                } else {
                    // callback.handleResponse(false, "Server code " + response.code());

                    callback.handleResponse(false, ErrorUtils.getErrorMessage("061", null));

                }
            }

            @Override
            public void onFailure(Throwable t) {

                callback.handleResponse(false, ErrorUtils.getErrorMessage("061", null));

             /*   if (t instanceof SocketTimeoutException)
                    callback.handleResponse(false, ErrorUtils.getErrorMessage("061", null));
                else
                    callback.handleResponse(false, ErrorUtils.getErrorMessage("061", null));*/
            }
        });

    }


    public void executeNewGetKey(String mobileNumber,String param, final IRetrofitTask callback) {
        try {


            if (!Constants.isNetworkAvailable(IMBAgentApp.getInstance().getContext())) {
                callback.handleResponse(false, ErrorUtils.getErrorMessage("9999", null));
                return;
            }
            //    param = "qnb|CU|" + param;

            param = Constants.SERVICE_PROVIDER + Constants.PIPE_SEPARATOR + Constants.USER_TYPE + Constants.PIPE_SEPARATOR +mobileNumber+Constants.PIPE_SEPARATOR+ param;
            String saltrequired = "Y";


            LogUtils.Verbose("TAg",param);
            Call<ResponseBody> responseBodyCall = iRetrofit.getSecurityKey(param);

            //Call<ResponseBody> responseBodyCall = iRetrofit.getSecurityKey(param, saltrequired);

            responseBodyCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Response<ResponseBody> response) {

                    if (response.code() == 200) {
                        try {
                            String apiResponse = response.body().string();
                            callback.handleResponse(true, apiResponse);
                        } catch (Exception e) {
                            e.printStackTrace();
                            callback.handleResponse(false, e.getMessage());
                            makeNull();
                        }
                    } else {
                        //callback.handleResponse(false, "Server code " + response.code());
                        callback.handleResponse(false, ErrorUtils.getErrorMessage("061", null));

                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    LogUtils.Exception((Exception) t);
                    callback.handleResponse(false, ErrorUtils.getErrorMessage("061", null));
                }
            });

        } catch (Exception e) {
            //    e.getMessage();
            callback.handleResponse(false, ErrorUtils.getErrorMessage("061", null));
        }

    }


    public void executeTask(String param, final IRetrofitTask callback) {


        if (!Constants.isNetworkAvailable(IMBAgentApp.getInstance().getContext())) {
            callback.handleResponse(false, ErrorUtils.getErrorMessage("9999", null));
            return;
        }

        String selectedLanguage = AppSettings.getData(IMBAgentApp.getInstance().getContext(), AppSettings.APPLANGUAGE).equals("ar") ? Constants.AR_LANGUAGE_CODE : Constants.EN_LANGUAGE_CODE;

        //LogUtils.e("TAG", selectedLanguage);

        String postParam = Constants.SERVICE_PROVIDER + Constants.PIPE_SEPARATOR + Constants.USER_TYPE + Constants.PIPE_SEPARATOR + IMBAgentApp.getInstance().getMobileNumber() + Constants.PIPE_SEPARATOR + selectedLanguage
                + Constants.PIPE_SEPARATOR + param;

        LogUtils.Verbose("postParam",postParam);
        //  String postParam = Constants.SERVICE_PROVIDER + Constants.PIPE_SEPARATOR + Constants.USER_TYPE + Constants.PIPE_SEPARATOR + AppSettings.getData(IMBAgentApp.getInstance().getAppContext(), AppSettings.MOBILE_NUMBER) + Constants.PIPE_SEPARATOR + param;

        Call<ResponseBody> responseBodyCall = iRetrofit.executeAPI(postParam);

        LogUtils.Verbose("responseBodyCall ",responseBodyCall.toString());

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {

                if (response.code() == 200) {
                    try {
                        String apiResponse = response.body().string();
                        callback.handleResponse(true, apiResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                        callback.handleResponse(false, e.getMessage());
                        makeNull();
                    }
                } else {
                    // callback.handleResponse(false, "Server code " + response.code());
                    callback.handleResponse(false, ErrorUtils.getErrorMessage("061", null));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                LogUtils.Verbose("TAG", " Fail " + t.toString());

                callback.handleResponse(false, ErrorUtils.getErrorMessage("061", null));
               /* if (t instanceof SocketTimeoutException)
                    callback.handleResponse(false, ErrorUtils.getErrorMessage("61", null));
                else
                    callback.handleResponse(false, t.getMessage());*/
            }
        });

    }


    public interface IRetrofitTask {
        void handleResponse(boolean isSuccess, String response);
    }

}
