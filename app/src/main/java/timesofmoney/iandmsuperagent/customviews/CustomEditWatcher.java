package timesofmoney.iandmsuperagent.customviews;

import android.os.Handler;
import android.text.TextUtils;
import android.widget.EditText;

import java.util.ArrayList;

import timesofmoney.iandmsuperagent.R;


/**
 * Created by kunalk on 6/9/2016.
 */
public class CustomEditWatcher implements CustomEditText.CustomTextWatcher {
    EditText edtNext, edtPrevious;
    CustomEditText edtCurr;
    int MAXLENGTH = 0, index;
    ArrayList<String>pinlist;


    public CustomEditWatcher(CustomEditText edtCurr, EditText edtNext, EditText edtPrevious, int MAXLENGTH, int index) {
        this.edtNext = edtNext;
        this.edtPrevious = edtPrevious;
        this.MAXLENGTH = MAXLENGTH;
        this.edtCurr = edtCurr;
        this.index = index;
    }


    public CustomEditWatcher(CustomEditText edtCurr, EditText edtNext, EditText edtPrevious, int MAXLENGTH, int index,ArrayList<String>pinlist) {
        this.edtNext = edtNext;
        this.edtPrevious = edtPrevious;
        this.MAXLENGTH = MAXLENGTH;
        this.edtCurr = edtCurr;
        this.index = index;
        this.pinlist=pinlist;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.toString().length() == MAXLENGTH && edtNext != null) {
            edtNext.setEnabled(true);
            edtNext.requestFocus();
        }
        if (s.toString().length() <= 0 && edtPrevious != null) {

            if (!edtPrevious.isEnabled())
                edtPrevious.setEnabled(true);
            edtPrevious.requestFocus();
          //  edtCurr.getEditText().setEnabled(false);
            edtPrevious.setSelection(edtPrevious.getText().toString().length());
        }


        if (MAXLENGTH == 1 && edtCurr != null && !TextUtils.isEmpty(edtCurr.getEditText().getText().toString())) {


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    edtCurr.setCustomTextWatcher(null);
                    pinlist.set(index, edtCurr.getText().toString());
                    edtCurr.getEditText().setText(edtCurr.getContext().getString(R.string.bullet));
                    edtCurr.setCustomTextWatcher(CustomEditWatcher.this);
                    edtCurr.getEditText().setSelection(edtCurr.getText().length());
                }
            }, 400);
        }else if(MAXLENGTH==1 && TextUtils.isEmpty(edtCurr.getEditText().getText().toString()) && pinlist!=null)
        {
            pinlist.set(index,"A");
        }
    }


}