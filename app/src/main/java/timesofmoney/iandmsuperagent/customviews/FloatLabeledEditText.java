package timesofmoney.iandmsuperagent.customviews;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.animation.AnimatorProxy;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.LogUtils;


public class FloatLabeledEditText extends RelativeLayout {

    private static final int DEFAULT_PADDING_LEFT = 2;

    private TextView mHintTextView;
    private EditText mEditText, edtCC;
    View line_view;
    boolean isShowFraction = false;
    int hideKeyboardLength = 0;

    private static final char space = ' ';

    private Context mContext;

    public FloatLabeledEditText(Context context) {
        super(context);
        mContext = context;
    }

    public FloatLabeledEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setAttributes(attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public FloatLabeledEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        setAttributes(attrs);
    }

    private void setAttributes(AttributeSet attrs) {
        mHintTextView = new TextView(mContext);
        mHintTextView.setId(R.id.Curr);
        mHintTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.dark_grey));


        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_regular.ttf");
        mHintTextView.setTypeface(typeface, 1);


        final TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.FloatLabeledEditText);

        final int padding = a.getDimensionPixelSize(R.styleable.FloatLabeledEditText_fletPadding, 0);
        final int defaultPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_PADDING_LEFT, getResources().getDisplayMetrics());
        final int paddingLeft = a.getDimensionPixelSize(R.styleable.FloatLabeledEditText_fletPaddingLeft, defaultPadding);
        final int paddingTop = a.getDimensionPixelSize(R.styleable.FloatLabeledEditText_fletPaddingTop, 0);
        final int paddingRight = a.getDimensionPixelSize(R.styleable.FloatLabeledEditText_fletPaddingRight, 0);
        final int paddingBottom = a.getDimensionPixelSize(R.styleable.FloatLabeledEditText_fletPaddingBottom, 0);
        Drawable background = a.getDrawable(R.styleable.FloatLabeledEditText_fletBackground);
        isShowFraction = a.getBoolean(R.styleable.FloatLabeledEditText_AllowFraction, false);
        hideKeyboardLength = a.getInt(R.styleable.FloatLabeledEditText_hideKeyboardLength, 0);
        if (padding != 0) {
            mHintTextView.setPadding(padding, padding, padding, padding);
        } else {
            mHintTextView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        }

        if (background != null) {
            setHintBackground(background);
        }

        mHintTextView.setTextAppearance(mContext, a.getResourceId(R.styleable.FloatLabeledEditText_fletTextAppearance, android.R.style.TextAppearance_Small));

        //Start hidden
        mHintTextView.setVisibility(INVISIBLE);
        AnimatorProxy.wrap(mHintTextView).setAlpha(0);

        //  setOrientation(VERTICAL);
        mHintTextView.setGravity(Gravity.LEFT);
        addView(mHintTextView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        a.recycle();
    }

    @SuppressLint("NewApi")
    private void setHintBackground(Drawable background) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mHintTextView.setBackground(background);
        } else {
            mHintTextView.setBackgroundDrawable(background);
        }
    }

    @Override
    public final void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (child instanceof EditText) {


            LayoutParams layoutParams = (LayoutParams) params;
            int BOTTOM_MARGIN = (int) (getContext().getResources().getDisplayMetrics().density * 8);
            layoutParams.setMargins(0, 0, 0, BOTTOM_MARGIN);
            layoutParams.addRule(RelativeLayout.BELOW, mHintTextView.getId());
            if (child.getTag() != null && child.getTag().equals("CURR")) {
                edtCC = (EditText) child;
                edtCC.setTextColor(ContextCompat.getColor(getContext(), R.color.dark_blue));
            } else
                setEditText((EditText) child);
        } else if (!(child instanceof TextView)) {
            line_view = child;
            LayoutParams layoutParams = (LayoutParams) params;
            layoutParams.addRule(RelativeLayout.BELOW, mEditText.getId());
        }

        super.addView(child, index, params);
    }

    private void setEditText(final EditText editText) {
        mEditText = editText;
        mEditText.setTextColor(ContextCompat.getColor(getContext(), R.color.dark_blue));

//        if (isShowFraction) {
//            mEditText.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
//            mEditText.setKeyListener(DigitsKeyListener.getInstance(false, true));
//        }

//        if(AppSettings.getData(getContext(),AppSettings.APPLANGUAGE)!=null && AppSettings.getData(getContext(),AppSettings.APPLANGUAGE).equals("ar"))
//        {
//            mEditText.setGravity(Gravity.RIGHT);
//            mHintTextView.setGravity(Gravity.RIGHT);
//
//        }
        mEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (line_view != null) {
                    if (TextUtils.isEmpty(s) && !mEditText.hasFocus())
                        line_view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark_grey));
                }
                setShowHint(!TextUtils.isEmpty(s));

//                if (s.length() > 0 && (s.length() % 5) == 0) {
//                    final char c = s.charAt(s.length() - 1);
//                    if (space == c) {
//                        s.delete(s.length() - 1, s.length());
//                    }
//                }
//                // Insert char where needed.
//                if (s.length() > 0 && (s.length() % 5) == 0) {
//                    char c = s.charAt(s.length() - 1);
//                    // Only if its a digit where there should be a space we insert a space
//                    if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
//                        s.insert(s.length() - 1, String.valueOf(space));
//                    }
//                }



            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (line_view != null) {
                    if (mEditText.getText().length() != 0)
                        line_view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark_blue));//(Color.parseColor("#701246"));
                    else
                        line_view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark_grey));//(Color.parseColor("#701246"));
                }

                if (isShowFraction && s.toString().equals(".")) {
                    mEditText.removeTextChangedListener(this);
                    mEditText.setText("");
                    mEditText.addTextChangedListener(this);
                }

                if (isShowFraction) {
                    if (s.toString().contains(".")) {
                        String sub = s.toString().substring(s.toString().indexOf("."), s.toString().length());
                        if (sub.length() > 3) {
                            mEditText.removeTextChangedListener(this);
                            String text = s.toString().substring(0, s.toString().length() - 1);
                            mEditText.setText(text);
                            LogUtils.Verbose("TAG", text);
                            mEditText.setSelection(text.length());
                            mEditText.addTextChangedListener(this);
                        }
                    }
                }
                if (!TextUtils.isEmpty(s.toString())) {
                    if (edtCC != null)
                        edtCC.setVisibility(View.VISIBLE);
                } else if (edtCC != null)
                    edtCC.setVisibility(GONE);


                if (hideKeyboardLength > 0 && hideKeyboardLength == s.toString().length()) {
                    Constants.hideKeyboard(mContext);
                }





            }

        });

        mEditText.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return false;
            }
        });

        mEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean gotFocus) {
                if (line_view != null) {
//                    if (gotFocus)
//                        line_view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.divider_below_edit_text));
//                    else
                    if (TextUtils.isEmpty(mEditText.getText().toString().trim()))
                        line_view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark_grey));
                    else
                        line_view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dark_blue));//(Color.parseColor("#701246"));

                }
                //  onFocusChanged(gotFocus);
            }
        });

        mHintTextView.setText(mEditText.getHint());

        if (!TextUtils.isEmpty(mEditText.getText())) {
            mHintTextView.setVisibility(VISIBLE);
        }
    }

    private void onFocusChanged(boolean gotFocus) {
        if (gotFocus && mHintTextView.getVisibility() == VISIBLE) {
            ObjectAnimator.ofFloat(mHintTextView, "alpha", 0.33f, 1f).start();
        } else if (mHintTextView.getVisibility() == VISIBLE) {
            AnimatorProxy.wrap(mHintTextView).setAlpha(1f);  //Need this for compat reasons
            ObjectAnimator.ofFloat(mHintTextView, "alpha", 1f, 0.33f).start();
        }
    }

    private void setShowHint(final boolean show) {
        AnimatorSet animation = null;
        if ((mHintTextView.getVisibility() == VISIBLE) && !show) {
            animation = new AnimatorSet();
            ObjectAnimator move = ObjectAnimator.ofFloat(mHintTextView, "translationY", 0, mHintTextView.getHeight() / 8);
            ObjectAnimator fade = ObjectAnimator.ofFloat(mHintTextView, "alpha", 1, 0);
            animation.playTogether(move, fade);
        } else if ((mHintTextView.getVisibility() != VISIBLE) && show) {
            animation = new AnimatorSet();
            ObjectAnimator move = ObjectAnimator.ofFloat(mHintTextView, "translationY", mHintTextView.getHeight() / 8, 0);
            ObjectAnimator fade;
            if (mEditText.isFocused()) {
                fade = ObjectAnimator.ofFloat(mHintTextView, "alpha", 0, 1);
            } else {
                fade = ObjectAnimator.ofFloat(mHintTextView, "alpha", 0, 1);
            }
            animation.playTogether(move, fade);
        }

        if (animation != null) {
            animation.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    mHintTextView.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mHintTextView.setVisibility(show ? VISIBLE : INVISIBLE);
                    AnimatorProxy.wrap(mHintTextView).setAlpha(show ? 1 : 0);
                }
            });
            animation.start();
        }
    }

    public EditText getEditText() {
        return mEditText;
    }

    public void setHint(String hint) {
        mEditText.setHint(hint);
        mHintTextView.setText(hint);
    }

    public CharSequence getHint() {
        return mHintTextView.getHint();
    }

    public class DecimalDigitsInputFilter implements InputFilter {

        Pattern mPattern;

        public DecimalDigitsInputFilter() {
            mPattern = Pattern.compile("[0-9]*+((\\.[0-9]?)?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            Matcher matcher = mPattern.matcher(dest);
            if (!matcher.matches())
                return "";
            return null;
        }
    }


    public void hideEdtCC() {
        if (edtCC != null)
            edtCC.setVisibility(GONE);
    }

}