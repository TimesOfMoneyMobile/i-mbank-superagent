package timesofmoney.iandmsuperagent.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.utilities.LogUtils;

/**
 * Created by kunalk on 3/22/2016.
 */
public class LoginCustomEditLayout extends FrameLayout {


    Drawable icon_Normal, icon_Active;
    private EditText edt1,edtCC;
    ImageView icon;
    private TextView txtHint;
    View viewImg, viewEdt;
    private int Max_Length;
    RelativeLayout rl_top;
    float DENSITY = 1.0f;
    private GenericTextWatcher genericTextWatcher;
    Typeface typeface_regular;
    Typeface typeface_italic;


    public LoginCustomEditLayout(Context context) {
        super(context);
        initView(context);
    }

    public void setGenericTextWatcher(GenericTextWatcher textWatcher) {
        this.genericTextWatcher = textWatcher;
    }


    public LoginCustomEditLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LogUtils.Verbose("TAG", " constructor");
        TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.loginCustomEdit);
        initView(context);
        icon_Normal = t.getDrawable(R.styleable.loginCustomEdit_iconNormal);
        icon_Active = t.getDrawable(R.styleable.loginCustomEdit_iconActive);
        setLayout(t.getString(0), t.getInteger(3, 10), t.getString(4));
    }

    private void initView(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.login_custom_edit, null);
        edt1 = (EditText) view.findViewById(R.id.edt1);
        edtCC = (EditText) view.findViewById(R.id.edtCC);

        rl_top = (RelativeLayout) view.findViewById(R.id.rl_top);
        icon = (ImageView) view.findViewById(R.id.img1);
        txtHint = (TextView) view.findViewById(R.id.txt1);
        viewEdt = view.findViewById(R.id.view2);
        viewImg =  view.findViewById(R.id.view1);

        viewImg.setVisibility(GONE);
        viewEdt.setVisibility(GONE);

        addView(view);
    }

    public void setEditTextFocus() {
        edt1.requestFocus();
    }

    public void setLayout(String editHint, int maxlength, String inputtype) {

        typeface_regular = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_regular.ttf");
        typeface_italic = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_italic.ttf");

        txtHint.setTypeface(typeface_regular);

        Max_Length = maxlength;
        icon.setImageDrawable(icon_Normal);
        edt1.setHint("");


        DENSITY = getResources().getDisplayMetrics().density;
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(maxlength);
        edt1.setFilters(FilterArray);

        int inputMethod = Integer.parseInt(inputtype);

        LogUtils.Verbose("TAG", " input type " + inputtype);
        switch (inputMethod) {
            case 1:

                edt1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                break;
            case 3:
                edt1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                edt1.setTransformationMethod(PasswordTransformationMethod.getInstance());
                break;
        }
        txtHint.setText(editHint);
        txtHint.setClickable(false);

        edt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



                if (TextUtils.isEmpty(s) && !edt1.hasFocus())
                {
                    viewEdt.setBackgroundColor(Color.parseColor("#ffffff"));
                    viewImg.setBackgroundColor(Color.parseColor("#ffffff"));

                    txtHint.setVisibility(View.VISIBLE);

                }

                if (!TextUtils.isEmpty(s)) {

                    if (s.toString().length() == Max_Length) {
                        if (genericTextWatcher != null)
                            genericTextWatcher.handleCallback();
                    }

                    if(Max_Length==getResources().getInteger(R.integer.mobileNoLength))
                        edtCC.setVisibility(View.VISIBLE);



                    txtHint.setVisibility(View.GONE);

                }else {
                    edtCC.setVisibility(View.GONE);
                    txtHint.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edt1.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    viewImg.setBackgroundColor(Color.parseColor("#394040"));
                    viewEdt.setBackgroundColor(Color.parseColor("#394040"));

                    icon.setImageDrawable(icon_Active);
                   // animateHintToY(25);

                }
                else if (TextUtils.isEmpty(edt1.getText().toString().trim())) {
                    viewImg.setBackgroundColor(Color.parseColor("#ffffff"));
                    viewEdt.setBackgroundColor(Color.parseColor("#ffffff"));
                    icon.setImageDrawable(icon_Normal);
                   // animateHintToY(0);

                }else {
                    viewImg.setBackgroundColor(Color.parseColor("#701246"));
                    viewEdt.setBackgroundColor(Color.parseColor("#701246"));

                }
            }
        });
    }


    public void animateHintToY(int offset) {


       // int animate_offset = (int) (offset * DENSITY);
        //txtHint.animate().translationY(animate_offset).alpha(1).start();

        if (offset == 0) {
            txtHint.setVisibility(View.GONE);
           // txtHint.setTypeface(typeface_regular);
           // txtHint.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);


        } else {
            txtHint.setVisibility(View.VISIBLE);
            //txtHint.setTypeface(typeface_italic);
          //  txtHint.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

        }

//        animation = new AnimatorSet();
//        ObjectAnimator move = ObjectAnimator.ofFloat(txtHint, "translationY", 0, animate_offset);
//        ObjectAnimator fade = ObjectAnimator.ofFloat(txtHint, "alpha", 1, 0);
//        animation.playTogether(move, fade);
    }

    public String getText() {
        return edt1.getText().toString();
    }

    public void setText(String text) {
        edt1.setText(text);
        viewImg.setBackgroundColor(Color.parseColor("#394040"));
        viewEdt.setBackgroundColor(Color.parseColor("#394040"));

        icon.setImageDrawable(icon_Active);
       // animateHintToY(25);
    }

    public interface GenericTextWatcher {
        void handleCallback();
    }
}
