package timesofmoney.iandmsuperagent.customviews;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.Map;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.activities.AboutmVisa;
import timesofmoney.iandmsuperagent.activities.CashInActivity;
import timesofmoney.iandmsuperagent.activities.CashOutActivity;
import timesofmoney.iandmsuperagent.activities.LoginActivity;
import timesofmoney.iandmsuperagent.activities.ProfileActivity;
import timesofmoney.iandmsuperagent.activities.RequestCashActivity;
import timesofmoney.iandmsuperagent.activities.SettingsActivity;
import timesofmoney.iandmsuperagent.activities.TransactionStatementActivity;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.dialogs.OkDialog;
import timesofmoney.iandmsuperagent.dialogs.ProgressDialog;
import timesofmoney.iandmsuperagent.dialogs.YesNoDialog;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.GenericResponseHandler;
import timesofmoney.iandmsuperagent.utilities.LogUtils;


/**
 * Created by kunalk on 1/27/2016.
 */
public class MenuDrawerView implements View.OnClickListener {

    Context context;
    View view;
    CustomTextView ctvAgentName, ctvMvisaId, ctvLogout;
    RelativeLayout rlCashIn, rlCashOut, rlBillPayment,rlRequestCash,rlAuthorizeFloat,rlTransactionStatement, rlProfile, rlSettings,  rlAboutmVisa;
    LinearLayout llLogout;
    String merchantLevel;


    public MenuDrawerView(Context context, View view) {
        this.context = context;
        this.view = view;

        initView();
    }

    private void initView() {

        rlCashIn = (RelativeLayout) view.findViewById(R.id.list_menu_rl_cash_in);
        rlCashOut = (RelativeLayout) view.findViewById(R.id.list_menu_rl_cash_out);
        rlBillPayment = (RelativeLayout) view.findViewById(R.id.list_menu_rl_bill_payment);
        rlRequestCash = (RelativeLayout) view.findViewById(R.id.list_menu_rl_request_cash);
        rlAuthorizeFloat = (RelativeLayout)view.findViewById(R.id.list_menu_rl_authorize_float);
        rlTransactionStatement = (RelativeLayout) view.findViewById(R.id.list_menu_rl_transaction_statement);
        rlProfile = (RelativeLayout) view.findViewById(R.id.list_menu_rl_profile);
        rlSettings = (RelativeLayout) view.findViewById(R.id.list_menu_rl_settings);
        llLogout = (LinearLayout) view.findViewById(R.id.list_menu_ll_logout);
        rlAboutmVisa = (RelativeLayout) view.findViewById(R.id.list_menu_rl_about_mvisa);
        ctvAgentName = (CustomTextView) view.findViewById(R.id.list_menu_ctv_agent_name);
        ctvMvisaId = (CustomTextView) view.findViewById(R.id.list_menu_ctv_mvisa_id);
        ctvLogout = (CustomTextView) view.findViewById(R.id.list_menu_ctv_logout);

        //if (IMBAgentApp.getMerchantProfile() != null) {

            ctvAgentName.setText(AppSettings.getData(context,AppSettings.FIRST_NAME)+" "+AppSettings.getData(context,AppSettings.LAST_NAME));



        //}

        String lastLoginDate = Constants.formattedDateFromString("yyyy-MM-dd hh:mm:ss", "MMM dd'' yy HH:mm", AppSettings.getData(context, AppSettings.LAST_LOGIN));
        String lastTransactionDate = Constants.formattedDateFromString("yyyy-MM-dd hh:mm:ss", "MMM dd'' yy HH:mm", AppSettings.getData(context, AppSettings.LASTTRANSACTION));

        // For the very first time if Last Login and Last Transaction is blank or empty.

        ctvMvisaId.setText(context.getString(R.string.mvisa_id) + " " + IMBAgentApp.getInstance().getAgentID());

//        if (!TextUtils.isEmpty(lastLoginDate) && !lastLoginDate.trim().equalsIgnoreCase("HRS")) {
//            ctvMvisaId.setText(context.getString(R.string.last_login) + " " + lastLoginDate);
//        } else {
//            ctvMvisaId.setVisibility(View.GONE);
//        }

//        if (!TextUtils.isEmpty(lastTransactionDate) && !lastTransactionDate.trim().equalsIgnoreCase("HRS")) {
//
//            ctvLogout.setText(context.getString(R.string.lasttransaction) + " " + lastTransactionDate);
//        } else {
//            ctvLogout.setVisibility(View.GONE);
//        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (hasSoftKeys((WindowManager) context.getSystemService(Context.WINDOW_SERVICE))) {

                LinearLayout.LayoutParams buttonnLayoutParams = (LinearLayout.LayoutParams) ctvLogout.getLayoutParams();
                buttonnLayoutParams.bottomMargin = context.getResources().getDimensionPixelSize(R.dimen.bottom_margin_for_logout_button_for_lollipop);
                ctvLogout.setLayoutParams(buttonnLayoutParams);
            }
        }





        rlCashIn.setOnClickListener(this);
        rlCashOut.setOnClickListener(this);
        rlBillPayment.setOnClickListener(this);
        rlRequestCash.setOnClickListener(this);
        rlAuthorizeFloat.setOnClickListener(this);
        rlTransactionStatement.setOnClickListener(this);
        rlProfile.setOnClickListener(this);
        rlSettings.setOnClickListener(this);
        llLogout.setOnClickListener(this);
        rlAboutmVisa.setOnClickListener(this);

        if (!TextUtils.isEmpty(merchantLevel) && merchantLevel.equals("1")) {
            //rlBillPayment.setVisibility(View.VISIBLE);
        } else {
//            rlProfile.setBackgroundColor(ContextCompat.getColor(context, R.color.menudrawerdark));
//            rlSettings.setBackgroundColor(ContextCompat.getColor(context, R.color.menudrawerlight));
//            llLogout.setBackgroundColor(ContextCompat.getColor(context, R.color.menudrawerdark));
//            rlAboutmVisa.setBackgroundColor(ContextCompat.getColor(context, R.color.menudrawerlight));
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean hasSoftKeys(WindowManager windowManager) {
        Display d = windowManager.getDefaultDisplay();

        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);

        int realHeight = realDisplayMetrics.heightPixels;
        int realWidth = realDisplayMetrics.widthPixels;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        d.getMetrics(displayMetrics);

        int displayHeight = displayMetrics.heightPixels;
        int displayWidth = displayMetrics.widthPixels;

        return (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
    }

    @Override
    public void onClick(View v) {

        if (v == rlCashIn) {
            Constants.startNewActivity(context, CashInActivity.class);
        } else if (v == rlCashOut) {
            Constants.startNewActivity(context, CashOutActivity.class);
        } else if (v == rlProfile) {
            Constants.startNewActivity(context, ProfileActivity.class);
        }else if (v == rlRequestCash) {
            Constants.startNewActivity(context, RequestCashActivity.class);
        }else if(v == rlAuthorizeFloat){
            Constants.startNewActivity(context, CashOutActivity.class);
        }else if (v == rlTransactionStatement) {
            Constants.startNewActivity(context, TransactionStatementActivity.class);
        } else if (v == rlBillPayment) {
            //Constants.startNewActivity(context, BillPayActivity.class);
        } else if (v == rlSettings) {
            Constants.startNewActivity(context, SettingsActivity.class);


        } else if (v == rlAboutmVisa) {
            Constants.startNewActivity(context, AboutmVisa.class);



        } else if (v == llLogout) {
            String message = context.getString(R.string.logout_msg);
            new YesNoDialog(context, message, context.getString(R.string.menu_logout), new YesNoDialog.IYesNoDialogCallback() {
                @Override
                public void handleResponse(int responsecode) {
                    if (responsecode == 1) {
                        logutUser();
                    }
                }
            });
        }
    }

    private void logutUser() {


        if (!Constants.isNetworkAvailable(context)) {
            new OkDialog(context, context.getString(R.string.errorNetwork), null, null);
            return;
        }

        String xmlParam = "\t\t<MobileNumber>" + IMBAgentApp.getInstance().getMobileNumber() + "</MobileNumber>";
        xmlParam = Constants.getReqXML("Logout", xmlParam);

        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Constants.showToast(context, e.toString());
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(((Activity) context), context.getString(R.string.msgWait));
        progressDialog.show();
        RetrofitTask retrofitTask = RetrofitTask.getInstance();

        retrofitTask.executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                progressDialog.dismiss();

                if (!isSuccess) {
                    new OkDialog(context, response, null, null);
                    return;
                }

                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);
                    Map<String, String> responseMap = GenericResponseHandler.handleResponse(decrypted);


                    if (responseMap.get("ResponseType").equals("Success")) {

                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        ((Activity) context).finish();


                    } else {
                        //Constants.showToast(context, responseMap.get("Message"));

                        //AppSettings.putData(context, AppSettings.MERCHANT_ID, null);
                        IMBAgentApp.getInstance().setAgentID(null);
                        AppSettings.putData(context, AppSettings.ISSESSIONEXPIRED, "");

                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    new OkDialog(context, context.getString(R.string.errorAPI), null, null);
                }


            }
        });
    }


}
