package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Attribute;

import timesofmoney.iandmsuperagent.models.adaptermodels.Item;

/**
 * Created by vishwanathp on 5/5/17.
 */
public class CashoutRequest implements Item {

    @Attribute
    String id,amount, cardNumber,firstName,txnDate;

    public String getId() {
        return id;
    }

    public String getAmount() {
        return amount;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getTxnDate() {
        return txnDate;
    }

    @Override
    public boolean isSectioned() {
        return false;
    }


}
