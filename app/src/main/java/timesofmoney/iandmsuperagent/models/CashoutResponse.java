package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by vishwanathp on 5/5/17.
 */
@Root(name="MpsXml")
public class CashoutResponse {

    @Element(name="Header")
    Header header;

    @Element(name="Request")
    Request request;

    @Element(name="Response")
    Response response;

    @Element(name="ResponseDetails")
    CashoutResponseDetails responseDetails;

    public Response getResponse() {
        return response;
    }

    public CashoutResponseDetails getResponseDetails() {
        return responseDetails;
    }


}
