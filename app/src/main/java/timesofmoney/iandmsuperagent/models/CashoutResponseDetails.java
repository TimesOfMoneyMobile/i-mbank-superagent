package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by vishwanathp on 5/5/17.
 */
public class CashoutResponseDetails {


    @Element(required = false)
    String ErrorCode, Reason;

    @Element(name = "CashOutRequests", required = false)
    CashOutRequests cashOutRequests;

    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }

    public CashOutRequests getCashOutRequests() {
        return cashOutRequests;
    }

    public static class CashOutRequests {

        @ElementList(entry = "CashOutRequest", inline = true)
        List<CashoutRequest> cashoutList;

        public List<CashoutRequest> getCashoutList() {
            return cashoutList;
        }

    }


}
