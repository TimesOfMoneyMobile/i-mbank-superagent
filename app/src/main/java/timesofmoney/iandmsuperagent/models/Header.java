package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Element;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Created by kunalk on 1/29/2016.
 */
public class Header {
    @Element(required = false)
    String ChannelId="App",Timestamp="TestTime",SessionId="Test",ServiceProvider="toml";

    public Header()
    {
        Calendar calendar=Calendar.getInstance();
        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        Timestamp=sdf.format(calendar.getTime());

        SessionId= "";

    }

    public String getSessionId() {
        return SessionId;
    }


}
