package timesofmoney.iandmsuperagent.models;

/**
 * Created by vishwanathp on 22/5/17.
 */
public class IMBankAgentNotification {
    private String message,timestamp,time;


    public String getMessage() {
        return message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }
}
