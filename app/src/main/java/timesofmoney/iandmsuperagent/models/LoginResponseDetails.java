package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Text;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kunalk on 1/29/2016.
 */
public class LoginResponseDetails {


    @Element(required = false)
    String SuperAgentId;

    @Element(required = false)
    String Message;

    @Element(required = false)
    String LastName;

    @Element(required = false)
    String FirstName;

    @Element(required = false)
    String EmailID;

    @Element(required = false)
    String Address;

    @Element(required = false)
    String ErrorCode;
    @Element(required = false)
    String Reason;

    @Element(required = false)
    String ChangeMpin;

    @Element(required = false)
    MerchantProfile Profile;

    @Element(required = false)
    String ChangeTpin;

    @Element(required = false)
    String LastLogin;

    @Element(required = false)
    String LastTransaction;



    @Element(required = false)
    String CompanyName;


    @Element(required = false, name = "Wallet")
    public Wallet wallet;


    public static class Wallet {

        @ElementList(entry = "WalletId", inline = true)
        List<Wallet.WalletId> Wallet;

        public List<Wallet.WalletId> getWallets() {
            return Wallet;
        }


        public static class WalletId implements Serializable {
            @Attribute
            public String AccountType, isPrimary, balance;


            @Text
            private String value;


            public void setBalance(String balance) {
                this.balance = balance;
            }

            public void setIsPrimary(String isPrimary) {
                this.isPrimary = isPrimary;
            }

            public String getAccountType() {
                return AccountType;
            }

            public String getValue() {
                return value;
            }

            public String getIsPrimary() {
                return isPrimary;
            }

            public String getBalance() {
                return balance;
            }
        }
    }





    public String getChangeTpin() {
        return ChangeTpin;
    }

    public String getReason() {
        return Reason;
    }


    public String getChangeMpin() {
        return ChangeMpin;
    }

    public MerchantProfile getProfile() {
        return Profile;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public Wallet getWallet() {
        return wallet;
    }


    public String getLastLogin() {
        return LastLogin;
    }

    public String getLastTransaction() {
        return LastTransaction;
    }


    public String getAgentId() {
        return SuperAgentId;
    }

    public String getMessage() {
        return Message;
    }

    public String getLastName() {
        return LastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getEmailID() {
        return EmailID;
    }


    public String getAddress() {
        return Address;
    }


    public String getCompanyName() {
        return CompanyName;
    }



}
