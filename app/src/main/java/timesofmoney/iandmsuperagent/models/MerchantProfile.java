package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Element;

import java.io.Serializable;

import timesofmoney.iandmsuperagent.utilities.Constants;

/**
 * Created by pankajp on 19-04-2016.
 */

public class MerchantProfile implements Serializable {


    @Element(required = false)
    String FirstName, LastName, MerchantId, MerchantName, CompanyName, BusinessDescription, MerchantType, CountryCode;
    @Element(required = false)
    String MerchantCategory;
    @Element(required = false)
    String Address;
    @Element(required = false)
    String City;
    @Element(required = false)
    String AlternateAddress;
    @Element(required = false)
    String Pincode;
    @Element(required = false)
    String EmailId;
    @Element(required = false)
    String MobileNumber;
    @Element(required = false)
    String Fax;
    @Element(required = false)
    String TaxIdentificationNumber;
    @Element(required = false)
    String TradingLicenceNumber;
    @Element(required = false)
    String MCC;
    @Element(required = false)
    String TipFlag = "";
    @Element(required = false)
    String ConvenienceFee = "";
    @Element(required = false)
    String MerchantLevel = "";


    @Element(required = false)
    String AllowRefund = "N";


    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getMerchantId() {
//return "43234690";
        return MerchantId;
    }

    public String getMerchantName() {
        return MerchantName;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public String getBusinessDescription() {
        return BusinessDescription;
    }

    public String getConvenienceFee() {
        return ConvenienceFee;
    }

    public String getMerchantType() {
        return MerchantType;
    }

    public String getMerchantCategory() {
        return MerchantCategory;
    }

    public String getAddress() {
        return Constants.decodeString(Address);
    }

    public String getCity() {
        return City;
    }

    public String getAlternateAddress() {
        return AlternateAddress;
    }

    public String getPincode() {
        return Pincode;
    }

    public String getEmailId() {
        return EmailId;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public String getFax() {
        return Fax;
    }

    public String getTaxIdentificationNumber() {
        return TaxIdentificationNumber;
    }

    public String getTradingLicenceNumber() {
        return TradingLicenceNumber;
    }

    public String getMCC() {
        return MCC;
    }

    public String getTipFlag() {
        return TipFlag;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public String getMerchantLevel() {
        return MerchantLevel;
    }

    public String getAllowRefund() {
        return AllowRefund;
    }

    public void setAllowRefund(String allowRefund) {
        AllowRefund = allowRefund;
    }
}
