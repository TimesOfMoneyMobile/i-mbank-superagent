package timesofmoney.iandmsuperagent.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;

/**
 * Created by pankajp on 5/3/2016.
 */
public class OtherDetail implements Parcelable {
    @Element(name = "Customer", required = false)
    public Customer customer;
    @Element(name = "Bank", required = false)
    public Bank bank;

    @Element(name = "OriginalTransaction", required = false)
    public OriginalTransaction originalTransaction;

    public OtherDetail(){

    }

    protected OtherDetail(Parcel in) {
        customer=in.readParcelable(Customer.class.getClassLoader());
        bank=in.readParcelable(Bank.class.getClassLoader());
    }

    public static final Creator<OtherDetail> CREATOR = new Creator<OtherDetail>() {
        @Override
        public OtherDetail createFromParcel(Parcel in) {
            return new OtherDetail(in);
        }

        @Override
        public OtherDetail[] newArray(int size) {
            return new OtherDetail[size];
        }
    };

    public Customer getCustomer() {
        return customer;
    }

    public Bank getBank() {
        return bank;
    }

    public OriginalTransaction getOriginalTransaction() {
        return originalTransaction;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(customer,1);
        // dest.writeParcelable(bank,1);
    }
}
