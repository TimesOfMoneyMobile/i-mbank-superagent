package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Attribute;

/**
 * Created by pankajp on 18/11/16.
 */

public class RefundDetails {

    @Attribute(required = false)
    String id;
    @Attribute(required = false)
    String txnType;
    @Attribute(required = false)
    String amount;

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


}
