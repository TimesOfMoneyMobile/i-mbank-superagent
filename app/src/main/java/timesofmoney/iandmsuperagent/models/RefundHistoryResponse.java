package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by pankajp on 5/3/2016.
 */
@Root(name="MpsXml")
public class RefundHistoryResponse {

    @Element(name="Header")
    Header header;

    @Element(name="Request")
    Request request;

    @Element(name="Response")
    Response response;

    @Element(name="ResponseDetails")
    RefundHistoryResponseDetails refundHistoryResponseDetails;

    public Header getHeader() {
        return header;
    }

    public Request getRequest() {
        return request;
    }

    public Response getResponse() {
        return response;
    }

    public RefundHistoryResponseDetails getRefundHistoryResponseDetails() {
        return refundHistoryResponseDetails;
    }
}
