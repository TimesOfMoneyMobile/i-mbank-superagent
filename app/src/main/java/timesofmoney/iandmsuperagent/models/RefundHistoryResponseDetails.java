package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Element;

/**
 * Created by pankajp on 20-04-2016.
 */
public class RefundHistoryResponseDetails {


    public Refunds getRefunds() {
        return refunds;
    }

    public void setSubMerchant(Refunds refunds) {
        this.refunds = refunds;
    }

    @Element(name = "Refunds", required = false)
    Refunds refunds;


    @Element(required = false)
    String AvailableAmount;

    @Element(required = false)
    String RefundedAmount;

    @Element(required = false)
    String ErrorCode;
    @Element(required = false)
    String Reason;

    public String getAvailableAmount() {
        return AvailableAmount;
    }

    public void setAvailableAmount(String availableAmount) {
        AvailableAmount = availableAmount;
    }

    public String getRefundedAmount() {
        return RefundedAmount;
    }

    public void setRefundedAmount(String refundedAmount) {
        RefundedAmount = refundedAmount;
    }


    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }
}
