package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by pankajp on 06-04-2016.
 */

public class Refunds {

    @ElementList(entry = "Refund", inline = true, required = false)
    List<RefundDetails> listRefunds;


    public List<RefundDetails> getListRefunds() {
        return listRefunds;
    }


}
