package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by pankajp on 06-04-2016.
 */

public class SubMerchant {

    @ElementList(entry = "SubUser", inline = true,required = false)
     List<SubUser> listSubUsers;


    public List<SubUser> getListSubUsers() {
        return listSubUsers;
    }



}
