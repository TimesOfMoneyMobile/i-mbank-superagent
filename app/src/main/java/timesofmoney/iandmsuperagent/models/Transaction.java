package timesofmoney.iandmsuperagent.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

import timesofmoney.iandmsuperagent.models.adaptermodels.Item;

/**
 * Created by pankajp on 5/3/2016.
 */
public class Transaction implements Item,Parcelable {

    @Attribute(required = false)
    String id;
    @Attribute(required = false)
    String txnType;
    @Attribute(required = false)
    String amount;



    @Attribute(required = false)
    String total;



    @Attribute(required = false)
    String type;
    @Attribute(required = false)
    String txnDate;
    @Attribute(required = false)
    String remark;


    @Attribute(required = false)
    String fee;

    @Element(name = "OtherDetails", required = false)
    OtherDetail otherDetail;

    public Transaction(){}

    protected Transaction(Parcel in) {
        id = in.readString();
        txnType = in.readString();
        amount = in.readString();
        type = in.readString();
        txnDate = in.readString();
        remark = in.readString();
        fee = in.readString();
        total = in.readString();

        otherDetail=in.readParcelable(OtherDetail.class.getClassLoader());
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getId() {
        return id;
    }

    public String getTxnType() {
        return txnType;
    }

    public String getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public String getRemark() {
        return remark;
    }

    public String getTotal() {
        return total;
    }

    public OtherDetail getOtherDetail() {
        return otherDetail;
    }

    @Override
    public boolean isSectioned() {
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(txnType);
        dest.writeString(amount);
        dest.writeString(type);
        dest.writeString(txnDate);
        dest.writeString(remark);
        dest.writeString(fee);
        dest.writeString(total);

        dest.writeParcelable(otherDetail,1);

    }



}
