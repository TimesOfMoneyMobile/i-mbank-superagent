package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Attribute;

import timesofmoney.iandmsuperagent.models.adaptermodels.Item;

/**
 * Created by vishwanathp on 11/10/2017.
 */

public class WithFloatRequest implements Item {



    @Attribute
    private String id,amount,agentId,firstName,lastName,txnDate;

    public String getId() {
        return id;
    }

    public String getAmount() {
        return amount;
    }

    public String getAgentId() {
        return agentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getTxnDate() {
        return txnDate;
    }



    @Override
    public boolean isSectioned() {
        return false;
    }
}
