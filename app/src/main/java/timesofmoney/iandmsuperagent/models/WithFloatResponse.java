package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by vishwanathp on 11/10/2017.
 */
@Root(name = "MpsXml")
public class WithFloatResponse {
    @Element(name = "Header")
    Header header;

    @Element(name = "Request")
    Request request;

    @Element(name = "Response")
    Response response;


    @Element(name = "ResponseDetails")
    WithFloatResponseDetails responseDetails;

    public Response getResponse() {
        return response;
    }

    public WithFloatResponseDetails getResponseDetails() {
        return responseDetails;
    }

}
