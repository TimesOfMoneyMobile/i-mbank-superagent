package timesofmoney.iandmsuperagent.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by vishwanathp on 11/10/2017.
 */

public class WithFloatResponseDetails {


    @Element(required = false)
    String ErrorCode, Reason;



    @Element(name = "WithFloatRequests", required = false)
    WithFloatRequests withFloatRequests;

    public String getErrorCode() {
        return ErrorCode;
    }

    public String getReason() {
        return Reason;
    }


    public WithFloatRequests getWithFloatRequests() {
        return withFloatRequests;
    }


    public static class WithFloatRequests {



        @ElementList(entry = "WithFloatRequest", inline = true)
        List<WithFloatRequest> withFloatRequestList;

        public List<WithFloatRequest> getWithFloatRequestList() {
            return withFloatRequestList;
        }

    }
}
