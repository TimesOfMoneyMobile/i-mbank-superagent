package timesofmoney.iandmsuperagent.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import timesofmoney.iandmsuperagent.utilities.LogUtils;

/**
 * Created by pankajp on 5/12/16.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        LogUtils.Verbose("TAG", "AlarmReceiver onReceive Called");
        Intent i = new Intent("SessionTimeout");
        i.putExtra("isSessionExpired", "Yes");
        context.sendBroadcast(i);


    }
}
