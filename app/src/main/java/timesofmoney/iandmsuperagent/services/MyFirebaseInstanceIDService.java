package timesofmoney.iandmsuperagent.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.simpleframework.xml.Serializer;

import java.util.Map;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;
import timesofmoney.iandmsuperagent.apicalls.RetrofitTask;
import timesofmoney.iandmsuperagent.utilities.AESecurity;
import timesofmoney.iandmsuperagent.utilities.AppSettings;
import timesofmoney.iandmsuperagent.utilities.Constants;
import timesofmoney.iandmsuperagent.utilities.GenericResponseHandler;
import timesofmoney.iandmsuperagent.utilities.LogUtils;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    private Serializer serializer;
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]


    @Override
    public void onCreate() {
        super.onCreate();

        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        LogUtils.Verbose(TAG,"refreshedToken"+refreshedToken);

        if(refreshedToken!=null)
        {
            Runnable r=new Runnable() {
                @Override
                public void run() {
                    sendRegistrationToServer(refreshedToken);
                }
            };

            new Thread(r).start();
        }

    }


    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.


        String xmlParam = "<DeviceToken>" + token + "</DeviceToken>\n" +
                "\t\t<DeviceOS>AND</DeviceOS>\t"+
                "\t\t<MobileNumber>" + IMBAgentApp.getInstance().getMobileNumber() + "</MobileNumber>\t";
        xmlParam = Constants.getReqXML("SaveUpdateDeviceToken", xmlParam);

        try {
            xmlParam = AESecurity.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            LogUtils.Exception(e);
            Constants.showToast(getApplicationContext(), getResources().getString(R.string.please_try_again));
            return;
        }

        RetrofitTask retrofitTask = RetrofitTask.getInstance();
        retrofitTask.executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (!isSuccess) {
                    // Constants.showToast(RegistrationIntentService.this, response);
                    return;
                }

                try {
                    String decrypted = AESecurity.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " SaveUpdateDeviceToken Decrypted " + decrypted);


                    Map<String, String> responseMap = GenericResponseHandler.handleResponse(decrypted);


                    if (responseMap.get("ResponseType").equals("Success")) {

                        //   Constants.showToast(RegistrationIntentService.this, responseMap.get("Message"));
                        //Constants.showToast(getApplicationContext(),"success"+responseMap.get("Message"));
                        AppSettings.putData(MyFirebaseInstanceIDService.this, AppSettings.isTOKENSENDTOSERVER, "true");

                    }
                } catch (Exception e) {
                    LogUtils.Exception(e);
                    //  Constants.showToast(RegistrationIntentService.this, e.toString());
                }

            }
        });

    }
}