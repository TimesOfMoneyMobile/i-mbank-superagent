package timesofmoney.iandmsuperagent.utilities;

/**
 * Created by kunalk on 4/29/2016.
 */
public class APIException extends Exception {

    public APIException(String error,String message)
    {

        super(ErrorUtils.getErrorMessage(error,message));

    }

    public APIException(String message, Throwable cause)
    {
        super(message,cause);

    }
}
