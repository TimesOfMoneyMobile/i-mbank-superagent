package timesofmoney.iandmsuperagent.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kunalk on 4/13/2016.
 */
public class AppSettings {


    public static final String ALLOW_REFUND = "ALLOW_REFUND";

    public static final String FIRST_NAME="firstname";
    public static final String LAST_NAME="lastname";
    public static final String ADDRESS="address";
    public static final String EMAIL_ID="EMAIL_ID";
    public static final String LAST_LOGIN = "lastlogin";
    public static final String LASTTRANSACTION = "lasttransaction";
    public static final String COMPANYNAME = "companyname";
    // public static String WALLETS="wallets";
    //public static MerchantProfile objMerchantProfile;
    public static final String isTOKENSENDTOSERVER = "isTOKENSENDTOSERVER";
    public static final String NOTIFICATIONS = "NOTIFICATIONS";
    public static final String ISCHECKEDTERMS = "ISCHECKEDTERMS";
    public static final String ISSESSIONEXPIRED = "ISSESSIONEXPIRED";
    public static final String APPLANGUAGE = "APPLANGUAGE";
    public static final String TIPFLAG = "TIPFLAG";
    public static final String CONFEE = "CONFEE";
   // public static final String BASE_URL = "BASE_URL";
    public static final String NOTIFICATION_COUNT = "NOTIFICATION_COUNT";

    public static void putData(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("merchant", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public static String getData(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences preferences = context.getSharedPreferences("merchant", context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }
}
