package timesofmoney.iandmsuperagent.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import timesofmoney.iandmsuperagent.IMBAgentApp;


/**
 * Created by kunalk on 4/7/2016.
 */
public class Constants {

    public static final int SUCCESS_P2P = 0;
    public static final int SUCCESS_P2M = 1;
    public static final int SUCCESS_OTP = 2;
    public static final String CASHOUT_REQUEST_CODE = "ListCashOutRequests";
    public static final String CARD_NUMBER = "CARD_NUMBER";
    public static final String AMOUNT_VALUE = "AMOUNT_VALUE";
    public static final String OTP = "OTP";

    public static final String TXN_ID = "TXN_ID";
    public static final String FEES = "FEES";
    public static final String TXN_TYPE = "TXN_TYPE";
    public static final String CASH_IN_SUCCESS = "CASH_IN_SUCCESS";
    public static final String CASH_OUT_SUCCESS = "CASH_OUT_SUCCESS";
    public static final String CASH_OUT_FAILURE = "CASH_OUT_FAILURE";
    public static final String TIME_STAMP = "TIME_STAMP";
    public static final String REQUEST_STATUS = "REQUEST_STATUS";
    public static final String REJECT = "Reject";
    public static final String AUTHORIZE = "Authorize";
    public static final String MVISA_ID = "MVISA_ID";
    public static final String REQUEST_CASH = "REQUEST_CASH";
    public static final String CASH_IN = "CASH_IN";
    public static final String SCREEN_TYPE = "SCREEN_TYPE";
    public static final String TOTAL_AMOUNT = "TOTAL_AMOUNT";
    public static final String FORCEFULL_UPDATE_ERROR_CODE = "302";


    /*public static final String QA_LINK = "https://qamovit.timesofmoney.in/mps/"*//*"https://qa.movit.timesofmoney.in/mps/"*//*;
    public static final String STAGING_LINK = "https://movit.timesofmoney.in/mps/";
    public static final String DEMO_LINK = "http://demo.timesofmoney.com/mps/";
    public static final String PRODUCTION_LINK = "https://nivmt.network.com.eg/mps/";*/


    public static boolean DEVICE_ID = true;

    public static final String SERVICE_PROVIDER ="imbank";// "qnb";//
    public static final String USER_TYPE = "SA";
    public static final String PIPE_SEPARATOR = "|";
    public static final String EN_LANGUAGE_CODE = "en";
    public static final String AR_LANGUAGE_CODE = "ar";
    public static final String RW_LANGUAGE_CODE = "rw";





    public static boolean isNetworkAvailable(Context context) {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

 /*   public static String getHash(byte[] bytes) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(bytes);

            return bytesToHexString(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    // This is old method .
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }*/


    // Ref : http://stackoverflow.com/questions/18714616/convert-hex-string-to-byte

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }


        return data;
    }

    final protected static char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static String byteArrayToHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;

        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        LogUtils.Verbose("Byte To String", new String(hexChars));

        return new String(hexChars);
    }

    // Old Method

    public static String getHash(byte[] bytes) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(bytes);
            if (IMBAgentApp.getInstance().getSalt() != null && !TextUtils.isEmpty(IMBAgentApp.getInstance().getSalt()))
                digest.update(hexStringToByteArray(IMBAgentApp.getInstance().getSalt()));

            return byteArrayToHexString(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String getHash(String data, String secret) {
        try {

            SecretKeySpec signingKey = null;
            try {
                signingKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                LogUtils.Error("getHash()", "Salt value is empty.");
            }
            Mac mac = Mac.getInstance("HmacSHA256");

            try {
                mac.init(signingKey);
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }
            byte[] rawHmac = new byte[0];
            try {
                rawHmac = mac.doFinal(data.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // Byte [] to String conversion

            return byteArrayToHexString(rawHmac);
            //  LogUtils.Verbose("Byte To String", bytesToHex(rawHmac));


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isValidEmail(String emailStr) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }


    public static void showToast(Context context, String msg) {
        if (context != null)
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static String getReqXML(String reqType, String reqDetails) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        String xml = "<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<MpsXml>\n" +
                "\t<Header>\n" +
                "\t\t<ChannelId>APP</ChannelId>\n" +
                "\t\t<Timestamp>" + sdf.format(calendar.getTime()) + "</Timestamp>\n" +
                "\t\t<SessionId>" + IMBAgentApp.getInstance().getSessionID() + "</SessionId>\n" +
                "\t\t<ServiceProvider>" + SERVICE_PROVIDER + "</ServiceProvider>\n" +
                "\t</Header>\n" +
                "\t<Request>\n" +
                "\t\t<RequestType>" + reqType + "</RequestType>\n" +
                "\t\t<UserType>" + USER_TYPE + "</UserType>\n" +
                "\t</Request>\n" +
                "\t<RequestDetails>\n" +
                reqDetails +
                "\n\t\t<ResponseURL>{ResponseURL}</ResponseURL>\n" +
                "\t\t<ResponseVar>{ResponseVar}</ResponseVar>\n" +
                "\t</RequestDetails>\n" +
                "</MpsXml>\n";

        LogUtils.Verbose("XML Req ", xml);
        return xml;
    }

    public static void changeLanguage(String language, Context context) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }

    public static String formatAmount(String amount) {


        try {
            NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
            nf.setMaximumFractionDigits(0);
            nf.setMinimumFractionDigits(0);




            Double amt = Double.parseDouble(amount);
            amount = nf.format(amt);
        } catch (Exception e) {
            LogUtils.Exception(e);
        }
        return amount;
    }

    public static String decodeString(String input) {

        if (input == null)
            input = "N.A.";

        if (input.length() < 4) {
            return input;
        }

     /*   try {
            String temp = URLDecoder.decode(input, "UTF-8");

            return temp;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/


        try {
            byte[] data = Base64.decode(input, Base64.DEFAULT);
            String temp = new String(data);
            LogUtils.Verbose("TAG", "Decode " + temp);
//            if (temp.matches("[a-zA-Z0-9,.\\-\\s]+") || isProbablyArabic(temp))
//                return temp;
//            else
//                return input;

            char[] arr = temp.toCharArray();
            for (Character c : arr) {
                if (String.valueOf(c).matches("[a-zA-Z0-9,.\\-\\s]+") || isProbablyArabic(String.valueOf(c))) {
                    continue;
                } else
                    return input;
            }
            return temp;

        } catch (Exception e) {
            return input;
        }


    }

    public static boolean isProbablyArabic(String s) {
        for (int i = 0; i < s.length(); ) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    public static void hideKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = ((Activity) context).getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    public static String setCardNo(String cardno1) {
        if (cardno1.equalsIgnoreCase("XXXX XXXX XXXX XXXX")) {
            return cardno1;
        } else {

            StringBuilder masked_Card = new StringBuilder(cardno1);
            masked_Card.insert(4, " ");
            masked_Card.insert(9, " ");
            masked_Card.insert(14, " ");

            masked_Card.setCharAt(0, 'X');
            masked_Card.setCharAt(1, 'X');
            masked_Card.setCharAt(2, 'X');
            masked_Card.setCharAt(3, 'X');
            masked_Card.setCharAt(5, 'X');
            masked_Card.setCharAt(6, 'X');
            masked_Card.setCharAt(7, 'X');
            masked_Card.setCharAt(8, 'X');
            masked_Card.setCharAt(10, 'X');
            masked_Card.setCharAt(11, 'X');
            masked_Card.setCharAt(12, 'X');
            masked_Card.setCharAt(13, 'X');
            return masked_Card.toString();
        }

    }

    public static boolean isValidMPIN(String str) {


        ArrayList<String> SameStr = new ArrayList<>();
        SameStr.add("00000");
        SameStr.add("11111");
        SameStr.add("22222");
        SameStr.add("33333");
        SameStr.add("44444");
        SameStr.add("55555");
        SameStr.add("66666");
        SameStr.add("77777");
        SameStr.add("88888");
        SameStr.add("99999");

        String SequenceAsc = "0123456789";
        String SequenceDes = "9876543210";

        if (SameStr.contains(str) || SequenceAsc.contains(str) || SequenceDes.contains(str))
            return false;

        return true;


    }


    public static String formattedDateFromString(String inputFormat, String outputFormat, String inputDate) {
        if (inputFormat.equals("")) { // if inputFormat = "", set a default input format.
            inputFormat = "yyyy-MM-dd hh:mm:ss";
        }
        if (outputFormat.equals("")) {
            outputFormat = "EEEE d 'de' MMMM 'del' yyyy"; // if inputFormat = "", set a default output format.
        }
        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.ENGLISH);
        SimpleDateFormat df_output = new SimpleDateFormat("MMM dd''yyyy HH:mm", Locale.ENGLISH);

        // You can set a different Locale, This example set a locale of Country Mexico.
        //SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, new Locale("es", "MX"));
        //SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, new Locale("es", "MX"));

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (Exception e) {
            LogUtils.Error("formattedDateFromString", "Exception in formateDateFromstring(): " + e.getMessage());
        }
        return outputDate + " HRS";

    }

    public static String getFormattedDate(String inputDate, String inputDateFomrat, String outputDateFormat) {
        SimpleDateFormat sourcedate = new SimpleDateFormat(inputDateFomrat, Locale.ENGLISH);
        SimpleDateFormat destDate = new SimpleDateFormat(outputDateFormat, Locale.ENGLISH);
        String formattedDateTime = null;
        try {
            formattedDateTime = destDate.format(sourcedate.parse(inputDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        //formattedDateTime = new StringBuilder(formattedDateTime).insert(6, "'").toString();
        return formattedDateTime == null ? "" : formattedDateTime;
    }


    public static boolean isDeviceRooted() {
        return checkRootMethod1() || checkRootMethod2() || checkRootMethod3() || checkRootMethod4("su");
    }

    private static boolean checkRootMethod1() {
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        String[] paths = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    private static boolean checkRootMethod3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            if (in.readLine() != null) return true;
            return false;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    public static boolean checkRootMethod4(String binaryName) {
        boolean found = false;
        if (!found) {
            String[] places = {"/sbin/", "/system/bin/", "/system/xbin/",
                    "/data/local/xbin/", "/data/local/bin/",
                    "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
            for (String where : places) {
                if (new File(where + binaryName).exists()) {
                    found = true;

                    break;
                }
            }
        }
        return found;
    }


    public static void startNewActivity(Context context, Class newActivity) {
        Intent intent = new Intent(context, newActivity);
        context.startActivity(intent);
    }

    public static void openPlayStore(Context context) {
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        }
        catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
