package timesofmoney.iandmsuperagent.utilities;

import android.content.Context;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.R;


/**
 * Created by pankajp on 5/7/2016.
 */
public class ErrorUtils {


    public static String getErrorMessage(String error, String message) {

        //  int errorcode = 0;
        String errorcode = "000";
        String errorMessage = "";
        Context context = IMBAgentApp.getInstance().getContext();

        try {
            // errorcode = Integer.parseInt(error);
            errorcode = error;
        } catch (Exception e) {

            return message;
        }

        switch (error) {
            case "001":
                errorMessage = context.getResources().getString(R.string.please_enter_correct_amount);
                return errorMessage;

            case "002":
                errorMessage = context.getResources().getString(R.string.please_enter_amount);
                return errorMessage;

            case "003":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_card_number);
                return errorMessage;

            case "004":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_mobile_number);
                return errorMessage;
            case "005":
                errorMessage = context.getResources().getString(R.string.please_enter_card_number);
                return errorMessage;
            case "006":
                errorMessage = context.getResources().getString(R.string.transaction_amount_is_greater_than_your_card_balance);
                return errorMessage;

            case "008":
                errorMessage = context.getResources().getString(R.string.your_number_is_blocked_please_contact_customer_service);
                return errorMessage;
            case "009":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_first_name);
                return errorMessage;
            case "010":
                errorMessage = context.getResources().getString(R.string.please_enter_first_name);
                return errorMessage;
            case "011":
                errorMessage = context.getResources().getString(R.string.you_card_is_already_de_linked);
                return errorMessage;
            case "012":
                errorMessage = context.getResources().getString(R.string.txn_id_can_not_be_blank);
                return errorMessage;
            case "013":
                errorMessage = context.getResources().getString(R.string.invalid_transaction_id);
                return errorMessage;
            case "014":
                errorMessage = context.getResources().getString(R.string.invalid_session);
                return errorMessage;
            case "015":
                errorMessage = context.getResources().getString(R.string.your_number_is_not_registered);
                return errorMessage;
            case "016":
                errorMessage = context.getResources().getString(R.string.please_enter_old_mpin);
                return errorMessage;
            case "017":
                errorMessage = context.getResources().getString(R.string.old_new_mpin_can_not_be_same);
                return errorMessage;
            case "018":
                errorMessage = context.getResources().getString(R.string.please_enter_atm_pin);
                return errorMessage;
            case "019":
                errorMessage = context.getResources().getString(R.string.please_enter_device_id);
                return errorMessage;
            case "020":
                errorMessage = context.getResources().getString(R.string.you_are_already_registered_on_device);
                return errorMessage;
            case "021":
                errorMessage = context.getResources().getString(R.string.please_enter_mobile_number);
                return errorMessage;
            case "022":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_citizen_id);
                return errorMessage;
            case "023":
                errorMessage = context.getResources().getString(R.string.please_enter_mpin);
                return errorMessage;
            case "024":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_last_name);
                return errorMessage;
            case "025":
                errorMessage = context.getResources().getString(R.string.please_enter_last_name);
                return errorMessage;
            case "026":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_city);
                return errorMessage;
            case "027":
                errorMessage = context.getResources().getString(R.string.please_enter_city);
                return errorMessage;
            case "028":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_country);
                return errorMessage;
            case "029":
                errorMessage = context.getResources().getString(R.string.please_enter_country);
                return errorMessage;
            case "030":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_email_id);
                return errorMessage;
            case "031":
                errorMessage = context.getResources().getString(R.string.please_enter_email_id);
                return errorMessage;


            case "032":
                errorMessage = context.getResources().getString(R.string.error_while_setting_opening_balance_of_customer);
                return errorMessage;

            case "033":
                errorMessage = context.getResources().getString(R.string.customer_general_account_wallet_not_found);
                return errorMessage;

            case "034":
                errorMessage = context.getResources().getString(R.string.customer_prepaid_account_wallet_not_found);
                return errorMessage;

            case "035":
                errorMessage = context.getResources().getString(R.string.customer_velocity_is_not_defined);
                return errorMessage;

            case "036":
                errorMessage = context.getResources().getString(R.string.error_while_updating_customer_general_account);
                return errorMessage;

            case "037":
                errorMessage = context.getResources().getString(R.string.sp_fee_account_wallet_not_found);
                return errorMessage;

            case "038":
                errorMessage = context.getResources().getString(R.string.error_while_updating_sp_wallet);
                return errorMessage;

            case "039":
                errorMessage = context.getResources().getString(R.string.please_enter_wallet_id);
                return errorMessage;

            case "040":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_wallet_id);
                return errorMessage;
            case "041":
                errorMessage = context.getResources().getString(R.string.wallet_id_is_inactive);
                return errorMessage;

            case "042":
                errorMessage = context.getResources().getString(R.string.cannot_fetch_balance_please_try_later);
                return errorMessage;
            case "043":
                errorMessage = context.getResources().getString(R.string.unsuccessful_transaction_please_try_again_again);
                return errorMessage;
            case "044":
                errorMessage = context.getResources().getString(R.string.please_select_valid_card);
                return errorMessage;
            case "045":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_tpin);
                return errorMessage;
            case "046":
                errorMessage = context.getResources().getString(R.string.please_enter_tpin);
                return errorMessage;
            case "047":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_mpin);
                return errorMessage;
            case "048":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_card_number);
                return errorMessage;
            case "049":
                errorMessage = context.getResources().getString(R.string.invalid_webservice_request_data);
                return errorMessage;
            case "050":
                errorMessage = context.getResources().getString(R.string.mobile_number_already_exists);
                return errorMessage;
            case "051":
                errorMessage = context.getResources().getString(R.string.error_while_inserting_data);
                return errorMessage;
            case "052":
                errorMessage = context.getResources().getString(R.string.mvisa_is_not_set_for_processing_c2c_transaction);
                return errorMessage;
            case "053":
                errorMessage = context.getResources().getString(R.string.sp_pool_account_wallet_not_found);
                return errorMessage;
            case "054":
                errorMessage = context.getResources().getString(R.string.error_occurred_on_processing_transaction);
                return errorMessage;
            case "055":
                errorMessage = context.getResources().getString(R.string.error_occurred_while_updating_customers_velocity);
                return errorMessage;
            case "056":
                errorMessage = context.getResources().getString(R.string.insufficient_balance);
                return errorMessage;
            case "057":
                errorMessage = context.getResources().getString(R.string.please_enter_new_mpin);
                return errorMessage;
            case "058":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_new_mpin);
                return errorMessage;
            case "059":
                errorMessage = context.getResources().getString(R.string.wrong_old_mpin);
                return errorMessage;
            case "060":
                errorMessage = context.getResources().getString(R.string.you_can_not_use_old_mpin);
                return errorMessage;
            case "061":
                errorMessage = context.getResources().getString(R.string.please_try_again);
                return errorMessage;
            case "062":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_customer_details);
                return errorMessage;
            case "063":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_merchant_details);
                return errorMessage;
            case "064":
                errorMessage = context.getResources().getString(R.string.user_currently_logged_in);
                return errorMessage;
            case "065":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_token);
                return errorMessage;
            case "066":
                errorMessage = context.getResources().getString(R.string.primary_card_can_not_be_de_link);
                return errorMessage;
            case "067":
                errorMessage = context.getResources().getString(R.string.you_can_not_de_link_this_card_as_at_least_one_prepaid_card_is_required);
                return errorMessage;
            case "068":
                errorMessage = context.getResources().getString(R.string.invalid_token_or_status_is_already_inactive);
                return errorMessage;
            case "069":
                errorMessage = context.getResources().getString(R.string.primary_account_set_successfully);
                return errorMessage;
            case "070":
                errorMessage = context.getResources().getString(R.string.error_while_updating_wallet);
                return errorMessage;
            case "071":
                errorMessage = context.getResources().getString(R.string.transactions_not_available);
                return errorMessage;
            case "072":
                errorMessage = context.getResources().getString(R.string.please_enter_card_number);
                return errorMessage;
            case "073":
                errorMessage = context.getResources().getString(R.string.please_enter_valid_old_mpin);
                return errorMessage;
            case "074":
                errorMessage = context.getResources().getString(R.string.you_cannot_use_last_three_mpin_again);
                return errorMessage;
            case "075":
                errorMessage = context.getResources().getString(R.string.please_enter_citizen_id);
                return errorMessage;
            case "076":
                errorMessage = context.getResources().getString(R.string.Wallets_are_not_proper);
                return errorMessage;
            case "077":
                errorMessage = context.getResources().getString(R.string.You_have_Exceeded_the_Number_of_Attempts);
                return errorMessage;

            case "079":
                errorMessage = context.getResources().getString(R.string.Users_login_data_could_not_be_saved);
                return errorMessage;
            case "080":
                errorMessage = context.getResources().getString(R.string.No_Beneficiaries_Available);
                return errorMessage;
            case "081":
                errorMessage = context.getResources().getString(R.string.Error_While_Removing_Beneficiary);
                return errorMessage;
            case "082":
                errorMessage = context.getResources().getString(R.string.Please_enter_valid_Beneficiary_ID);
                return errorMessage;
            case "083":
                errorMessage = context.getResources().getString(R.string.Beneficiary_ID_Cannot_be_Blank);
                return errorMessage;
            case "084":
                errorMessage = context.getResources().getString(R.string.Error_while_updating_Merchant_General_Wallet);
                return errorMessage;
            case "085":
                errorMessage = context.getResources().getString(R.string.Merchant_Prepaid_Wallet_not_found);
                return errorMessage;
            case "086":
                errorMessage = context.getResources().getString(R.string.Merchant_General_Wallet_not_found);
                return errorMessage;
            case "087":
                errorMessage = context.getResources().getString(R.string.Error_While_Setting_Opening_Balance_of_Merchant);
                return errorMessage;
            case "088":
                errorMessage = context.getResources().getString(R.string.From_Date_than_Equal_to_Current_Date);
                return errorMessage;
            case "089":
                errorMessage = context.getResources().getString(R.string.To_Date_Current_Date);
                return errorMessage;
            case "090":
                errorMessage = context.getResources().getString(R.string.To_Date_Should_be_greater_than_From_Date);
                return errorMessage;
            case "091":
                errorMessage = context.getResources().getString(R.string.Range_between_From_Date_and_To_Date_should_be_less_than_6_months);
                return errorMessage;
            case "092":
                errorMessage = context.getResources().getString(R.string.No_Data_Found);
                return errorMessage;
            case "094":
                errorMessage = context.getResources().getString(R.string.Error_Fetching_Transactions);
                return errorMessage;
            case "095":
                errorMessage = context.getResources().getString(R.string.Velocity_exceeds_limit);
                return errorMessage;
            case "096":
                errorMessage = context.getResources().getString(R.string.Please_enter_valid_merchant_ID);
                return errorMessage;
            case "097":
                errorMessage = context.getString(R.string.error_nosubMerchants);
                return errorMessage;

            case "099":
                errorMessage = context.getResources().getString(R.string.Viral_Wallets_doesnt_Exist);
                return errorMessage;
            case "100":
                errorMessage = context.getResources().getString(R.string.Duplicate_Beneficiary_PAN);
                return errorMessage;
            case "101":
                errorMessage = context.getResources().getString(R.string.Please_enter_valid_tip);
                return errorMessage;
            case "102":
                errorMessage = context.getResources().getString(R.string.Error_on_user_logout);
                return errorMessage;

            case "103":
                errorMessage = context.getResources().getString(R.string.Invalid_card_details);
                return errorMessage;

            case "104":
                errorMessage = context.getResources().getString(R.string.Error_processing_request);
                return errorMessage;

            case "105":
                errorMessage = context.getResources().getString(R.string.Customer_Registration_Failed);
                return errorMessage;

            case "106":
                errorMessage = context.getResources().getString(R.string.Error_while_saving_Card_Details);
                return errorMessage;

            case "107":
                errorMessage = context.getResources().getString(R.string.Card_is_already_linked_to_your_account);
                return errorMessage;

            case "108":
                errorMessage = context.getResources().getString(R.string.Error_while_saving_Card_Details);
                return errorMessage;

            case "109":
                errorMessage = context.getResources().getString(R.string.Card_is_already_linked_to_your_account);
                return errorMessage;

            case "110":
                errorMessage = context.getResources().getString(R.string.You_cannot_add_more_cards);
                return errorMessage;

            case "114":
                errorMessage = context.getResources().getString(R.string.user_not_found);
                return errorMessage;

            case "218":
                errorMessage = context.getResources().getString(R.string.you_are_already_registered_on_device);
                return errorMessage;

            case "266":
                errorMessage = context.getResources().getString(R.string.Invalid_Credentials);
                return errorMessage;

            case "289":
                errorMessage = context.getResources().getString(R.string.invalid_details);
                return errorMessage;


            case "9999":
                errorMessage = context.getResources().getString(R.string.errorNetwork);
                return errorMessage;

            case "$01":
                errorMessage = context.getString(R.string.transaction_is_invalid);
                return errorMessage;
            case "$02":
                errorMessage = "";
                return errorMessage;
            case "$03":
                errorMessage = context.getString(R.string.your_card_has_expired);
                return errorMessage;
            case "$04":
                errorMessage = context.getString(R.string.invalid_ATM_pin);
                return errorMessage;
            case "$05":
                errorMessage = context.getString(R.string.invalid_transaction_type);
                return errorMessage;
            case "$06":
                errorMessage = "";
                return errorMessage;
            case "$07":
                errorMessage = context.getString(R.string.please_try_again);
                return errorMessage;
            case "$08":
            case "$86":
            case "$WI":
                errorMessage = context.getString(R.string.unable_to_connect_server);
                return errorMessage;
            case "$09":
                errorMessage = context.getString(R.string.please_enter_valid_card_number);
                return errorMessage;
            case "$10":
                errorMessage = context.getString(R.string.transaction_cannot_process);
                return errorMessage;
            case "$11":
                errorMessage = context.getString(R.string.you_have_exceeded_no_of_attempts_for_entering_wrong_PIN);
                return errorMessage;
            case "$13":
            case "$23":
            case "$35":
            case "$75":
                errorMessage = context.getString(R.string.account_is_invalid);
                return errorMessage;
            case "$14":
                errorMessage = context.getString(R.string.you_have_exceeded_txn_limit);
                return errorMessage;
            case "$15":
                errorMessage = context.getString(R.string.please_put_velocity);
                return errorMessage;
            case "$24":
                errorMessage = context.getString(R.string.sufficient_funds_not_available);
                return errorMessage;
            case "$33":
                errorMessage = context.getString(R.string.transaction_is_declined);
                return errorMessage;
            case "$39":
                errorMessage = context.getString(R.string.unable_to_process_at_this_moment);
                return errorMessage;
            case "$42":
                errorMessage = context.getString(R.string.unable_to_process_your_transaction);
                return errorMessage;
            case "$50":
                errorMessage = context.getString(R.string.transaction_timeout_please_try_agian);
                return errorMessage;

            case "$IF":
                errorMessage = context.getString(R.string.invalid_source_of_fund);
                return errorMessage;
            case "$IR":
                errorMessage = context.getString(R.string.please_enter_valid_card_number);
                return errorMessage;
            case "$NR":
                errorMessage = context.getString(R.string.sender_not_valid_for_this_transaction);
                return errorMessage;
            case "$PX":
                errorMessage = context.getString(R.string.txn_incomplete);
                return errorMessage;
            case "$RB":
                errorMessage = context.getString(R.string.receiver_account_is_blocked);
                return errorMessage;

            default:
                return message;

        }
    }
}
