package timesofmoney.iandmsuperagent.utilities;

import android.app.Activity;
import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import timesofmoney.iandmsuperagent.R;


/**
 * Created by kunalk on 5/9/2016.
 */
public class KeyBoardUtils {

    public static final int ARABIC_KEYBOARD = 1;
    public static final int ENGLISH_KEYBOARD = 2;
    private RelativeLayout keyboard_layout;
    private Context context;
    private String text="";

    public void setCustomKeyboard(final Context context, final RelativeLayout keyboardlayout, final EditText editText, final View view,int type) {
        this.keyboard_layout = keyboardlayout;
        this.context = context;


        final KeyboardView kv = (KeyboardView) ((Activity)context).getLayoutInflater().inflate(R.layout.keyboard, null);
        final Keyboard keyboard = new Keyboard(context, R.xml.qwerty);
        kv.setKeyboard(keyboard);

        keyboard_layout.addView(kv);
        keyboard_layout.setVisibility(View.GONE);


        kv.setOnKeyboardActionListener(new KeyboardView.OnKeyboardActionListener() {
            @Override
            public void onPress(int primaryCode) {

            }

            @Override
            public void onRelease(int primaryCode) {

            }

            @Override
            public void onKey(int primaryCode, int[] keyCodes) {

                LogUtils.Verbose("TAG", " Key is " + primaryCode + " ");
                switch(primaryCode){
                    case Keyboard.KEYCODE_DELETE :
                        try {
                            text = text.substring(0, text.length() - 1);
                            editText.setText(text);
                            break;
                        }catch (Exception e){}

                    case 32:

                        try {
                            text = text+" ";
                            editText.setText(text);
                            break;
                        }catch (Exception e){}

                    case -4:
                         keyboardlayout.setVisibility(View.GONE);
                        if(view!=null)
                            view.requestFocus();
                        break;
                    default:
                        try {

                            char code = (char)primaryCode;
                            text=text+code;
                            editText.setText(text);

                            LogUtils.Debug("TAG", "Code " + primaryCode + "Arabic  " + text);
                        }catch (Exception e){}

                }


                editText.setSelection(editText.length());
            }

            @Override
            public void onText(CharSequence text) {

                LogUtils.Verbose("TAG", "text " + text);
            }

            @Override
            public void swipeLeft() {

            }

            @Override
            public void swipeRight() {

            }

            @Override
            public void swipeDown() {

            }

            @Override
            public void swipeUp() {

            }
        });


        final View.OnTouchListener exitSoftKeyBoard = new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                if(v.equals(editText)){
                    editText.requestFocus();
                    keyboardlayout.setVisibility(View.VISIBLE);
                }
                return true;
            }
        };

        editText.setOnTouchListener(exitSoftKeyBoard);

        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!hasFocus)
                    keyboardlayout.setVisibility(View.GONE);
            }
        });

    }


    public void hideCustomKeyboard()
    {
        if(keyboard_layout.getVisibility()==View.VISIBLE)
            keyboard_layout.setVisibility(View.GONE);
    }

    public boolean isVisible()
    {
      if(keyboard_layout!=null)
        return keyboard_layout.getVisibility()==View.VISIBLE?true:false;

        return false;
    }
}
