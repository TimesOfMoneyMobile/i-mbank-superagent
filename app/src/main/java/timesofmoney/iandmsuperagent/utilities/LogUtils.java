package timesofmoney.iandmsuperagent.utilities;

import android.util.Log;

import timesofmoney.iandmsuperagent.BuildConfig;


/**
 * Created by kunalk on 10/27/2015.
 */
public class LogUtils {


    public static void Verbose(String tag, String message) {

        if (BuildConfig.DEBUG)
            Log.v(tag, message);

    }

    public static void Error(String tag, String message) {

        if (BuildConfig.DEBUG)
            Log.e(tag, message);

    }

    public static void Debug(String tag, String message) {

        if (BuildConfig.DEBUG)
            Log.d(tag, message);

    }


    public static void Exception(Exception e,String request, String response) {

        if (BuildConfig.DEBUG)
            e.printStackTrace();

    }

    public static void Exception(Exception e) {

        if (BuildConfig.DEBUG)
            e.printStackTrace();

    }

    public static void Exception(Exception e, String p) {

        if (BuildConfig.DEBUG)
            e.printStackTrace();
        Log.v("Exception", p);

    }
}
