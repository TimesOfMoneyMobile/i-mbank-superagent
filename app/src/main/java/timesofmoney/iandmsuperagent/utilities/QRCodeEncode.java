package timesofmoney.iandmsuperagent.utilities;

import android.content.Context;
import android.text.TextUtils;

import timesofmoney.iandmsuperagent.IMBAgentApp;
import timesofmoney.iandmsuperagent.models.MerchantProfile;

/**
 * Created by pankajp on 4/26/2016.
 */
public class QRCodeEncode {

    Context mContext;

    public QRCodeEncode(Context mContext) {
        this.mContext = mContext;
    }


    public static MerchantProfile getMerchant() {
        MerchantProfile mp = IMBAgentApp.getMerchantProfile();
        return mp;
    }

    public boolean isAlphanumeric(String str) {
        String temp = str;
        String s = temp.replaceAll("\\s+", "");
        if (s.matches("^[a-zA-Z0-9]*$")) {
            return true;
        }

        return false;
    }

    public boolean isNumeric(String str) {
        try {

            Float.parseFloat(str);

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public String getAscciValue(int length) {
        switch (length) {
            case 0:
                return "0";
            case 1:
                return "1";
            case 2:
                return "2";
            case 3:
                return "3";
            case 4:
                return "4";
            case 5:
                return "5";
            case 6:
                return "6";
            case 7:
                return "7";
            case 8:
                return "8";
            case 9:
                return "9";
            case 10:
                return "A";
            case 11:
                return "B";
            case 12:
                return "C";
            case 13:
                return "D";
            case 14:
                return "E";
            case 15:
                return "F";
            case 16:
                return "G";
            case 17:
                return "H";
            case 18:
                return "I";
            case 19:
                return "J";
            case 20:
                return "K";
            case 21:
                return "L";
            case 22:
                return "M";
            case 23:
                return "N";
            case 24:
                return "O";
            case 25:
                return "P";
            case 26:
                return "Q";
            case 27:
                return "R";
            case 28:
                return "S";
            case 29:
                return "T";
            case 30:
                return "U";
            case 31:
                return "V";
            case 32:
                return "W";
            case 33:
                return "X";
            case 34:
                return "Y";
            case 35:
                return "Z";
            default:
                return null;
        }

    }

    public boolean isAlpha(String str) {
        if (str.matches("[a-zA-Z]*")) {
            return true;
        }
        return false;
    }

    public boolean merchantIdCheck() {

        if (IMBAgentApp.getMerchantProfile() != null) {

            if (IMBAgentApp.getMerchantProfile().getMerchantId() != null) {
                String merchantId = IMBAgentApp.getMerchantProfile().getMerchantId();
                int merchantIdLength = merchantId.length();

                if (merchantIdLength >= 8 && merchantIdLength <= 16 && isNumeric(merchantId)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;

    }

    public boolean merchantNameCheck() {

        if (IMBAgentApp.getMerchantProfile() != null) {

            if (IMBAgentApp.getMerchantProfile().getMerchantName() != null) {
                String merchantName = IMBAgentApp.getMerchantProfile().getMerchantName();
                int merchantNameLength = merchantName.length();

                if (isAlphanumeric(merchantName) && merchantNameLength <= 25) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean merchantCCCheck() {

        if (IMBAgentApp.getMerchantProfile() != null) {

            if (IMBAgentApp.getMerchantProfile().getMCC() != null) {

                String merchantCC = IMBAgentApp.getMerchantProfile().getMCC();
                int merchantCCLength = merchantCC.length();

                if (isNumeric(merchantCC) && merchantCCLength == 4) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean cityNameCheck() {

        if (IMBAgentApp.getMerchantProfile() != null) {

            if (IMBAgentApp.getMerchantProfile().getCity() != null) {
                String cityName = IMBAgentApp.getMerchantProfile().getCity();
                int cityNameLength = cityName.length();

                if (isAlphanumeric(cityName) && cityNameLength <= 13) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean countryCodeCheck() {

        if (IMBAgentApp.getMerchantProfile() != null) {

            if (IMBAgentApp.getMerchantProfile().getCountryCode() != null) {
                String countryCode = IMBAgentApp.getMerchantProfile().getCountryCode();
                int countryCodeLength = countryCode.length();

                if (isAlpha(countryCode) && countryCodeLength == 2) {
                    return true;
                }
            }
        }

        return false;
    }

    public String getMerchantQR() {

        String merchantQR = "";
        if (merchantIdCheck()) {
            String merchantid = QRCodeEncode.getMerchant().getMerchantId();
            merchantQR = "0" + getAscciValue(merchantid.length()) + merchantid;
            LogUtils.Verbose("id", merchantid);
        } else
            return "1";


        if (merchantNameCheck()) {
            String merchantName = QRCodeEncode.getMerchant().getMerchantName();
            merchantQR = merchantQR + "1" + getAscciValue(merchantName.length()) + merchantName;
            LogUtils.Verbose("name", merchantName);
        } else
            return "2";

        if (merchantCCCheck()) {
            String merchantCC = QRCodeEncode.getMerchant().getMCC();
            merchantQR = merchantQR + "2" + getAscciValue(merchantCC.length()) + merchantCC;
        } else
            return "3";

        if (cityNameCheck()) {
            String cityName = QRCodeEncode.getMerchant().getCity();
            merchantQR = merchantQR + "3" + getAscciValue(cityName.length()) + cityName;
        } else
            return "4";

        if (countryCodeCheck()) {
            String countryCode = QRCodeEncode.getMerchant().getCountryCode();
            merchantQR = merchantQR + "4" + getAscciValue(countryCode.length()) + countryCode;
        } else
            return "5";


        merchantQR = merchantQR + "5" + "3" + "818";

        String ConFee = "";
        int ConFeeLength;
        if (!TextUtils.isEmpty(QRCodeEncode.getMerchant().getTipFlag())) {
            if (QRCodeEncode.getMerchant().getTipFlag().equalsIgnoreCase("F")) {


                float confee = 0;
                try {
                    confee = Float.valueOf(QRCodeEncode.getMerchant().getConvenienceFee());
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                }
                int intconfee = (int) confee;
                int conlength = (int) Math.log10(intconfee) + 1;
                ConFeeLength = QRCodeEncode.getMerchant().getConvenienceFee().length();
                ConFee = "A" + getAscciValue(conlength) + intconfee;

                merchantQR = merchantQR + "9202" + ConFee;

            } else if (QRCodeEncode.getMerchant().getTipFlag().equalsIgnoreCase("P")) {
                float confee = 0;
                try {
                    confee = Float.valueOf(QRCodeEncode.getMerchant().getConvenienceFee());
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                }

                int intconfee = (int) confee;
                int conlength = (int) Math.log10(intconfee) + 1;
                ConFeeLength = QRCodeEncode.getMerchant().getConvenienceFee().length();
                ConFee = "B" + getAscciValue(conlength) + intconfee;
                merchantQR = merchantQR + "9203" + ConFee;

            } else if (QRCodeEncode.getMerchant().getTipFlag().equalsIgnoreCase("Y")) {

                merchantQR = merchantQR + "9201";
            }
        }

        LogUtils.Verbose("STATIC", merchantQR);

        return merchantQR;
    }

    public String dynamicQR(String transactionAmount, String pid, String sid) {

        String merchantQR = "";
        if (merchantIdCheck()) {
            String merchantid = QRCodeEncode.getMerchant().getMerchantId();
            merchantQR = "0" + getAscciValue(merchantid.length()) + merchantid;
            LogUtils.Verbose("id", merchantid);
        } else
            return "1";

        if (merchantNameCheck()) {
            String merchantName = QRCodeEncode.getMerchant().getMerchantName();
            merchantQR = merchantQR + "1" + getAscciValue(merchantName.length()) + merchantName;
            LogUtils.Verbose("name", merchantName);
        } else
            return "2";

        if (merchantCCCheck()) {
            String merchantCC = QRCodeEncode.getMerchant().getMCC();
            merchantQR = merchantQR + "2" + getAscciValue(merchantCC.length()) + merchantCC;
        } else
            return "3";

        if (cityNameCheck()) {
            String cityName = QRCodeEncode.getMerchant().getCity();
            merchantQR = merchantQR + "3" + getAscciValue(cityName.length()) + cityName;
        } else
            return "4";

        if (countryCodeCheck()) {
            String countryCode = QRCodeEncode.getMerchant().getCountryCode();
            merchantQR = merchantQR + "4" + getAscciValue(countryCode.length()) + countryCode;
        } else
            return "5";

        merchantQR = merchantQR + "5" + "3" + "818";

        if (isNumeric(transactionAmount) && transactionAmount.length() <= 12) {
            merchantQR = merchantQR + "6" + getAscciValue(transactionAmount.length()) + transactionAmount;
        }

        // Primary ID

        if (isAlphanumeric(pid) && pid.length() > 0) {
            merchantQR = merchantQR + "7" + getAscciValue(pid.length()) + pid;
        }

        // Secondary ID
        if (isAlphanumeric(sid) && sid.length() > 0) {
            merchantQR = merchantQR + "8" + getAscciValue(sid.length()) + sid;
        }

        String ConFee = "";
        int ConFeeLength;
        if (!TextUtils.isEmpty(QRCodeEncode.getMerchant().getTipFlag())) {
            if (QRCodeEncode.getMerchant().getTipFlag().equalsIgnoreCase("F")) {
                float confee = 0;
                try {
                    confee = Float.valueOf(QRCodeEncode.getMerchant().getConvenienceFee());
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                }

                int intconfee = (int) confee;
                int conlength = (int) Math.log10(intconfee) + 1;
                ConFeeLength = QRCodeEncode.getMerchant().getConvenienceFee().length();
                ConFee = "A" + getAscciValue(conlength) + intconfee;

                merchantQR = merchantQR + "9202" + ConFee;

            } else if (QRCodeEncode.getMerchant().getTipFlag().equalsIgnoreCase("P")) {
                float confee = 0;
                try {
                    confee = Float.valueOf(QRCodeEncode.getMerchant().getConvenienceFee());
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                }

                int intconfee = (int) confee;
                int conlength = (int) Math.log10(intconfee) + 1;
                ConFeeLength = QRCodeEncode.getMerchant().getConvenienceFee().length();
                ConFee = "B" + getAscciValue(conlength) + intconfee;
                merchantQR = merchantQR + "9203" + ConFee;

            } else if (QRCodeEncode.getMerchant().getTipFlag().equalsIgnoreCase("Y")) {

                merchantQR = merchantQR + "9201";
            }
        }


        LogUtils.Verbose("QRCODEDYNAMIC", merchantQR);
        return merchantQR;


    }


}


